if(!global['App']) {
    global.App = {};
}

var express = require('express'),
    app     = express(),
    path = require('path'),
    serverConfig = require('./config/server-config'),
    db = require('./config/server-db'),
    fs = require("fs"),
    https = require('https'),
    multipart = require('connect-multiparty');


//Middleware
var logger = require('morgan'),
    methodOverride = require('method-override'),
    compression = require('compression'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    errorHandler = require('errorhandler');



var privateKey = fs.readFileSync('./certs/privatekey.pem');
var certificate = fs.readFileSync('./certs/certificate.pem');

// var privateKey = fs.readFileSync('/etc/letsencrypt/live/lcmc.io/privkey.pem');
// var certificate = fs.readFileSync('/etc/letsencrypt/live/lcmc.io/cert.pem');


var options = {
    key: privateKey,
    cert: certificate
};


//Configure
app.set('port', process.env.PORT || serverConfig.port);



app.use(logger(serverConfig.logger));

app.use(methodOverride());
//
app.use(multipart({
    uploadDir: serverConfig.fileUploadFolder
}));

// app.use(multer({
//     dest: serverConfig.fileUploadFolder,
//     onFileUploadStart: function (file) {
//         console.log(file.originalname + ' is starting...')
//     },
//     onFileUploadComplete: function (file) {
//         console.log(file.fieldname + ' uploaded to  ' + file.path);
//     }
// }));


if(serverConfig.enableCompression) {
    app.use(compression()); //Performance - we tell express to use Gzip compression
}
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '5mb'
}));
// parse application/json
app.use(bodyParser.json({limit: '5mb'}));

//CORS Supports
if(serverConfig.enableCORS){

    app.use( function(req, res, next) {
        var ac = serverConfig.AccessControl;
        res.header('Access-Control-Allow-Origin', ac.AllowOrigin); // allowed hosts
        res.header('Access-Control-Allow-Methods', ac.AllowMethods); // what methods should be allowed
        res.header('Access-Control-Allow-Headers', ac.AllowHeaders); //specify headers
        res.header('Access-Control-Allow-Credentials', ac.AllowCredentials); //include cookies as part of the request if set to true
       // res.header('Access-Control-Max-Age', ac.MaxAge); //prevents from requesting OPTIONS with every server-side call (value in seconds)

        if (req.method === 'OPTIONS') {
            res.sendStatus(200).end();
        }
        else {
            next();
        }
    });
}
app.all('/api/*', [require('./middlewares/RouterProcessor')]);
app.all('/api/v1/*', [require('./middlewares/ValidateRequest')]);
app.use('/', require('./routes'));


app.use(function(req, res, next) {
    //  var err = new Error('Not Found');
    //  err.status = 404;
    next('404 Not Found');
});

//Dev
if (app.get('env') === 'development') {

    app.use(errorHandler({ dumpExceptions: true, showStack: true }));
}

global.App.env = app.get('env');

var serv = https.createServer(options, app).listen(process.env.PORT || 3000, function () {
     var addr = serv.address();
    console.log("Express server listening at", addr.address + ":" + addr.port, app.settings.env);
});

// var secureServer = https.createServer(options, app);
//
// secureServer.listen(app.get('port'), function(){
//     console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
// });

