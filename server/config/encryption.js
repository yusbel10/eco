var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    passphrase = '@SudoAdmin1!';

var encryption = {


    encrypt: function (string) {
        var cipher = crypto.createCipher(algorithm, passphrase);
        var crypted = cipher.update(string, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    },

    decryp: function (string) {
        var decipher = crypto.createDecipher(algorithm, passphrase);
        var dec = decipher.update(string, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    }

};
module.exports = encryption;

/**

function encrypt (string){
    var cipher = crypto.createCipher(algorithm,passphrase);
    var crypted = cipher.update(string,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt (string){
    var decipher = crypto.createDecipher(algorithm,passphrase);
    var dec = decipher.update(string,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
}

//module.exports = {
//    encrypt : encrypt,
 //   decrypt : decrypt,
 //   test : test
//}

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;

 **/

