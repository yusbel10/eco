Ext.define('ECO.view.apps.patients.children.contact.ContactInfoController',{
    extend: 'Ext.app.ViewController',

    alias: 'controller.contactInfo',

    //region loadAllGridStores
    loadAllGridStores: function () {

        var me = this,
            viewModel = this.getView().getViewModel().getData().patientRecord;



        var telephoneStore = Ext.create('Ext.data.SimpleStore', {
            autoLoad: false,
            sortOnLoad: true,
            remoteSort: false,
            model: 'ECO.view.apps.patients.children.contact.model.Telephone',
            proxy:{
                type: 'rest',
                url: ECO.app.globals.url+'v1/patient/profile/phone',
                extraParams: {pat_id: viewModel.pat_id},
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            listeners:{
                load: function (e, rec, success, op, eOpts) {

                }
            }

        });

        var addressStore = Ext.create('Ext.data.SimpleStore', {
            autoLoad: false,
            sortOnLoad: true,
            remoteSort: false,
            model: 'ECO.view.apps.patients.children.contact.model.Address',
            proxy:{
                type: 'rest',
                url: ECO.app.globals.url+'v1/patient/address',
                extraParams: {pat_id: viewModel.pat_id},
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            listeners:{
            load: function (e, rec, success, op, eOpts) {

            }
        }

        });

        telephoneStore.load({callback: function (data) {}});
        addressStore.load({callback: function (data) {}});


       // this.getView().setTeleComboStore(telComboStore);
        this.getView().setTeleStore(telephoneStore);
        this.getView().setAddressStore(addressStore);
    },
    //endregion

    //region loadAllColumns
    loadAllColumns: function () {

        var telComboStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'type', 'tele_type'],
            data: [
                {"id":0, type:"0", tele_type:'Home'},
                {"id":1, type:"1", tele_type:'Mobile'},
                {"id":2, type:"2", tele_type:'Alternative'}
            ]
        });


        var telephoneColumn = [{
            xtype: 'gridcolumn',
            renderer: function(value) {
                if(value) {
                    return '<span class="x-fa fa-star"></span>';
                }
                return '<span class="x-fa fa-star-o"></span>';

            },
            width: 45,
            dataIndex: 'tele_primary'
        },{
            xtype: 'gridcolumn',
            dataIndex: 'tele_type',
            text: 'Type',
            width: 150,
            renderer: function (val) {
                return ECO.helper.GridRenderer.phoneTypeIntToString(val);
            },
            editor: new Ext.form.field.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                displayField:'tele_type',
                valueField:'type',
                publishes: 'id',
                queryMode: 'local',
                store: telComboStore,
                allowBlank: false

            })
            // flex: 1
        },{
            xtype: 'gridcolumn',
            dataIndex: 'tele_number',
            text: 'Phone',
            width: 150,
            editor: {
                xtype: 'textfield',
                maxLength: 10,
                enforceMaxLength: true,
                maskRe: /[0-9.]/,
                regex: /^\d{10}?$/,
                allowBlank: false
            }
        },{
            xtype: 'gridcolumn',
            dataIndex: 'tele_comment',
            text: 'Comment',
            cellWrap: true,
            fieldStyle: 'text-transform:uppercase',
            flex: 1,
            editor: {
                xtype: 'textfield',
                maxLength: 50,
                enforceMaxLength: true,
                listeners:{
                    change:function(field, newval){
                        field.setValue(newval.toUpperCase());
                    }
                }
            }
        },{
            xtype: 'actioncolumn',
            items: [{
                xtype: 'button',
                iconCls: 'x-fa fa-save',
                tooltip: 'Save',
                handler:'onSavePhoneClick'

            },{
                xtype: 'button',
                iconCls: 'x-fa fa-close',
                tooltip: 'Remove current telephone',
                handler: 'onRemovePhoneClick'
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-star-o',
                tooltip: 'Mark as primary',
                handler:'onSetPhonePrimaryClick'
            },{
                xtype: 'button',
                hidden: !ECO.Profile.isUserInRole(['2']),
                tooltip: 'Call number',
                // bind: '{record.progress}',
                iconCls: 'x-fa fa-phone-square',
                handler: 'onCallPatientBtnClick'

            }],

            cls: 'content-column',
            width: 150,
            dataIndex: 'bool',
            text: 'Actions',
            tooltip: 'edit '
        }];

        var addressColumn = [{
            xtype: 'gridcolumn',
            renderer: function(value) {
                if(value) {
                    return '<span class="x-fa fa-star"></span>';
                }
                return '<span class="x-fa fa-star-o"></span>';

            },
            width: 45,
            dataIndex: 'addr_primary'
        },{
            xtype: 'gridcolumn',
            dataIndex: 'addr_street',
            fieldStyle: 'text-transform:uppercase',
            text: 'Street',
            flex: 1,
            cellWrap: false,
            editor: {
                xtype: 'textfield',
                allowBlank: false,
                listeners:{
                    change:function(field, newval){
                        field.setValue(newval.toUpperCase());
                    }
                }
            }
        },{
            xtype: 'gridcolumn',
            dataIndex: 'addr_apartment',
            fieldStyle: 'text-transform:uppercase',
            text: 'Apt',
            width:80,
            editor: {
                xtype: 'textfield',
                listeners:{
                    change:function(field, newval){
                        field.setValue(newval.toUpperCase());
                    }
                }
            }
        },{
            xtype: 'gridcolumn',
            dataIndex: 'addr_city',
            fieldStyle: 'text-transform:uppercase',
            text: 'City',
            width:150,
            editor: {
                xtype: 'textfield',
                allowBlank: false,
                listeners:{
                    change:function(field, newval){
                        field.setValue(newval.toUpperCase());
                    }
                }
            }
        },{
            xtype: 'gridcolumn',
            dataIndex: 'addr_zipcode',
            text: 'Zip',
            width:60,
            editor: {
                xtype: 'textfield',
                maxLength: 5,
                enforceMaxLength: true,
                maskRe: /[0-9.]/,
               // regex: /^\d{10}?$/,
                allowBlank: false
            }
        },{
            xtype: 'gridcolumn',
            dataIndex: 'addr_comment',
            fieldStyle: 'text-transform:uppercase',
            text: 'Comment',
            cellWrap: true,
            flex: 1,
            editor: {
                xtype: 'textfield',
                allowBlank: true,
                listeners:{
                    change:function(field, newval){
                        field.setValue(newval.toUpperCase());
                    }
                }
            }
        },{
            xtype: 'actioncolumn',
            items: [{
                xtype: 'button',
                iconCls: 'x-fa fa-save',
                tooltip: 'Save',
                handler:'onSaveAddressClick'

            },{
                xtype: 'button',
                iconCls: 'x-fa fa-close',
                tooltip: 'Remove current address',
                handler: 'onRemoveAddressClick'
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-star-o',
                tooltip: 'Mark as primary',
                handler:'onSetAddressPrimaryClick'
            }],

            cls: 'content-column',
            width: 120,
            dataIndex: 'bool',
            text: 'Actions',
            tooltip: 'edit '
        }];


        this.getView().setTeleColumn(telephoneColumn);
        this.getView().setAddressColumn(addressColumn);
    },
    //endregion

    //region onCallPatientBtnClick
    onCallPatientBtnClick: function (grid, rowIndex, colIndex) {
        var view = this.getView(),
            PatientRecord =  view.getViewModel().getData().patientRecord,
            data = grid.getStore().getAt(rowIndex).getData();

        if(ECO.Profile.isUserInRole(['2'])){

            if (!data.tele_number || data.tele_number == "" || data.tele_number == null){ return }
            var voicePanel = ECO.cache.Global.getVoicePanel(),
                isCollapse = voicePanel.getCollapsed();



            voicePanel.setLastPatient((PatientRecord.pat_id) || null);
            voicePanel.setLastNumber((data.tele_number) || null);

            ECO.cache.Global.updateVoicePanelProfile(PatientRecord.pat_picture, PatientRecord.pat_first_name + ' ' + PatientRecord.pat_last_name, data.tele_number);



            if(isCollapse){
                if(!ECO.cache.Global.getSvg()){ ECO.cache.Global.createVisualizers(false); }

                voicePanel.expand();
            }

        }
    },
    //endregion

    //region onSavePhoneClick
    onSavePhoneClick: function (grid, rowIndex, colIndex) {

        var rec = grid.getStore().getAt(rowIndex).getData();


        if(!ECO.helper.Func.validateVariable(rec.tele_number) || !ECO.helper.Func.validateVariable(rec.tele_type)){
            alert('Invalid Fields');
            return;
        }
        if(!rec.tele_pat_id ){
            rec.tele_pat_id = this.getView().getViewModel().getData().patientRecord.pat_id;
        }

        Ext.Msg.confirm('Confirm', 'Are you sure you want to modify this field?',
            function (choice) {
                if (choice === 'yes') {


                    ECO.helper.Func.callAjax('v1/patient/profile/phone', rec, 'POST').then(function (content) {

                        if(content.success){
                            ECO.helper.Func.showToast('Phone saved successfully');
                            grid.getStore().reload();
                        }else{
                            ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving phone');
                        }

                    });
                }
        });
    },
    //endregion

    //region onRemovePhoneClick
    onRemovePhoneClick: function (grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex).getData();
        Ext.Msg.confirm('Confirm', 'Are you sure you want to remove this field?',
            function (choice) {
                if (choice === 'yes') {

                    ECO.helper.Func.callAjax('v1/patient/profile/phone', rec, 'DELETE').then(function (content) {

                        if(content.success){
                            ECO.helper.Func.showToast('Phone removed successfully');
                            grid.getStore().reload();
                        }else{
                            ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving phone');
                        }

                    });
                }
            });
    },
    //endregion

    //region onSetPrimaryClick
    onSetPhonePrimaryClick: function (grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex).getData();

        if(!rec.tele_pat_id){
            rec.tele_pat_id = this.getView().getViewModel().getData().patientRecord.pat_id;
        }

        if(rec.tele_primary == 1){
            Ext.Msg.alert('Exitst', 'Information is already set as primary.');
            return;
        }


        Ext.Msg.confirm('Confirm', 'Are you sure you want to set as primary?',
            function (choice) {
                if (choice === 'yes') {

                    ECO.helper.Func.callAjax('v1/patient/profile/phone/primary', rec, 'PUT').then(function (content) {

                        if(content.success){
                            ECO.helper.Func.showToast('Phone saved successfully');
                            grid.getStore().reload();
                        }else{
                            ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving phone');
                        }

                    });
                }
            });
    },
    //endregion

    //region onAddAddressClick
    onAddAddressClick: function(e, btn, panel){
        var recStore = this.lookupReference('contactAddressGrid').getStore(),
            view = this.getView();


        this.openAddressVerificationWindow(null, this, recStore, view);
    },
    //endregion

    //region onAddressEditClick
    onSaveAddressClick: function(grid, rowIndex, colIndex){

        var rec = grid.getStore().getAt(rowIndex).getData(),
            view = this.getView(),
            me = this;

        if(!rec.pat_id){
            rec.pat_id = this.getView().getViewModel().getData().patientRecord.pat_id;
        }

        if(!rec.addr_city || !rec.addr_street || !rec.addr_zipcode || !rec.pat_id){
            Ext.Msg.alert('Error', 'Missing Field');
            return;
        }else{
            Ext.Msg.confirm('Confirm', 'Are you sure you want to modify this field?',
                function (choice) {
                    if (choice === 'yes') {
                        me.config._selectAddress = false;
                        var address = rec.addr_street + ' ' + rec.addr_city + ' FL ' + rec.addr_zipcode;
                        ECO.helper.Func.initMask();


                        ECO.helper.Func.codeAddress(address, function(results){
                            if(results){
                                var multAddressStore = Ext.create('Ext.data.Store',{
                                    fields:[
                                        {name: 'formatted_address', type: 'string'}
                                    ],
                                    autoLoad: true,
                                    data: results,
                                    proxy: {
                                        type: 'memory',
                                        reader: {
                                            type: 'json'
                                        }
                                    },
                                    lazyFill: false
                                });

                                if(multAddressStore.getData().length > 0){
                                    me.config._foundAddress = true;
                                    me.openAddressVerificationWindow(rec, this, grid, view, multAddressStore);

                                }else{
                                   //If not address found then save

                                    me.saveAddress(rec, grid, false);

                                }
                                ECO.helper.Func.fadeOutMask().delay(1000);
                            }else{
                                //unable to connec to to google
                                ECO.helper.Func.fadeOutMask().delay(1000);
                                Ext.Msg.alert('Error', 'Unable to geocode address');
                            }
                        });

                    }
                });
        }

    },
    //endregion

    //region saveAddress
    saveAddress: function (rec, grid, win) {

        if(!rec.addr_city || !rec.addr_street || !rec.addr_zipcode || !rec.addr_patient_id){
            Ext.Msg.alert('Missing', 'Missing address information');
            return;
        }

        ECO.helper.Func.callAjax('v1/patient/profile/address', rec, 'POST').then(function (content) {


            if(content.success){
                ECO.helper.Func.showToast('Address saved successfully');
                grid.getStore().reload();
                if(win){ win.destroy() }
            }else{
                ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving Address');
            }

        });
    },
    //endregion

    //region openAddressVerificationWindow
    openAddressVerificationWindow: function(rec, _me, grid, _view, multaddStore){
        var win,
            me = this,
            patid= _view.getViewModel().getData().patientRecord.pat_id;

        if(!patid){
            return;
        }

        if(!rec) {

            rec = {pat_id: patid};

        }

        win = new Ext.Window({
            applyTo : _me,
            layout: 'fit',
            resize: false,
            modal: true,
            width: 755,
            height: 460,
            closable: false,
            header:false,
            frame: true,
            closeAction : 'close',
            plain:true,
            items: [{
                xtype:'panel',
                layout: 'card',
                cls: 'forms-patient-dynamic-window',
                bodyPadding: 15,
                defaults: {
                    border:false
                },
                buttons:[{
                    itemId: 'card-skip',
                    Id: 'card-skip',
                    text: 'Skip',
                    handler:function () {
                        me.saveAddress(rec, grid, win);
                    }
                },{
                    text:'Close',
                    handler: function () {
                        win.destroy();
                    }
                }],
                items: [{
                    id: 'card-0',
                    fieldDefaults: {
                        labelAlign: 'left',
                        labelWidth: 90,
                        anchor: '100%',
                        msgTarget: 'side'
                    },
                    items: [{
                        margin: '0 0 20 0',
                        xtype: 'component',
                        html: [
                            '<h2>Verify address...</h2>'
                        ]
                    }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                            columnWidth: 0.65,
                            width: 380,
                            layout: 'fit',
                            xtype: 'gridpanel',
                            reference: 'multiAddressGrid',
                            itemId: 'multiAddressGrid',
                            store: multaddStore,
                            height: 380,
                            columns: [{
                                text: 'Address',
                                flex: 1,
                                sortable: true,
                                dataIndex: 'formatted_address'
                            }],
                            listeners: {
                                selectionchange: function (model, records) {
                                    var rec = records[0];
                                    if (rec) {
                                        var gmap = win.down('#multipleAddressMap').gmap,//this.up().items.items[1],
                                            gridpanel = win.down('#multiAddressGrid');
                                        var results = gridpanel.getStore().getData().items,
                                            selectedrec = gridpanel.getSelectionModel().getSelection()[0],
                                            rowindex = gridpanel.store.indexOf(selectedrec);
                                        gmap.setCenter(results[rowindex].get('geometry').location);

                                        new google.maps.Marker({
                                            map: gmap,
                                            position: results[rowindex].get('geometry').location
                                        });
                                    }
                                },
                                itemdblclick: function (dataview, index, item, e) {
                                    try {
                                        var street = false,
                                            route = false,
                                            city = false,
                                            apt = rec.addr_apartment,
                                            comment = rec.addr_comment,
                                            zip = false,
                                            lat = false,
                                            lng = false,
                                            addid = (rec.addr_id || null);
                                        // if(index.get('types')[0] != "street_address"){
                                        //     throw 'error';
                                        // }

                                        for (var i = 0; i < index.get('address_components').length; i++) {
                                            if (index.get('address_components')[i].types[0] === "street_number") {
                                                street = index.get('address_components')[i].long_name;
                                            }
                                            if (index.get('address_components')[i].types[0] === "route") {
                                                route = index.get('address_components')[i].long_name;
                                            }
                                            if (index.get('address_components')[i].types[0] === "locality") {
                                                city = index.get('address_components')[i].long_name;
                                            }
                                            if (index.get('address_components')[i].types[0] === "postal_code") {
                                                zip = index.get('address_components')[i].long_name;
                                            }
                                        }
                                        lat = index.get('geometry').location.lat();
                                        lng = index.get('geometry').location.lng();
                                        if (!street || !route || !city || !zip || !lat || !lng) {
                                            throw 'error';
                                        } else {
                                            street = street + ' ' + route;
                                            var data = {
                                                addr_street: street,
                                                addr_apartment: apt,
                                                addr_city: city,
                                                addr_zipcode: zip,
                                                addr_lat: lat,
                                                addr_lng: lng,
                                                addr_comment: comment,
                                                addr_patient_id: rec.pat_id,
                                                addr_id: addid
                                            };

                                            me.saveAddress(data, grid, win);
                                        }

                                    }
                                    catch (err) {
                                        me.config._foundAddress = false;
                                        Ext.MessageBox.show({
                                            title: 'Verify',
                                            msg: 'Unable to geocode address, select the skip button',
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.WARNING
                                        });
                                        return;
                                    }
                                }
                            }
                        },{
                            columnWidth: 0.35,
                            margin: '0 0 0 10',
                            xtype: 'gmappanel',
                            reference: 'multipleAddressMap',
                            itemId: 'multipleAddressMap',
                            center: new google.maps.LatLng(-34.397, 150.644),
                            mapOptions: {
                                mapTypeId: 'roadmap',
                                zoom: 12
                            },
                            markers: [],
                            width: 320,
                            height: 260
                        }]
                    }]
                }]
            }]
        });

        win.show();

        // win = new Ext.Window({
        //     applyTo : _me,
        //     layout: 'fit',
        //     resize: false,
        //     modal: true,
        //     width: 755,
        //     height: 460,
        //     closable: false,
        //     header:false,
        //     frame: true,
        //     closeAction : 'close',
        //     plain:true,
        //     items: [{
        //         xtype:'panel',
        //         layout: 'card',
        //         cls: 'forms-patient-dynamic-window',
        //         bodyPadding: 15,
        //          defaults: {
        //              border:false
        //          },
        //         buttons:[{
        //             itemId: 'card-prev',
        //             text: '&laquo; Previous',
        //             handler: function () {
        //                 // var foundPatient = me.config._foundPatient;
        //                 var view = win,
        //                     l = view.items.items[0].getLayout(),
        //                     i = l.activeItem.id.split('card-')[1];
        //                  view.down('#card-next').setText('Next &raquo;');
        //                 me.doCardNavigation(-1, view);
        //             },
        //             disabled: true
        //         },{
        //             itemId: 'card-next',
        //             Id: 'card-next',
        //             text: 'Next &raquo;',
        //             handler: function(){
        //                 var nMe = this,
        //                     view = win,
        //                     l = view.items.items[0].getLayout(),
        //                     showNext = false;
        //                 var i = l.activeItem.id.split('card-')[1];
        //                 if(i == 0)
        //                 {
        //                     me.config._selectAddress = false;
        //
        //                     //If on Found Address card
        //                     showNext = false;
        //                     var addressForm = win.down('#addressForm');
        //                     var addressFormValues = addressForm.getForm().getValues();
        //                     if(!addressForm.isValid()){
        //                         alert('Invalid Fields');
        //                         return;
        //                     }
        //                     var address = addressFormValues._street + ' ' + addressFormValues._city + ' FL ' + addressFormValues._zip;
        //
        //
        //                     var searchAddressMask = new Ext.LoadMask({msg:"Please Wait...", target: win});
        //                     searchAddressMask.show();
        //                     var foundAddress = ECO.helper.Func.codeAddress(address, function(results){
        //                         if(results){
        //                             var multAddressStore = Ext.create('Ext.data.Store',{
        //                                 fields:[
        //                                     {name: 'formatted_address', type: 'string'}
        //                                 ],
        //                                 autoLoad: true,
        //                                 data: results,
        //                                 proxy: {
        //                                     type: 'memory',
        //                                     reader: {
        //                                         type: 'json'
        //                                     }
        //                                 },
        //                                 lazyFill: false
        //                             });
        //
        //                             if(multAddressStore.getData().length > 0){
        //                                 me.config._foundAddress = true;
        //                                 var _foundAddressGrid = win.down('#multiAddressGrid');
        //                                 _foundAddressGrid.setStore(multAddressStore);
        //                                 me.doCardNavigation(1, view);
        //                             }else{
        //                                 me.doCardNavigation(2, view);
        //                             }
        //                         }else{
        //                             //unable to connecto to google;
        //                             me.doCardNavigation(2, view);
        //                          }
        //                     });
        //                      searchAddressMask.destroy();
        //                  }else if(i ==1){
        //                     var street,
        //                         city,
        //                         apt,
        //                         zip,
        //                         comment,
        //                         lng,
        //                         lat;
        //                      street = win.down('#cStreet').getValue(), city = win.down('#cCity').getValue(), apt = win.down('#cApt').getValue(), zip = win.down('#cZip').getValue(), comment = win.down('#cComment').getValue();
        //
        //
        //                     win.down('#cAddress').setValue(street + ' ' + city + ' ' + zip);
        //                     showNext = true;
        //
        //                  }else if(i ==2){
        //                      debugger;
        //                     var bForm = win.down('#addressForm').getForm(),
        //                         bFormValues = bForm.getValues(),
        //                         patAddress = false;
        //
        //                     if(me.config._selectAddress == false){
        //                         lat = null, lng = null;
        //                         patAddress = {street: win.down('#cStreet').getValue(), city: win.down('#cCity').getValue(), apt: win.down('#cApt').getValue(), zip: win.down('#cZip').getValue(), lat:lat, lng:lng};
        //                     }else{
        //                         patAddress = me.config._selectAddress
        //                     }
        //
        //                     if(bForm.isValid()){
        //
        //                         var val ={ address: patAddress, patInfo:bFormValues, pat_id: patid };//me.lookupReference('htmlNote').lastValue};
        //                         showNext = false;
        //
        //                         ECO.helper.Func.callAjax('v1/patient/profile/address', val, 'POST').then(function (content) {
        //                             debugger;
        //                             if(content.success){
        //                                 ECO.helper.Func.showToast('Phone saved successfully');
        //
        //                                 grid.reload();
        //                                 win.destroy();
        //                             }else{
        //                                 ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving phone');
        //                             }
        //
        //                         });
        //                     }
        //                     else {
        //
        //                         Ext.Msg.alert( "Error!", "Your form is invalid!" );
        //                     }
        //
        //                 }
        //
        //                 if(showNext == true){
        //                     me.doCardNavigation(1, view);
        //                 }
        //              }
        //         },{
        //             text:'Close',
        //             handler: function () {
        //                 win.close();
        //             }
        //         }],
        //          items: [{
        //              id: 'card-0',
        //              xtype: 'form',
        //              reference: 'addressForm',
        //              itemId: 'addressForm',
        //              items: [{
        //                  margin: '0 0 20 0',
        //                  xtype: 'component',
        //                  html: [
        //                      '<h2>Continue...</h2>',
        //                      '<p>Please make sure that the information below is correct.</p>'
        //                  ]
        //              },{
        //                  xtype: 'textfield',
        //                  fieldLabel: 'Street',
        //                  reference: 'cStreet',
        //                  bind:rec.addr_street,
        //                  //  value:rec.addr_street,
        //                  fieldStyle: 'text-transform:uppercase',
        //                  itemId: 'cStreet',
        //                  afterLabelTextTpl: [
        //                      '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
        //                  ],
        //                  name: '_street',
        //                  allowBlank: false
        //              }, {
        //                  xtype: 'textfield',
        //                  afterLabelTextTpl: [
        //                      '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
        //                  ],
        //                  fieldLabel: 'City',
        //                  fieldStyle: 'text-transform:uppercase',
        //                  // value:rec.addr_city,
        //                  itemId: 'cCity',
        //                  name: '_city',
        //                  allowBlank: false
        //              }, {
        //                  xtype: 'textfield',
        //                  fieldLabel: 'Apt',
        //                  cFieldName: 'cApt',
        //                  fieldStyle: 'text-transform:uppercase',
        //                  // value:rec.addr_apartment,
        //                  itemId: 'cApt',
        //                  name: '_apt',
        //                  listeners: {
        //                      change: me.onFormFieldChange
        //                  }
        //              }, {
        //                  xtype: 'textfield',
        //                  afterLabelTextTpl: [
        //                      '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
        //                  ],
        //                  fieldLabel: 'Zip',
        //                  //  value:rec.addr_zipcode,
        //                  itemId: 'cZip',
        //                  name: '_zip',
        //                  allowBlank: false,
        //                  maxLength: 5,
        //                  enforceMaxLength: true,
        //                  maskRe: /[0-9.]/,
        //                  regex: /^\d{5}?$/
        //              }, {
        //                  xtype: 'textarea',
        //                  fieldLabel: 'Comment',
        //                  //   value:rec.addr_comment,
        //                  cFieldName: 'cComment',
        //                  itemId: 'cComment',
        //                  name: '_comment',
        //                  maxLength: 45,
        //                  enforceMaxLength: true,
        //                  listeners: {
        //                      change: me.onFormFieldChange
        //                  }
        //              }]
        //          }, {
        //              id: 'card-1',
        //              fieldDefaults: {
        //                  labelAlign: 'left',
        //                  labelWidth: 90,
        //                  anchor: '100%',
        //                  msgTarget: 'side'
        //              },
        //              items: [{
        //                  margin: '0 0 20 0',
        //                  xtype: 'component',
        //                  html: [
        //                      '<h2>Verify address...</h2>'
        //                  ]
        //              }, {
        //                  xtype: 'container',
        //                  layout: 'hbox',
        //                  items: [{
        //                      columnWidth: 0.65,
        //                      width: 380,
        //                      layout: 'fit',
        //                      xtype: 'gridpanel',
        //                      reference: 'multiAddressGrid',
        //                      itemId: 'multiAddressGrid',
        //                      height: 380,
        //                      columns: [{
        //                          text: 'Address',
        //                          flex: 1,
        //                          sortable: true,
        //                          dataIndex: 'formatted_address'
        //                      }],
        //                      listeners: {
        //                          selectionchange: function (model, records) {
        //                              var rec = records[0];
        //                              if (rec) {
        //                                  var gmap = win.down('#multipleAddressMap').gmap,//this.up().items.items[1],
        //                                      gridpanel = win.down('#multiAddressGrid');
        //                                  var results = gridpanel.getStore().getData().items,
        //                                      selectedrec = gridpanel.getSelectionModel().getSelection()[0],
        //                                      rowindex = gridpanel.store.indexOf(selectedrec);
        //                                  gmap.setCenter(results[rowindex].get('geometry').location);
        //
        //                                  new google.maps.Marker({
        //                                      map: gmap,
        //                                      position: results[rowindex].get('geometry').location
        //                                  });
        //                              }
        //                          },
        //                          itemdblclick: function (dataview, index, item, e) {
        //                              try{
        //                                  var street = false,
        //                                      route = false,
        //                                      city = false,
        //                                      apt = win.down('#cApt').getValue(),
        //                                      zip = false,
        //                                      lat = false,
        //                                      lng = false;
        //                                         // if(index.get('types')[0] != "street_address"){
        //                                         //     throw 'error';
        //                                         // }
        //
        //                                  for(var i=0; i<index.get('address_components').length; i++){
        //                                      if(index.get('address_components')[i].types[0] === "street_number"){
        //                                          street = index.get('address_components')[i].long_name;
        //                                      }
        //                                      if(index.get('address_components')[i].types[0]=== "route"){
        //                                          route = index.get('address_components')[i].long_name;
        //                                      }
        //                                      if(index.get('address_components')[i].types[0]=== "locality"){
        //                                          city = index.get('address_components')[i].long_name;
        //                                      }
        //                                      if(index.get('address_components')[i].types[0] === "postal_code"){
        //                                          zip = index.get('address_components')[i].long_name;
        //                                      }
        //                                  }
        //                                  lat = index.get('geometry').location.lat();
        //                                  lng = index.get('geometry').location.lng();
        //                                  if(!street || !route || !city || !zip || !lat || !lng){
        //                                      throw 'error';
        //                                  }else{
        //                                      street = street + ' ' + route;
        //                                      me.config._selectAddress = {street:street, apt: win.down('#cApt').getValue(), city:city, zip:zip, lat:lat, lng:lng};
        //                                      win.down('#cAddress').setValue(street + ' ' + ' ' + city + ' ' + zip);
        //                                      me.doCardNavigation(1, win);
        //                                  }
        //
        //                              }
        //                               catch(err){
        //                                   me.config._foundAddress = false;
        //                                    Ext.MessageBox.show({
        //                                        title:'Verify',
        //                                        msg: 'Unable to geocode address, select the skip button',
        //                                        buttons: Ext.MessageBox.OK,
        //                                        icon: Ext.MessageBox.WARNING
        //                                    });
        //                                   return;
        //                               }
        //                          }
        //                      }
        //                  }, {
        //                      columnWidth: 0.35,
        //                      margin: '0 0 0 10',
        //                      xtype: 'gmappanel',
        //                      reference: 'multipleAddressMap',
        //                      itemId: 'multipleAddressMap',
        //                      center: new google.maps.LatLng(-34.397, 150.644),
        //                      mapOptions: {
        //                          mapTypeId: 'roadmap',
        //                          zoom: 12
        //                      },
        //                      markers: [],
        //                      width: 320,
        //                      height: 260
        //                  }]
        //              }]
        //          },{
        //              id: 'card-2',
        //              scrollable: true,
        //              layout: 'container',
        //              //   xtype:'form',
        //              reference: 'confirmInfo',
        //              items:[{
        //                  xtype:'component',
        //                  html:[
        //                      '<h2>Confirm...</h2>',
        //                      '<p>Please make sure that your information below is correct.</p>'
        //                  ]
        //              }, {
        //                  xtype: 'form',
        //                  layout: 'hbox',
        //                  fieldDefaults: {
        //                      msgTarget: 'side',
        //                      labelAlign: 'left',
        //                      labelWidth: 100,
        //                      labelStyle: 'font-weight:bold'
        //                  },
        //                  items: [{
        //                      items: [{
        //                          xtype: 'displayfield',
        //                          fieldLabel: 'Address',
        //                          name: 'cAddress',
        //                          itemId: 'cAddress'
        //                      },{
        //                          xtype: 'displayfield',
        //                          fieldLabel: 'Apt',
        //                          name: 'cApt',
        //                          itemId: 'cApt'
        //                      },{
        //                          xtype: 'displayfield',
        //                          fieldLabel: 'Comment',
        //                          name: 'cComment'
        //                      }]
        //                  }]
        //              }]
        //          }]
        //     }],
        //     listeners: {
        //         close: function () {
        //             this.destroy();
        //         }
        //     }
        // });
        // win.show();
    },
    //endregion

    //region onFormFieldChange
    onFormFieldChange:function(field, newValue){
        (field.cFieldName == 'cDOB' || field.cFieldName == 'cPhone') ? '' : field.setValue(newValue.toUpperCase());
        var copyField = this.up().up().items.items[2].down('[name=' + field.cFieldName + ']');
        copyField.setValue(field.getValue());

    },
    //endregion

    //region doCardNavigator
    doCardNavigation: function (incr, view) {

        var l = view.items.items[0].getLayout();
        if (l.activeItem == undefined){return}
        var i = l.activeItem.id.split('card-')[1];
        var next = parseInt(i, 10) + incr;
        l.setActiveItem(next);

        view.down('#card-prev').setDisabled(next===0);
        (next == 1) ? view.down('#card-next').setText('Skip &raquo;') : view.down('#card-next').setText('Next &raquo;');
        if(next == 2){view.down('#card-next').setText('Finish')}
        // view.down('#card-next').setDisabled(next===2);
    },
    //endregion

    //region onRemoveAddressClick
    onRemoveAddressClick: function (grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex).getData();
        Ext.Msg.confirm('Confirm', 'Are you sure you want to remove this field?',
            function (choice) {
                if (choice === 'yes') {

                    ECO.helper.Func.callAjax('v1/patient/profile/address', rec, 'DELETE').then(function (content) {

                        if(content.success){
                            ECO.helper.Func.showToast('Address removed successfully');
                            grid.getStore().reload();
                        }else{
                            ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving phone');
                        }

                    });
                }
            });
    },
    //endregion

    //region onSetAddressPrimaryClick
    onSetAddressPrimaryClick: function (grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex).getData();

        if(!rec.addr_patient_id){
            rec.addr_patient_id = this.getView().getViewModel().getData().patientRecord.pat_id;
        }

        if(rec.addr_primary == 1){
            Ext.Msg.alert('Exists', 'Information is already set as primary.');
            return;
        }


        Ext.Msg.confirm('Confirm', 'Are you sure you want to set as primary?',
            function (choice) {
                if (choice === 'yes') {

                    ECO.helper.Func.callAjax('v1/patient/profile/address/primary', rec, 'PUT').then(function (content) {

                        if(content.success){
                            ECO.helper.Func.showToast('Address saved successfully');
                            grid.getStore().reload();
                        }else{
                            ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving phone');
                        }

                    });
                }
            });
    },
});