Ext.define('ECO.view.apps.patients.model.Insurance', {
    extend: 'Ext.data.Model',
    fields:[{
        name:'insu_id'
    },{
        name:'insu_provider'
    },{
        name:'insu_number'
    },{
        name:'insu_part'
    },{
        name:'insuprovider_name'
    },{
        name: 'insu_effective_date', type: 'date', dateFormat: 'Y-m-d H:i:s',
        convert: function(val, rec){
            return Ext.util.Format.date(val, 'm/d/Y');
            //return Ext.util.Format.date(val, 'Y-m-d');
        }
    }]
});
