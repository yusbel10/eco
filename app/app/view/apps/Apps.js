Ext.define('ECO.view.apps.Apps', {
    extend: 'Ext.tab.Panel',
    xtype: 'eco-appspanel',
    reference:'eco-appspanel',
    requires:[
        'ECO.view.apps.AppsController',
        'ECO.view.apps.logistics.LogisticsMain',
        'ECO.view.apps.patients.PatientsMain'
    ],

    layout: 'responsivecolumn',
    padding: 0,
    controller: 'apps',
    defaults: {
        scrollable: true
    },

    listeners:{
        openapp: 'appDblClick'
    },

    //region initComponent
    initComponent: function(){

        var me = this;

        var DataViewStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            sortOnLoad: true,
            remoteSort: false,
            fields: ['name', 'thumb', 'url', 'type', 'roles'],
            data: ECO.Profile.getAppList()
        });


        this.items = [{
            title: 'Apps',

            iconCls: 'right-icon x-fa fa-th',
            height:500,
            items:{
                xtype: 'dataview',
                id: '_img-chooser-view',
                singleSelect: true,
                overItemCls: 'x-view-over',
                plugins: {
                    xclass: 'Ext.ux.DataView.Animated'
                },
                itemSelector: 'div.thumb-wrap',
                tpl: [
                    // '<div class="details">',
                    '<tpl for=".">',
                    '<div class="thumb-wrap">',
                    '<div class="thumb">',
                    '<img src="https://s3.amazonaws.com/lcmc.eco.resources/resources/{thumb}" />',
                    '</div>',
                    '<span>{name}</span>',
                    '</div>',
                    '</tpl>'
                    // '</div>'
                ],
                store: DataViewStore,
                listeners: {
                    scope: this,
                    itemdblclick: function (a, rec, c) {
                       // window.location.href='#' + rec.data.href;

                         me.fireEvent('openapp', rec);

                        /**  if(ECO.User.isUserInRole(rec.data.role)) {
                             me.fireEvent('openapp', rec);
                         }
                         else{
                             Ext.MessageBox.show({title:"", msg:Ext.String.format('You do not have permission to access this application'),
                                 buttons:Ext.MessageBox.OK ,icon: Ext.MessageBox.WARNING})
                         }
                         **/
                    }
                }
            }

        }];

        this.callParent(arguments)
    }
    //endregion

});