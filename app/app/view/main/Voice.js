/**
 * This example shows how to create basic panels. Panels typically have a header and a body,
 * although the header is optional. The panel header can contain a title, and icon, and
 * one or more tools for performing specific actions when clicked.
 */
Ext.define('ECO.view.main.Voice', {
    extend: 'Ext.Panel',
    xtype: 'voice',
    reference:'voicePanel',
    requires: [
     //   'Ext.d3.canvas.Canvas',
       // 'Ext.d3.svg.Svg'
    ],
    config:{
        lastPatient: null,
        lastFacility: null,
        lastNumber: null
    },
    controller: 'mainviewport',
    cls: 'voiceProfile-container',

    width: 300,
    //collapsible: true,
    collapsed: true,
    collapseDirection: 'right',
    collapseMode: 'mini',
    animCollapse: true,
    bodyBorder: false,
    border: false,
    dock:'right',
    animateShadow: true,
    modal: false,
    titleCollapse : false,
    titleRotation: 0,
    ui: 'voice',
    title:'Voice Console',
    tools:[
        //  { type:'pin' }
    ],
    listeners: {
        expand: function () {
            var me = this,
                refs = this.getReferences(),
                voiceProfileTelephone = refs.voiceProfileTelephone;

            voiceProfileTelephone.focus();

        },
        afterrender: function () {
            if(ECO.Profile.isUserInRole(['2'])){
                ECO.cache.Global.setVoicePanel(this);
            }

           // this.lookupReference('voiceProfileImage').setSrc('https://storage.googleapis.com/lcmc-eco/10.png');
        }
        // showHideToolbarBtn: function (mainToolbar, isCollapsing) {
        //     ;
        //     (isCollapsing) ? this.fireEvent('collapse', mainToolbar) : this.fireEvent('expand', mainToolbar) ;
        //
        //
        // },
        // expand: function (mainToolbar) {
        //     ;
        //     mainToolbar.enable();
        //     this.expand()
        // },
        // collapse: function (mainToolbar) {
        //     ;
        //     mainToolbar.enable();
        //     this.collapse()
        // }
    },
    initComponent : function() {
        var me = this;

        Ext.apply(this, {
            // items: [{
            //     width: 300,
            //     html: [
            //         '<div class="voiceBase" id="audio_1"></div>'
            //     ]
            // }, {
            //     width: 300,
            //     html: [
            //         '<canvas class="voiceBase voice_canvas" id="audio_2"></canvas>'
            //     ]
            // },{
            //     width: 300,
            //     html:[
            //         '<div class="voiceBase"  id="audio_3"></div>'
            //     ]
            // }],

            items:[{
                xtype:'container',
                padding: 20,
                layout: {
                    type: 'vbox',
                    align: 'center',
                    pack: 'center'
                },
                items: [{
                    xtype: 'image',
                    reference:'voiceProfileImage',
                    cls: 'voiceProfilePic',
                    height: 80,
                    width: 80,
                    alt: 'profile-picture',
                    src: 'resources/images/user-profile/no-image.jpg'
                },{
                    xtype: 'displayfield',
                    fieldCls: 'voiceProfileName',
                    reference:'voiceProfileName',
                    value: 'Manual Calling',
                    style: {
                        "word-wrap": 'break-word'
                    },
                    renderer: function(value){
                        if(value.length > 25) {
                            value = value.substring(0,24)+"...";
                        }
                        return value;
                    }
                  //  maxWidth: 30
                },{
                    xtype:'form',
                    layout: 'hbox',
                    cls: 'voiceBase',
                    defaultFocus: 'voiceProfileTelephone',
                    defaultButton : 'voiceProfileTelephoneBtn',
                    items:[{
                        xtype:'textfield',
                        emptyText: 'Telephone',
                        reference:'voiceProfileTelephone',
                        name:'voiceTelephone',
                        maskRe: /[0-9.]/,
                        maxLength: 11,
                        minLength: 3,
                        enforceMaxLength: true,
                        allowBlank:false,
                        tabIndex: '-1',
                        clearIcon : true,
                        listeners: {
                            'change': function(rec){
                               // var a = me;
                              //  var me = this.up().up().up();
                                var lastPat = me.getLastPatient(),
                                    lastFac = me.getLastFacility(),
                                    lastNum = me.getLastNumber();

                                if(lastPat || lastFac){
                                    if(lastNum !== rec.value) {
                                        console.log('No match');
                                        me.setLastPatient(null);
                                        me.setLastFacility(null);

                                        ECO.cache.Global.updateVoicePanelProfile(false, 'Manual Calling', rec.value)
                                    }

                                }


                            }
                        }
                    },{
                        xtype:'button',
                        ui: 'soft-green',
                        reference:'voiceProfileTelephoneBtn',
                        iconCls: 'x-fa fa-phone',
                        disabled:true,
                        handler: 'onVoiceTelephoneBtnClick',
                        formBind: true
                    }]
                },{
                    xtype: 'displayfield',
                    fieldCls: 'voiceProfileStatus',
                    reference:'voiceProfileTelephoneStatusField',
                    name: 'voiceProfileTelephoneStatusField',
                    value: 'Not Connected'
                }]
            },{
                width: 300,
                html:[
                    '<div class="voiceBase"  id="audio_3"></div>'
                ]
            }]

        });
        this.callParent(arguments);
    }
});