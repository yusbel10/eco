Ext.define('ECO.view.apps.patients.children.InPatientTabs', {
    extend: 'Ext.tab.Panel',
    xtype: 'in-patient-tabs',
    reference:'in-patient-tabs',
    requires:[
        'ECO.view.apps.patients.children.contact.ContactInfo'
    ],
    session: {},
    bodyStyle: 'background-color: red !important;', //hope you like red smilie
    ui:'ProfileDetailTabs',


    //region initComponent
    initComponent: function(){
        var me = this,
            viewModelData = this.getViewModel().getData().patientRecord;

        Ext.apply(this, {
            listeners:{
                //   'tabchange': 'onTabChange'
            },
            items: [{
                title: 'Contact Info',
                name:'contact_info',
                iconCls: 'right-icon x-fa fa-info-circle',
                items: [{
                    xtype: 'patient-profile-contactinfo',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },

                    viewModel: {
                        data: {
                            patientRecord: viewModelData
                        }
                    }
                }]

            }, {
                title: 'Appointments',
                name:'appointments',
                iconCls: 'right-icon x-fa fa-calendar',
                items: [{
                    xtype: 'tabpanel',
                    tabPosition: 'left',
                    tabRotation: 0,
                    flex: 1,
                    height: 400,
                    ui: 'ProfileDetailTabs',
                    tabBar: {
                        // turn off borders for classic theme.  neptune and crisp don't need this
                        // because they are borderless by default
                        border: false
                    },

                    defaults: {
                        textAlign: 'left',
                        bodyPadding: 15
                    },
                    items: [{
                        title: 'All'
                    }, {
                        title: 'Internals'
                    }, {
                        title: 'Referrals'
                    }, {
                        title: 'Social'
                    }, {
                        title: 'Wellness'
                    }]
                }]
            },{
                title: 'Insurance',
                iconCls: 'right-icon x-fa fa-dollar'
            },{
                title: 'Logs',
                iconCls: 'right-icon x-fa fa-archive'
            }]

        });

        this.callParent(arguments);
    }
    //endregion

});