Ext.define('ECO.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',
    //TODO:


    onLoginButton: function(btn, e, eOpts) {
        var me = this,
            username = this.lookupReference('loginusername').lastValue.toLowerCase(),
            password = this.lookupReference('loginpassword').lastValue,
            remember = this.lookupReference('loginremember').lastValue,
            type   = (btn.name == 'loginButton') ? 0 : 1,
            params = {username: username, password: password, type: type};

        if(remember){
            localStorage.setItem('rememberLogin', true);
            localStorage.setItem('usernamefield', username);
        }else{
            localStorage.removeItem('rememberLogin');
            localStorage.removeItem('usernamefield');
        }



        var promise = ECO.helper.Func.callAjax('login', params, 'POST');

        promise.then(function (res) {
            if(res.success == true) {
                localStorage.setItem('token', Ext.encode(res.token));
                location.reload();
            } else{
                Ext.Msg.alert('Error', res.message);
                Ext.create('ECO.view.' + ('pages.Error500Window'),{ hideMode:'offsets', routeId: 'pages.Error500Window'});
            }
        }, function (value) {
        }).always(function(){
            console.log('Performed Login');
        });


    },

    onLockScreenLoginClick: function (btn, e, eOpts) {
        var me = this,
            token = Ext.decode(localStorage.getItem('token')),
            username = token.user[0].user_username,
            password = this.lookupReference('LockScreenPasswordField').lastValue,
            type   = (btn.name == 'loginButton') ? 0 : 1,
            params = {username: username, password: password, type: type};

        var promise = ECO.helper.Func.callAjax('login', params, 'POST');

        promise.then(function (res) {
            if(res.success == true) {
                localStorage.removeItem('token');
                localStorage.setItem('token', Ext.encode(res.token));
                location.reload();
            } else{
                Ext.Msg.alert('Error', res.message);
                Ext.create('ECO.view.' + ('pages.Error500Window'),{ hideMode:'offsets', routeId: 'pages.Error500Window'});
            }
        }, function (value) {
        }).always(function(){
            console.log('Performed Login');
        });
    }
});
