
Ext.define("ECO.helper.GridRenderer", {
    singleton : true,
    //region config
    config: {
        statics: {
            //In Chart

            getPhoneType: function(data){
                return this.__proto__.PhoneType[data];
            },
            getSatisfactionType: function (data) {
                return this.__proto__.SatisfactionType[data];
            },

            getSatisfactionStatus: function (data) {
                return this.__proto__.SatisfactionStatus[data];
            },
            getRisStatus:function (data){
                return this.__proto__.RisStatus[data];
            },
            getSex: function (data) {
                return this.__proto__.sex[data];
            },
            getRace: function (data) {
                return this.__proto__.race[data];
            },
            getRisAttachmentType:function(data){
                return this.__proto__.RisAttachmentType[data];
            },
            getUltrasoundType:function(data){
                return this.__proto__.ultrasoundType[data];
            },
            getUltrasoundTypeUrl:function(data){
                return this.__proto__.ultrasoundTypeUrl[data];
            },
            getPatientDocumentType:function(data){
                return this.__proto__.PatientDocumentType[data];
            },
            getInsuranceProvider: function (data) {
                return this.__proto__.InsuranceProvicer[data];
            },


            //In Chart

            PhoneType:{
                0:'Home',
                1:'Mobile',
                2:'Alternative'
            },

            SatisfactionType:{
                0:'Compliment',
                1:'Complaint'
            },

            SatisfactionStatus:{
                0:'Open',
                1:'Closed',
                2:'Canceled'
            },

            PatientDocumentType: {
                0:'Legal',
                1:'Patient Record',
                2:'Other'
            },

            RisStatus:{
                0:'Requested',
                1:'Ready To Dictate',
                2:'Dictated',
                3:'Transcribed',
                4:'Signed'
            },

            RisModality: {
                0:'Echo 2d',
                1:'CT',
                2:'DexaScan',
                3:'Mammography',
                4:'MRI',
                5:'Ultrasound',
                6:'XRay'
            },
            RisAttachmentType: {
                0:'Clinical Notes',
                1:'Dictation',
                2:'Report'
            },
            sex: {
                0: 'Male',
                1: 'Female'
            },
            race: {
                0:'White',
                1:'Black or African American',
                2:'American Indian or Alaskan Native',
                3:'Asian',
                4:'Native Hawaiian or other Pacific Islander'
            },

            ultrasoundType:{
                0:"Abdomen",
                1:"Genitourinary",
                2:"Thyroid",
                3:"Gynecologic",
                4:"Prostate"
            },

            ultrasoundTypeUrl:{
                0:"https://s3.amazonaws.com/lcmc.bucket.reserved/modalities/abdomen.jpg",
                1:"https://s3.amazonaws.com/lcmc.bucket.reserved/modalities/genitourinary.jpg",
                2:"https://s3.amazonaws.com/lcmc.bucket.reserved/modalities/thyroid.jpg",
                3:"https://s3.amazonaws.com/lcmc.bucket.reserved/modalities/gynecologic.jpg",
                4:"https://s3.amazonaws.com/lcmc.bucket.reserved/modalities/prostate.jpg"
            },
            InsuranceProvider:{
                0:"HealthSun",
                1:"Medicare",
                2:"Medicaid"
            }

        }
    },
    //endregion

    //region methods
    phoneTypeIntToString: function (data) {
        return this.config.statics.getPhoneType(data);
    },

    satisfactionTypeIntToString: function(data){
        return this.config.statics.getSatisfactionType(data);
    },

    satisfactionStatusIntToString: function (data) {
        return this.config.statics.getSatisfactionStatus(data);
    },

    risAttachentUrlToIcon:function(data){

    },
    sexFormat:function(data){
        return this.config.statics.getSex(data);
    },
    raceFormat:function(data){
        return this.config.statics.getRace(data);
    },

    ultrasoundType:function(data){
        return this.config.statics.getUltrasoundType(data);
    },

    ultrasoundTypeUrl:function(data){
        return this.config.statics.getUltrasoundTypeUrl(data);
    },

    weekNumberOfYear:function(year, month){
        var firstOfMonth = new Date(year, month-1, 1);
        var lastOfMonth = new Date(year, month, 0);

        var used = firstOfMonth.getDay() + lastOfMonth.getDate();

        return Math.ceil( used / 7);
    },

    risStatusConvert:function(data){
        return this.config.statics.getRisStatus(data);
    },

    risModalityConvert: function(data){
        return this.config.statics.getRisModality(data);
    },
    risAttachmentTypeConvert: function(data){
        return this.config.statics.getRisAttachmentType(data);
    },
    patAttachmentTypeConvert: function(data){
        return this.config.statics.getPatientDocumentType(data);
    },
    risAttachmentUrl:function(data){

    },
    insuranceProviderFormat:function(data){
        return this.config.statics.getInsuranceProvider(data);
    },
    //Function to convert userid to username
    userIdToUsername: function(data){
        //Get the all user data

        var store = (Ext.getStore('ECO.store.Users').getData());


        //Loop  until you match userid to username and return the username
        for(var i = 0; i < store.length; i++){
            var inArray = (data == store.items[i].data.usrid) ? store.items[i].data.username : false;

            if(inArray){
                return inArray;
            }
        }

        store = null;
    },

    phoneFormat: function(value){
        var arr = value.split('');
        if (arr.length == 7) {
            var first_part = arr.splice(0, 3);
            return first_part.join('') + '-' + arr.join('');
        } else if (arr.length == 10) {
            first_part = arr.splice(0, 3);
            var second_part = arr.splice(0, 3);
            return "(" + first_part.join('') + ") " + second_part.join('') + '-' + arr.join('');
        } else {
            return value;
        }
    },

    doctorIdToName: function(data){
        var doctors = (Ext.getStore('ECO.store.Doctors').getData());
        for(var i = 0; i < doctors.length; i++){

            if(data == doctors.items[i].data.doc_Id){
                return doctors.items[i].data.doc_name;
            }
        }
        doctors = null;
    },

    userIdToFirstLast: function(data){
        var store = (Ext.getStore('ECO.store.Users').getData());
        var storeLength = store.length;

        for(var i = 0; i < storeLength; i++){
            //var a = store.items[i].data.fac_Id;
            var inArray = (val == store.items[i].data.usrid) ? store.items[i].data.first_name + ' ' + store.items[i].data.last_name : false;

            if(inArray){
                return inArray;
            }
        }
    },

    userFacilityIdToName: function(data){
        var store = (Ext.getStore('ECO.store.Facilities').getData());

        for(var i = 0; i < store.length; i++){
            //var a = store.items[i].data.fac_Id;
            var inArray = (data == store.items[i].data.fac_Id) ? store.items[i].data.fac_name : false;

            if(inArray){
                return inArray;
            }
        }
        store = null;
    },

    risPriorityConvert: function(data){

        return (data == true) ? '<b><span style="color:' + "#cf4c35" + ';">' + 'STAT' + '</span></b>' : '<span style="color:' + "#73b51e" + ';">' + 'Normal' + '</span>';
    },

    ticketStatusConvert: function(data){

        //Get the all user data
        var store = Ext.create('Ext.data.Store', {
            storeId: 'clientsStore',
            fields: [{
                name: 'name',
                type: 'string'
            }, {
                name: 'id',
                type: 'int'
            }],
            data: {
                'items': [{
                    'name': 'Pending',
                    "id": 0
                }, {
                    'name': 'Open',
                    "id": 1
                }, {
                    'name': 'Closed',
                    "id": 2
                }]
            },
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'items'
                }
            }
        });


        //Loop  until you match userid to username and return the username
        for(var i = 0; i < store.data.length; i++){
            var inArray = (data == store.data.items[i].id) ? store.data.items[i].data.name : false;

            if(inArray){
                return inArray;
            }
        }

        store = null;
    },

    dobToAge: function(data){
        var today = new Date();
        var birthDate = new Date(data);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    },

    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    }
    //endregion

});