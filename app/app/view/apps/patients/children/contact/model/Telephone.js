Ext.define('ECO.view.apps.patients.children.contact.model.Telephone', {
    extend: 'Ext.data.Model',
    fields:[
        {name: 'tele_id', type: 'int'},
        {name: 'tele_create_date', type: 'date', dateFormat: 'Y-m-d H:i:s',
            convert: function(val, rec){
                return Ext.util.Format.date(val, 'm/d/Y');
                //return Ext.util.Format.date(val, 'Y-m-d');

            }},
        {name: 'tele_type', type:'int'},
        {name: 'tele_number', type: 'string',
            convert: function (val, rec) {
                if(!val || val == null){
                    return '';
                }
                return (/^[\d\+\-\(\) ]+$/.test(val)) ? val : 'Wrong Format';
            }},
        {name: 'tele_comment', type: 'string'},
        {name: 'tele_primary', type: 'boolean'},
        {name: 'tele_active', type: 'bool'}

    ]
});
