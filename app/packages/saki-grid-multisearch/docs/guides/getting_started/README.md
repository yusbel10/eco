# Getting Started Guide


There are two basic scenarios of using the plugin in a project:

1. as a package in workspace
2. as a package in application

Both are very easy to use.

##  Using in workspace

Step by step instructions:

### 1.1 Create the workspace

    cd /your/ext/path
    sencha generate workspace /your/ws

### 1.2 Extract the plugin zip file
Download the plugin zip file to <code>packages</code> subfoder of your workspace. Then:

    cd /your/ws/packages
    unzip saki-grid-multisearch-x.y.z.zip

where x.y.z stands for the downloaded file version

### 1.3 Build the examples (optional)
The included examples must be built before you can run them. To build execute:

    cd /your/ws/packages/saki-grid-multisearch/examples/gms
    sencha app build

You can see the examples at http://localhost/your/ws/packages/saki-grid-multisearch/examples/gms

### 1.4 Create your application (optional)
You would need this step if you do not already have an Ext application.

    cd /your/ws/ext
    sencha generate app MyApp ../myapp

This would generate application with name "MyApp" in folder <code>/your/ws/myapp</code>.

### 1.5 Add the package name to requires array in app.json
The example of app.json:

    {
        "name": "MyApp",
        "theme":"ext-theme-gray",
        "requires": [
            "saki-grid-multisearch"
        ],
        "id": "cb5b1c88-0f95-4da7-b763-00bc8998b770"
    }

### 1.6 For grid configuration see the documentation
See the {@link Ext.saki.grid.MultiSearch MultiSearch Documentation} to find out how to configure
your grids to use the MultiSearch plugin. Do not forget to add the plugin to grid's requires array.

    Ext.define('MyApp.view.MyGrid', {
         extend:'Ext.grid.Panel'
        ,requires:['Ext.saki.grid.MultiSearch']
    });


## Using in application

Step by step instructions:

### 2.1 Create your application (optional)
You would need this step if you do not already have an Ext application.

    cd /your/ext/path
    sencha generate app MyApp /your/myapp

This would generate application with name "MyApp" in folder <code>/your/myapp</code>.


### 2.2 Extract the plugin zip file
Download the plugin zip file to <code>packages</code> subfoder of your application. Then:

    cd /your/myapp/packages
    unzip saki-grid-multisearch-x.y.z.zip

where x.y.z stands for the downloaded file version

### 2.3 Build the examples (optional)
The included examples must be built before you can run them. To build execute:

    cd /your/myapp/packages/saki-grid-multisearch/examples/gms
    sencha app build

You can see the examples at http://localhost/your/myapp/packages/saki-grid-multisearch/examples/gms

### 2.4 Add the package name to requires array in app.json
The example of app.json:

    {
        "name": "MyApp",
        "theme":"ext-theme-gray",
        "requires": [
            "saki-grid-multisearch"
        ],
        "id": "cb5b1c88-0f95-4da7-b763-00bc8998b770"
    }

### 2.5 For grid configuration see the documentation
See the {@link Ext.saki.grid.MultiSearch MultiSearch Documentation} to find out how to configure
your grids to use the MultiSearch plugin. Do not forget to add the plugin to grid's requires array.

    Ext.define('MyApp.view.MyGrid', {
         extend:'Ext.grid.Panel'
        ,requires:['Ext.saki.grid.MultiSearch']
    });

