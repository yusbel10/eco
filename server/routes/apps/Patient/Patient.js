var Similar = require('../../Helper/Similar'),
    Converters = require('../../Helper/Converters'),
    fs = require("fs"),
    gCloudKeyFileName ='config/lcmc-9a0e5d436984.json';
  

var pat = {
    get: function (req, res) {

        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection;

        var limit = parseInt(req.query.limit),
            page  = parseInt(req.query.page),
            start = parseInt(req.query.start),
            filter= req.query.filter,
            whereQuery = "";
           // totalCount = "(select count(*) FROM eco_patient";

        if(filter){
            var active = "",
                chart  = "",
                fname  = "",
                lname  = "",
                dob    = "";

            var Array = JSON.parse(filter);

            for(var i=0; i<Array.length; i++){
                switch (Array[i].property){
                    case "pat_active_status":
                        active = (Array[i].value.replace(/\s/g,'') == "Active" ? 0 : 1);
                        whereQuery += Array[i].property + " = " + active + "";
                        break;

                    case "pat_chart_number":
                        chart = Array[i].value;
                        whereQuery += Array[i].property + " LIKE '%" + chart + "%'";
                        break;

                    case "pat_first_name":
                        fname = Array[i].value;
                        whereQuery += Array[i].property + " LIKE '%" + fname + "%'";
                        break;

                    case "pat_last_name":
                        lname = Array[i].value;
                        whereQuery += Array[i].property + " LIKE '%" + lname + "%'";
                        break;

                    case "pat_dob":
                        dob = Array[i].value;
                      //  var nDob = Converters.date(dob);
                        whereQuery += Array[i].property+ " = " + "'"+dob+"'";
                        break;


                    default:
                        break;
                }

                if(i != (Array.length) - 1){
                    whereQuery += " AND ";
                }
            }


            MysqlKnex.select('pat_id', 'pat_picture', 'pat_active_status', 'pat_chart_number',
                'pat_first_name', 'pat_last_name', 'pat_middle_name',
                'pat_sex', 'pat_dob', 'tele_number AS pat_tele', function () {
                    this.count('*').from('eco_patient').as('totalCount').whereRaw(whereQuery)
                })
                .leftJoin('eco_telephone', function () {
                    this.on('eco_patient.pat_id', '=', 'eco_telephone.tele_patient_id').andOn('eco_telephone.tele_primary', '=', 1)
                })
                .from('eco_patient')
                .whereRaw(whereQuery)
                .limit(limit).offset(start)
                .then(function (response) {

                    var count = (response.length > 0) ? response[0].totalCount :  0;
                    res.json({ success: true, data: response, totalCount: count });
                }).catch(function(error) {
                    res.json ({ success: false, code: 503, message: "Error in connection database" });
            });

        }else{
            MysqlKnex.select('pat_id', 'pat_picture', 'pat_active_status', 'pat_chart_number',
                        'pat_first_name', 'pat_last_name', 'pat_middle_name',
                        'pat_sex', 'pat_dob', 'tele_number AS pat_tele', function () {
                    this.count('*').from('eco_patient').as('totalCount')
                })
                .leftJoin('eco_telephone', function () {
                    this.on('eco_patient.pat_id', '=', 'eco_telephone.tele_patient_id').andOn('eco_telephone.tele_primary', '=', 1)
                })
                .from('eco_patient')
                .limit(limit).offset(start)
                .then(function (response) {
                    res.json({ success: true, data: response, totalCount:response[0].totalCount });
                }).catch(function(error) {
                    res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }

    },
    post: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            fname = (req.body.patInfo._firstname || req.body.patInfo.pat_first_name),
            lname = (req.body.patInfo._lastname || req.body.patInfo.pat_last_name),
            mi    = (req.body.patInfo._mi || req.body.patInfo.pat_middle_name),
            sex   = (req.body.patInfo._sex || req.body.patInfo.pat_sex),
            dob = (req.body.patInfo._dob || req.body.patInfo.pat_dob),
            phone = (req.body.patInfo._phone || req.body.patInfo.tele_number),
            tele_type = req.body.patInfo.tele_type,
            ssn     = (req.body.patInfo.pat_ssn || req.body.patInfo._ssn),
            street  = req.body.address.street,
            city    = req.body.address.city,
            apt     = req.body.address.apt,
            zip     = req.body.address.zip,
            lat     = req.body.address.lat,
            lng     = req.body.address.lng,
            pat_id  = null,
            chart    = null,
            userid  = req.body.userid,
            tmpDob  = dob.split('-'),
            tmpSex  = (sex == 0) ? 'M' : 'F',
            pat_openerp_id = (req.body.patInfo.pat_openerp_id || null);




        if(!fname || !lname || !dob || !phone || !sex.toString() || !street || !city || !zip || !userid || !ssn){
            res.json ({ success: false, code: 400, message: "Missing data"});
            return;
        }




        MysqlKnex('eco_patient')
            .insert({pat_creator_id: userid, pat_ssn: ssn, pat_first_name: fname, pat_middle_name: mi, pat_last_name: lname, pat_dob: dob, pat_sex: sex, pat_openerp_id: pat_openerp_id,  })
            .returning('pat_id')
            .then(function (response) {
                pat_id = response[0];
                chart  = tmpDob[0] + tmpSex + pat_id;
                MysqlKnex('eco_patient')
                    .where('pat_id', '=', pat_id)
                    .update({ pat_chart_number: chart })
                    .then(function (response) {
                        MysqlKnex('eco_address')
                            .insert({ addr_creator_id: userid, addr_patient_id: pat_id, addr_street: street, addr_apartment: apt, addr_zipcode: zip, addr_city: city, addr_lat: lat, addr_lng: lng, addr_primary: 1 })
                            .then(function (response) {
                                MysqlKnex('eco_telephone')
                                    .insert({ tele_creator_id: userid, tele_type: tele_type, tele_number: phone, tele_patient_id: pat_id, tele_primary: 1 })
                                    .then(function (response) {
                                        res.json({  success: true,
                                                    pat_id: pat_id,
                                                    pat_chart_number: chart,
                                                    pat_first_name: fname,
                                                    pat_last_name: lname,
                                                    pat_middle_name: mi,
                                                    pat_dob: dob,
                                                    pat_sex: sex,
                                                    pat_ssn: ssn,
                                                    pat_active_status: 1,
                                                    pat_openerp_id: pat_openerp_id
                                        });
                                    })
                            })
                    })



            }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
        });
    },
    put: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            params = req.body;

        params.DOB = Converters.date(params.DOB);

        if(params){
            MysqlKnex('eco_patient').where('pat_id', params.pat_id)
                .update({
                    pat_first_name: params.FirstName,
                    pat_middle_name: (params.MI == '') ? null : params.MI,
                    pat_last_name: params.LastName,
                    pat_email: params.email,
                    pat_dob: params.DOB,
                }).then(function (response) {
                res.json({ success: true });
            }).catch(function (error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }


    },
    delete: function (req, res) {

    },
    getProfile: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection;

        var patId = (req.query.pat_id) || null;

        if(patId){
            MysqlKnex('eco_patient')
                .leftJoin('eco_insurance', function () {
                    this.on('eco_patient.pat_id', '=', 'eco_insurance.insu_patient_id').andOn('eco_insurance.insu_provider', '=', 0)
                })
                .where('pat_id', patId)
                .then(function (response) {
                    res.json({ success: true, data: response });
            }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 400, message: "Missing data"});
        }

    },
    editProfileImage: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            tmp_path = req.files.images.path,
            fileName = req.files.images.name,
            domain = req.body.domain,
            script = '<script type="text/javascript">document.domain = "' + domain + '";</script>',
            buffer = fs.readFileSync(req.files.images.path),
            config = {
                keyFilename: gCloudKeyFileName,
                projectId: 'lcmc-1091'
            };

        var gcs = require('@google-cloud/storage')(config);

        var bucket = gcs.bucket('lcmc-eco');


        var gcsname = 'patient/profile_images/' + Date.now() + fileName;
        var file = bucket.file(gcsname);

        var stream = file.createWriteStream({
            metadata: {
                contentType: req.files.images.type
            }
        });


        stream.on('error', function (err) {
            req.files.cloudStorageError = err;
            next(err);
        });

        stream.on('finish', function () {
            req.files.cloudStorageObject = gcsname;
            req.files.cloudStoragePublicUrl = Converters.gCloudUrl(gcsname);

            MysqlKnex('eco_patient').where('pat_id', req.body.pat_id)
                .update({
                    pat_picture: req.files.cloudStoragePublicUrl
                }).then(function (response) {

                //Issue with EXTJS FILE UPLOAD so we need to send a respond like this in order to get a success call back after submit

                var body = '<body>' + JSON.stringify({success: true, pat_picture: req.files.cloudStoragePublicUrl }) + '</body>';

                res.send('<html><head>' + script + '</head>' + body + '</html>');

            }).catch(function (error) {
                //Issue with EXTJS FILE UPLOAD so we need to send a respond like this in order to get a success call back after submit

                var body = '<body>' + JSON.stringify({ success: false, code: 503, message: "Error in connection database"}) + '</body>';

                res.send('<html><head>' + script + '</head>' + body + '</html>');
            });


        });

        stream.end(buffer);

        fs.unlink(tmp_path);



    },

    updateNote: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection;

        var patId = (req.body.pat_id) || null,
            note = (req.body.pat_note) || null;

        if(patId){

            MysqlKnex('eco_patient').where('pat_id', patId)
                .update({
                    pat_notes: note
                }).then(function (response) {
                res.json({ success: true });
            }).catch(function (error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 400, message: "Missing data"});
        }

    },

    getAllTelephone: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patId = (req.query.pat_id) || null,
            limit = parseInt(req.query.limit),
            page  = parseInt(req.query.page),
            start = parseInt(req.query.start);

        if(patId){

            MysqlKnex.select('*', function(){
                this.count('*').from('eco_telephone').as('totalCount')})
                .from('eco_telephone')
                .limit(limit).offset(start)
                .where('tele_patient_id', patId)
                .andWhere('tele_active', 1)
                .then(function (response) {
                    res.json({ success: true, data: response });
                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 400, message: "Missing data"});
        }


    },

    getAllAddress: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patId = (req.query.pat_id) || null,
            limit = parseInt(req.query.limit),
            page  = parseInt(req.query.page),
            start = parseInt(req.query.start);

        if(patId){
            MysqlKnex.select('*', function(){
                this.count('*').from('eco_address').as('totalCount')})
                .from('eco_address')
                .limit(limit).offset(start)
                .where('addr_patient_id', patId)
                .andWhere('addr_active', 1)
                .then(function (response) {
                    res.json({ success: true, data: response });
                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 400, message: "Missing data"});
        }
    },

    createPatientSearch: function (req, res) {
		var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            PostgresqlKnex = global.App.postgreSQlDatabase.postgresConnection,
            fname = req.query._firstname,
            lname = req.query._lastname,
            dob = req.query._dob,
            ssn = req.query._ssn,
            ssnFormat = '',
            phone = req.query._phone,
            sensitivity = 35,
            tmpArray =[],
            limit = parseInt(req.query.limit),
            page  = parseInt(req.query.page),
            start = parseInt(req.query.start),
            res1 = null,
            res2 = null;

        if(!fname || !lname || !dob || !phone){
            res.json ({ success: false, code: 400, message: "Missing data"});
            return;
        }

        // var tmpSSN = ssn.replace(/\D/g, ''),
        //     char = {0:'',3:'-',5:'-'};


        // for (var i = 0; i < tmpSSN.length; i++) {
        //     ssnFormat+= (char[i]||'') + ssn[i];
        // }

        ssnFormat = ssn.replace(/(\d{3})(\d{2})(\d{4})/, '$1-$2-$3');

        MysqlKnex.select('*', function(){
            this.count('*').from('eco_patient').as('totalCount')})
            .leftJoin('eco_telephone', function () {
                this.on('eco_patient.pat_id', '=', 'eco_telephone.tele_patient_id').andOn('eco_telephone.tele_primary', '=', 1)
            })
            .from('eco_patient')
            .limit(limit).offset(start)
            .where('pat_dob', dob)
            .orWhere('pat_ssn', ssn)
            .then(function (_res1) {
                res1 = _res1;
				PostgresqlKnex.select('*', function () {
                    this.count('*').from('ts_patient').as('totalCount')})
                    .from('ts_patient')
                    .limit(limit).offset(start)
                    .where('dob', dob)
                    .orWhere('ssn', ssnFormat)
                    .then(function (_res2) {
                        res2 = _res2;
						for(var i=0; i<res1.length; i++){
							var isFname = Similar(fname, res1[i].pat_first_name);
							var isLName = Similar(lname, res1[i].pat_last_name);

							//  console.log('OriginalFirst: '+fname + ', OriginalLast: ' + lname + ' ----- FoundFirst: ' +rows[i].pat_first_name + ' FoundLast: ' +rows[i].pat_last_name+ ' -----  ' + 'firrst: '+isFname + ' last: '+isLName);

							if(isFname >= sensitivity || isLName >= sensitivity){
								res1[i].db = 1;
								tmpArray.push(res1[i]);
							}
						}

						for(var i=0; i<res2.length; i++){
							var isFname = Similar(fname, res2[i].first_name);
							var isLName = Similar(lname, res2[i].last_name);

							if(isFname >= sensitivity || isLName >= sensitivity || ssnFormat == res2[i].ssn){
								serializeDB(res2, i);
								tmpArray.push(res2[i]);
							}
						}

						res.json({ success: true, data: tmpArray });

					}).catch(function(error) {
					    if(res1){
                            for(var i=0; i<res1.length; i++){
        						var isFname = Similar(fname, res1[i].pat_first_name);
        						var isLName = Similar(lname, res1[i].pat_last_name);
        
        						//  console.log('OriginalFirst: '+fname + ', OriginalLast: ' + lname + ' ----- FoundFirst: ' +rows[i].pat_first_name + ' FoundLast: ' +rows[i].pat_last_name+ ' -----  ' + 'firrst: '+isFname + ' last: '+isLName);
        
        						if(isFname >= sensitivity || isLName >= sensitivity){
        							res1[i].db = 1;
        							tmpArray.push(res1[i]);
        						}
        					}
        					res.json({ success: true, data: tmpArray });
                            return;
                        }
					res.json ({ success: false, code: 503, message: "Error in connection PostgeSQL database"});
				});

            }).catch(function(error) {
                if(res2){
			        for(var i=0; i<res2.length; i++){
			            var isFname = Similar(fname, res2[i].first_name);
					    var isLName = Similar(lname, res2[i].last_name);
					    
					    if(isFname >= sensitivity || isLName >= sensitivity || ssnFormat == res2[i].ssn){
							serializeDB(res2, i);
							tmpArray.push(res2[i]);
						}
			        }
			        
			        res.json({ success: true, data: tmpArray });
			        return;
			    }
                
            res.json ({ success: false, code: 503, message: "Error in connection database"});
        });
        
        function genderConvert(gender){
			return (gender == "female") ? 1 : 0;
		}

		function serializeDB(res2, i){
			//Have to serialize postgres / mysql data
			res2[i].db = 2;
			res2[i].pat_openerp_id = res2[i].id;
			res2[i].pat_first_name = res2[i].first_name;
			res2[i].pat_last_name = res2[i].last_name;
			res2[i].pat_middle_name = res2[i].middle_name;
			res2[i].pat_dob = res2[i].dob;
			res2[i].pat_ssn = res2[i].ssn;
			res2[i].pat_sex = genderConvert(res2[i].gender);
            res2[i].tele_type = 0;
			res2[i].tele_number = (res2[i].home_phone) ? res2[i].home_phone : res2[i].mobile_phone;
			res2[i].pat_chart_number = res2[i].previous_file_number;
			res2[i].pat_active_status = (res2[i].patient_active);
			res2[i].addr_street = res2[i].street;
			res2[i].addr_city = res2[i].city;
			res2[i].addr_apartment = res2[i].street2;
			res2[i].addr_zipcode = res2[i].zip;

            if(res2[i].home_phone){
                res2[i].tele_type = 0
            }else if(res2[i].mobile_phone) {
                res2[i].tele_type = 1
            }
		}

    },

    addEditPhone: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patId = req.body.tele_pat_id,
            comment = req.body.tele_comment,
            type = req.body.tele_type,
            number = req.body.tele_number,
            userid = req.body.userid,
            teleid = req.body.tele_id;


        if(!teleid){
            MysqlKnex('eco_telephone')
                .insert({tele_creator_id: userid, tele_type: type, tele_number: number, tele_comment: comment, tele_patient_id: patId})
                .then(function (response) {
                    res.json({ success: true });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            MysqlKnex('eco_telephone')
                .where('tele_id', '=', teleid)
                .update({ tele_type: type, tele_number: number, tele_comment: comment })
                .then(function (response) {
                    res.json({ success: true });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }


    },
    removePhone: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patId = req.body.tele_pat_id,
            userid = req.body.userid,
            teleid = req.body.tele_id;


        if(teleid){
            MysqlKnex('eco_telephone')
                .where('tele_id', '=', teleid)
                .update({ tele_active: 0 })
                .then(function (response) {
                    res.json({ success: true });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 422, message: "Missing information"});
        }

    },
    markPhonePrimary: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patId = req.body.tele_pat_id,
            userid = req.body.userid,
            teleid = req.body.tele_id;


        if(teleid){
            MysqlKnex('eco_telephone')
                .where('tele_patient_id', '=', patId)
                .update({ tele_primary: 0 })
                .then(function (response) {
                    MysqlKnex('eco_telephone')
                        .where('tele_id', '=', teleid)
                        .update({ tele_primary: 1 })
                        .then(function (response) {
                            res.json({ success: true });
                        }).catch(function(error) {
                        res.json ({ success: false, code: 503, message: "Error in connection database"});
                    });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 422, message: "Missing information"});
        }

    },

    addEditAddress: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patid = req.body.addr_patient_id,
            apt = req.body.addr_apartment,
            city = req.body.addr_city,
            lat = req.body.addr_lat,
            lng = req.body.addr_lng,
            street = req.body.addr_street,
            zip = req.body.addr_zipcode,
            comment = req.body.addr_comment,
            userid = req.body.userid,
            addid = (req.body.addid || req.body.addr_id || null);


        if(!patid || !city || !street || !zip){
            res.json ({ success: false, code: 400, message: "Missing data"});
            return;
        }


        if(!addid){


            MysqlKnex('eco_address')
                .insert({ addr_creator_id: userid, addr_street: street, addr_city: city, addr_apartment: apt, addr_zipcode: zip, addr_comment: comment, addr_patient_id: patid, addr_lat: lat, addr_lng: lng })
                .then(function (response) {
                    res.json({ success: true });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            MysqlKnex('eco_address')
                .where('addr_id', '=', addid)
                .update({ addr_street: street, addr_city: city, addr_apartment: apt, addr_zipcode: zip, addr_comment: comment, addr_lat: lat, addr_lng: lng })
                .then(function (response) {
                    res.json({ success: true });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }
    },

    removeAddress: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            userid = req.body.userid,
            addid = (req.body.addid || req.body.addr_id || null);


        if(addid){
            MysqlKnex('eco_address')
                .where('addr_id', '=', addid)
                .update({ addr_active: 0 })
                .then(function (response) {
                    res.json({ success: true });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 422, message: "Missing information"});
        }

    },

    markAddressPrimary: function (req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            patid = req.body.addr_patient_id,
            userid = req.body.userid,
            addid = (req.body.addid || req.body.addr_id || null);


        if(addid){
            MysqlKnex('eco_address')
                .where('addr_patient_id', '=', patid)
                .update({ addr_primary: 0 })
                .then(function (response) {
                    MysqlKnex('eco_address')
                        .where('addr_id', '=', addid)
                        .update({ addr_primary: 1 })
                        .then(function (response) {
                            res.json({ success: true });
                        }).catch(function(error) {
                        res.json ({ success: false, code: 503, message: "Error in connection database"});
                    });

                }).catch(function(error) {
                res.json ({ success: false, code: 503, message: "Error in connection database"});
            });
        }else{
            res.json ({ success: false, code: 422, message: "Missing information"});
        }

    }
};


module.exports = pat;