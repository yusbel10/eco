var twilio = require('twilio');
var config = require('../../config/voice_config.js');
var voice = {
    token: function(req, res) {
        console.log('Requested voice token');

        var identity = "YusbelRojas";

        var capability = new twilio.Capability(config.TWILIO_ACCOUNT_SID,
            config.TWILIO_AUTH_TOKEN);
        capability.allowClientOutgoing(config.TWILIO_TWIML_APP_SID);
        capability.allowClientIncoming(identity);
        var token = capability.generate();

        res.json({
            identity: identity,
            token: token,
            success: true
        });
        // var allusers = data; // Spoof a DB call
        //res.json(allusers);

        //query.querydb("a");
    },

    voice: function (req, res) {
        var twiml = new twilio.TwimlResponse();

        var to = '7862416381';

        if(to) {
            twiml.dial({ callerId: config.TWILIO_CALLER_ID}, function() {
                // wrap the phone number or client name in the appropriate TwiML verb
                // by checking if the number given has only digits and format symbols
                if (/^[\d\+\-\(\) ]+$/.test(to)) {
                    this.number(to);
                } else {
                    this.client(to);
                }
            });
        } else {
            twiml.say("Thanks for calling!");
        }
    }
};

module.exports = voice;