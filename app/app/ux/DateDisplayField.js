Ext.define('ECO.ux.DateDisplayField', {
    extend: 'Ext.form.field.Display',
    alias: 'widget.datedisplayfield',

    datePattern: 'Y-m-d',

    valueToRaw: function(value) {
        return Ext.util.Format.date(value, this.datePattern);
    }
});