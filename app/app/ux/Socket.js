var socket;
Ext.define("ECO.ux.Socket", {
    requires: [

    ],

    draggingRecord      : null,
    socketHost          : 'localhost:3001',

    config: {
        username   : null
    },

    isUserConnected: function () {
        return this.socket.connected;
    },


    constructor: function(config) {
        this.initConfig(config);
        var me= this;

        //create a WebSocket and connect to the server running at host domain

        try{
            socket = me.socket = io.connect(me.socketHost);
            if(!socket.connected){
                //  showToast('Connection Failed', Ext.String.format("Unable to connect to the chat server"));

            }
        }

        catch (ex){
           // ECO.ShowToast.show(Ext.String.format("Unable to connect to the chat server"));
        }

        socket.emit('username', config.username, function(data){

            if(!data){
                socket.disconnect();
            }
        });



        this.callParent(arguments);


    }


});