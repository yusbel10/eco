
Ext.define('ECO.view.apps.patients.PatientsMainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox',
        'Ext.layout.container.Card',
        'Ext.grid.feature.Grouping'
        // 'ECO.ux.DateDisplayField'
    ],

    alias: 'controller.patients',

    config: {
        foundPatient: false,
        foundAddress: false,
        selectAddress:false,
        app_chart_tab: false,
        foundPatientData: null

    },

    listeners:{
        openSelectedPatient:  'openPatient'
    },

    //region loadAllGridStores
    loadAllGridStores: function () {

        var me = this,
            patientStore = Ext.create('Ext.data.Store', {
                autoLoad: false,
                remoteFilter: true,
                sortOnLoad: true,
                pageSize: 25,
                remoteSort: false,
                simpleSortMode: true,
                model: 'ECO.view.apps.patients.model.Patient',
                proxy: {
                    type: 'rest',
                    url: ECO.app.globals.url+'v1/patient',
                    reader: {
                        type: 'json',
                        rootProperty: 'data',
                        successProperty: 'success',
                        totalProperty: 'totalCount'
                    }
                },
                listeners:{
                    filterchange: function (store, filters, eOpts) {

                    },
                    beforeload: function (store, op, eOpts) {

                    },

                    load: function (e, rec, success, op, eOpts) {
                        var refs = me.getReferences(),
                            patientGrid = refs.patientGrid;

                        patientGrid.up().updateLayout();
                    }
                }
            });

        patientStore.load({

            callback: function (data) {
            }
        });



        this.getView().setPatientStore(patientStore);
    },
    //endregion

    //region loadAllColumns
    loadAllColumns: function () {

        var ActiveStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
            data: {
                'items': [{
                    'name': 'Active',
                    "id": 0
                }, {
                    'name': 'Inactive',
                    "id": 1
                }]
            },  proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'items'
                }
            }

        });

        var patientColumn = [{
            sortable: false,
            filterField:false,
            dataIndex: 'pat_picture',
            renderer: function(value) {
                if(!value){
                    value = 'resources/images/user-profile/no-image.jpg';
                }
                return "<img src='" + value + "' alt='Patient Picture' height='40px' width='40px'>";
            },
            width:75
        },{
            dataIndex: 'pat_active_status',
            //text: '<span class="x-fa fa-exclamation-triangle"></span>',
            text:'Active',
            //width: 50,
            flex:1,
            renderer: function(value) {
                // return value ? '<span class="x-fa fa-exclamation-triangle"></span>' : '';
                return (value) ? '' : '<font color="red">Inactive';
            },
            filterField:{
                xtype:'combobox',
                publishes: 'value',
                valueField:'id',
                displayField: 'name',
                minChars: 0,
                queryMode: 'local',
                store: ActiveStatusStore,
                emptyText : ''
            }
        },{
            dataIndex: 'pat_chart_number',
            text: 'Chart',
            flex: 1,
            filterField:true
        },{
            dataIndex: 'pat_first_name',
            text: 'First',
            flex: 1,
            filterField:true
        },{
            dataIndex: 'pat_last_name',
            text: 'Last',
            flex: 1,
            filterField:true
        },{
            xtype: 'datecolumn',
            dataIndex: 'pat_dob',
            text: 'DoB',
            flex: 1,

           // format: 'm/d/Y',
            filterField:{
                xtype: 'datefield',
                format: 'Y-m-d',
                submitFormat: 'Y-m-d'
            },

            renderer: function (value, metaData, record, row, col, store, gridView) {
                return Ext.util.Format.date(value, 'm/d/Y');
            }
        },{
            dataIndex:'pat_tele',
            text: 'Phone',
            flex:1
        },{
            xtype: 'widgetcolumn',
            hidden: !ECO.Profile.isUserInRole(['2']),
            width: 55,
            widget: {
                ui:'soft-green',
                // bind: '{record.progress}',
                xtype: 'button',
                iconCls: 'x-fa fa-phone',
                handler: 'onCallPatientBtnClick'
            }
        }];


        this.getView().setPatientColumn(patientColumn);
    },
    //endregion

    //region openPatient
    openPatient: function(rec){
        var me = this;
        if(rec){

            ECO.helper.Func.callAjax('v1/patient/profile',{ pat_id: rec.pat_id },'GET').then(function (content) {
                var data = content.data[0];
                me.createTab('apps', data, {
                    xtype: 'in-patient-main-container',
                    session: true,
                    title: data.pat_first_name + ' ' + (data.pat_middle_name || '') + ' ' + data.pat_last_name,
                    viewModel: {
                        data: {
                            patientRecord:data
                        }
                    }
                });
            });


        }
    },
    //endregion

    //region createTab
    createTab: function (prefix, rec, cfg) {
        var type = this.type;
        var tabs = this.getView(),
            id = (rec != null) ? prefix + '_' + rec.pat_id : null,
            tab = tabs.items.getByKey(id);

        if (!tab) {
            cfg.itemId = id;
            cfg.closable = true;
            cfg.tabConfig = { maxWidth: 120 };
            cfg.type = type;
            tab = tabs.add(cfg);
        }

        tabs.setActiveTab(tab);
    },
    //endregion

    //region onCallPatientBtnClick
    onCallPatientBtnClick: function (btn, event) {
        var me = this,
            //  rowIndex = btn.up('gridview').indexOf(btn.el.up('table')),
            data = btn.getWidgetRecord().getData();


        if(ECO.Profile.isUserInRole(['2'])){

            if (!data.pat_tele || data.pat_tele == "" || data.pat_tele == null){ return }
            var voicePanel = ECO.cache.Global.getVoicePanel(),
                voiceRefs = voicePanel.getReferences(),
                isCollapse = voicePanel.getCollapsed();


            voicePanel.setLastPatient((data.pat_id) || null);
            voicePanel.setLastNumber((data.pat_tele) || null);

            ECO.cache.Global.updateVoicePanelProfile(data.pat_picture, data.pat_first_name + ' ' + data.pat_last_name, data.pat_tele);



            if(isCollapse){
                if(!ECO.cache.Global.getSvg()){ ECO.cache.Global.createVisualizers(false); }

                voicePanel.expand();
            }

        }
    },
    //endregion

    //region onAddPatientBtnClick
    onAddPatientBtnClick: function(btn, event) {
        var me = this;
        this.config.foundPatientData = null;

        this.config.app_chart_tab = this.getView();

        //Create Temp Store for the saving of gender
        var patSexStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
            data: [
                {"id": 0, "sex": "0", "name": "Male"},
                {"id": 1, "sex": "1", "name": "Female"}
            ]
        });

        var win = new Ext.Window({
            applyTo: me,
            layout: 'fit',
            resize: false,
            modal: true,
            width: 755,
            height: 460,
            closable: false,
            header: false,
            frame: true,
            closeAction: 'close',
            plain: true,
            items: [{
                xtype: 'panel',
                layout: 'card',
                cls: 'forms-patient-dynamic-window',
                bodyPadding: 15,
                defaults: {
                    border: false
                },
                buttons: [{
                    itemId: 'card-prev',
                    text: '&laquo; Previous',
                    handler: function () {
                        var foundPatient = me.config._foundPatient;
                        var view = win,
                            l = view.items.items[0].getLayout(),
                            i = l.activeItem.id.split('card-')[1];

                        view.down('#card-next').setText('Next &raquo;');

                        if (!foundPatient) {
                            if (i == 2) {
                                me.config.foundPatientData = false;
                                me.doCardNavigation(-2, view);
                            } else {
                                me.doCardNavigation(-1, view);
                            }
                        } else {
                            me.doCardNavigation(-1, view);
                        }
                    },
                    disabled: true
                }, {
                    itemId: 'card-next',
                    Id: 'card-next',
                    text: 'Next &raquo;',
                    handler: function () {
                        var nMe = this,
                            view = win,
                            l = view.items.items[0].getLayout(),
                            showNext = false;
                        var i = l.activeItem.id.split('card-')[1];

                        //If on search patient card
                        if (i == 0) {
                            showNext = false;
                            var basicForm = win.down('#basicForm');
                            var basicFormValues = basicForm.getForm().getValues();
                            if (!basicForm.isValid()) {
                                Ext.Msg.alert('Invalid', 'Invalid Fields');
                                return;
                            }

                            var mask = new Ext.LoadMask({msg: "Please Wait...", target: win});
                            mask.show();

                            var patientStore = Ext.create('Ext.data.Store', {
                                fields: [
                                    {name: 'pat_id', type: 'int'},
                                    {name: 'pat_picture', type: 'string'},
                                    {name: 'pat_active_status', type: 'boolean'},
                                    {name: 'pat_chart_number', type: 'string'},
                                    {name: 'pat_first_name', type: 'string'},
                                    {name: 'pat_last_name', type: 'string'},
                                    {
                                        name: 'tele_number', type: 'string',
                                        convert: function (val, rec) {
                                            if (!val) {
                                                return
                                            }
                                            return val.replace(/\D/g, '');
                                        }
                                    },
                                    {
                                        name: 'pat_ssn', type: 'string',
                                        convert: function (val, rec) {
                                            if (!val) {
                                                return
                                            }
                                            return val.replace(/\D/g, '');
                                        }
                                    },
                                    {name: 'pat_middle_name', type: 'string'},
                                    {name: 'pat_notes', type: 'string'},
                                    {
                                        name: 'pat_dob', type: 'date', dateFormat: 'Y-m-d H:i:s',
                                        convert: function (val, rec) {
                                            return val;

                                        }
                                    }
                                ],
                                autoLoad: true,
                                autoDestroy: true,
                                groupField: 'db',
                                proxy: {
                                    type: 'rest',
                                    method: 'GET',
                                    url: ECO.app.globals.url + 'v1/patient/create_patient_search',
                                    extraParams: basicFormValues,
                                    reader: {
                                        type: 'json',
                                        rootProperty: 'data',
                                        totalProperty: 'totalCount'
                                    }
                                },
                                listeners: {
                                    load: function (store, records) {
                                        debugger;
                                        mask.destroy();
                                        if (records) {
                                            if (records.length > 0) {
                                                me.config.foundPatient = true;
                                                var _foundPatientGrid = win.down('#foundPatientGrid');
                                                _foundPatientGrid.setStore(patientStore);
                                                me.doCardNavigation(1, view);
                                            } else {
                                                me.doCardNavigation(2, view);
                                            }
                                        } else {
                                            me.doCardNavigation(2, view);
                                        }

                                    }
                                },
                                lazyFill: false
                            });

                        } else if (i == 1) {
                            showNext = true;
                        } else if (i == 2) {
                            me.config._selectAddress = false;

                            //If on Found Address card
                            showNext = false;
                            var addressForm = win.down('#addressForm');
                            var addressFormValues = addressForm.getForm().getValues();
                            if (!addressForm.isValid()) {
                                Ext.Msg.alert('Invalid', 'Invalid Fields');
                                return;
                            }
                            var address = addressFormValues._street + ' ' + addressFormValues._city + ' FL ' + addressFormValues._zip;

                            var searchAddressMask = new Ext.LoadMask({msg: "Please Wait...", target: win});
                            searchAddressMask.show();

                            ECO.helper.Func.callAjax('v1/google/geocode', {address: address}, 'POST').then(function (result) {
                                debugger;
                                if (result.success == true) {
                                    var multAddressStore = Ext.create('Ext.data.Store', {
                                        fields: [
                                            {name: 'formatted_address', type: 'string'}
                                        ],
                                        autoLoad: true,
                                        data: result.data.json.results,
                                        proxy: {
                                            type: 'memory',
                                            reader: {
                                                type: 'json'
                                            }
                                        },
                                        lazyFill: false
                                    });

                                    if (multAddressStore.getData().length > 0) {
                                        me.config._foundAddress = true;
                                        var _foundAddressGrid = win.down('#multiAddressGrid');
                                        _foundAddressGrid.setStore(multAddressStore);
                                        me.doCardNavigation(1, view);
                                    } else {
                                        me.doCardNavigation(2, view);
                                    }

                                } else {
                                    //unable to connecto to google;
                                    ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Unable to geocode address');
                                    me.doCardNavigation(2, view);

                                }
                            });
                            searchAddressMask.destroy();
                        } else if (i == 3) {
                            var city,
                                apt,
                                zip,
                                lng,
                                lat;
                            street = win.down('#cStreet').getValue(), city = win.down('#cCity').getValue(), apt = win.down('#cApt').getValue(), zip = win.down('#cZip').getValue();

                            win.down('#cAddress').setValue(street + ' ' + apt + ' ' + ' ' + city + ' ' + zip);
                            showNext = true;
                        } else if (i == 4) {
                            var bForm = win.down('#basicForm').getForm(),
                                bFormValues = (me.config.foundPatientData) ? me.config.foundPatientData[0] : bForm.getValues(),
                                patAddress = false;

                            if (!me.config._selectAddress) {
                                lat = null;
                                lng = null;
                                patAddress = {
                                    street: win.down('#cStreet').getValue(),
                                    city: win.down('#cCity').getValue(),
                                    apt: win.down('#cApt').getValue(),
                                    zip: win.down('#cZip').getValue(),
                                    lat: lat,
                                    lng: lng
                                };
                            } else {
                                patAddress = me.config._selectAddress;
                            }

                            bFormValues.pat_openerp_id = (me.config.foundPatientData) ? me.config.foundPatientData[0].pat_openerp_id : null;
                            if (!bFormValues.tele_number) {
                                bFormValues.tele_number = bForm.getValues()._phone;
                                bFormValues.tele_type = 0;
                            }

                            if (bForm.isValid()) {
                                var val = {patInfo: bFormValues, address: patAddress};
                                showNext = false;

                                ECO.helper.Func.callAjax('v1/patient', val, 'POST').then(function (results) {
                                    if (results.success) {
                                        ECO.helper.Func.showToast('Patient created successfully');

                                        me.createFoundPatientTab(me, 'apps', results, {
                                            xtype: 'in-patient-main-container',
                                            session: true,
                                            title: results.pat_first_name + ' ' + (results.pat_middle_name || '') + ' ' + results.pat_last_name,
                                            viewModel: {
                                                data: {
                                                    patientRecord: results
                                                }
                                            }
                                        });
                                        me.config.foundAddress = null;
                                        me.config.foundPatient = null;
                                        me.config.foundPatientData = null;
                                        win.destroy();
                                    } else {
                                        ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving patient');
                                    }
                                });
                            } else {
                                Ext.Msg.alert("Error!", "Your form is invalid!");
                            }
                        }
                        if (showNext) {
                            me.doCardNavigation(1, view);
                        }
                    }
                }, {
                    text: 'Close',
                    handler: function () {
                        win.close();
                    }
                }],
                items: [{
                    cls: 'forms-patient-dynamic-window',
                    id: 'card-0',
                    xtype: 'form',
                    itemId: 'basicForm',
                    reference: 'basicForm',
                    items: [{
                        margin: '0 0 20 0',
                        xtype: 'component',
                        html: [
                            '<h2>Lets start by searching for the patient!</h2>',
                            '<p>Please make sure that your information below is correct.</p>'
                        ]
                    }, {
                        xtype: 'textfield',
                        name: '_firstname',
                        fieldStyle: 'text-transform:uppercase',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'First Name',
                        allowBlank: false,
                        cFieldName: 'cFirstName',
                        listeners: {
                            change: me.onFormFieldChange
                        }
                    }, {
                        xtype: 'textfield',
                        name: '_mi',
                        maxLength: 1,
                        enforceMaxLength: true,
                        fieldStyle: 'text-transform:uppercase',
                        cFieldName: 'cMI',
                        fieldLabel: 'MI',
                        listeners: {
                            change: me.onFormFieldChange
                        }
                    }, {
                        xtype: 'textfield',
                        name: '_lastname',
                        fieldStyle: 'text-transform:uppercase',
                        cFieldName: 'cLastName',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'Last Name',
                        allowBlank: false,
                        listeners: {
                            change: me.onFormFieldChange
                        }
                    }, {
                        xtype: 'textfield',
                        name: '_ssn',
                        maxLength: 9,
                        enforceMaxLength: true,
                        cFieldName: 'cSSN',
                        maskRe: /[0-9.]/,
                        regex: /^\d{9}?$/,
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'SSN',
                        allowBlank: false,
                        listeners: {
                            change: me.onFormFieldChange
                        }
                    }, {
                        xtype: 'textfield',
                        name: '_phone',
                        maxLength: 10,
                        enforceMaxLength: true,
                        cFieldName: 'cPhone',
                        maskRe: /[0-9.]/,
                        regex: /^\d{10}?$/,
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'Telephone',
                        allowBlank: false,
                        listeners: {
                            change: me.onFormFieldChange
                        }
                    }, {
                        xtype: 'datefield',
                        name: '_dob',
                        format: 'Y-m-d',
                        submitFormat: 'Y-m-d',
                        cFieldName: 'cDOB',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'Dob',
                        allowBlank: false,
                        listeners: {
                            change: me.onFormFieldChange
                        },
                        maxValue: new Date()
                    }, {
                        xtype: 'combobox',
                        name: '_sex',
                        store: patSexStore,
                        cFieldName: 'cSex',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'Sex',
                        allowBlank: false,
                        publishes: 'id',
                        typeAhead: true,
                        queryMode: 'local',
                        valueField: 'sex',
                        displayField: 'name',
                        listeners: {
                            change: me.onFormFieldChange
                        }
                    }]
                }, {
                    cls: 'forms-patient-dynamic-window',
                    id: 'card-1',
                    scrollable: 'y',
                    items: [{
                        margin: '0 0 20 0',
                        xtype: 'component',
                        html: [
                            '<h2>Similar patients found!</h2>',
                            '<p>Please select if the patient you are trying to add already exists.</p>'
                        ]
                    }, {
                        xtype: 'grid',
                        viewConfig: {
                            forceFit: true
                        },
                        reference: 'foundPatientGrid',
                        itemId: 'foundPatientGrid',
                        scrollable: 'y',
                        headerBorders: true,
                        rowLines: true,
                        stripeRows: false,
                        collapsible: false,
                        columns: [{
                            xtype: 'gridcolumn',
                            dataIndex: 'db',
                            hidden: true,
                            flex: 1,
                            renderer: function (value) {
                                return (value == 1) ? 'ECO' : 'OpenERP';
                            }
                        }, {
                            xtype: 'gridcolumn',
                            sortable: false,
                            filterField: false,
                            dataIndex: 'pat_picture',
                            renderer: function (value) {
                                if (!value) {
                                    value = 'resources/images/user-profile/no-image.jpg';
                                }
                                return "<img src='" + value + "' alt='Patient Picture' height='40px' width='40px'>";
                            },
                            width: 75
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'pat_active_status',
                            text: 'Active',
                            flex: 1,
                            renderer: function (value) {
                                return (value) ? '' : '<font color="red">Inactive';
                            }
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'pat_ssn',
                            text: 'SSN',
                            flex: 1,
                            renderer: function (val) {
                                if (val.length == 9) {
                                    return val.replace(/(\d{3})(\d{2})(\d{4})/, '$1-$2-$3');
                                }
                                return val;

                            }
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: ('pat_first_name' || 'first_name'),
                            text: 'First',
                            flex: 1
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: ('pat_last_name' || 'last_name'),
                            text: 'Last',
                            flex: 1
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'tele_number',
                            text: 'Phone',
                            flex: 1
                        }, {
                            xtype: 'datecolumn',
                            dataIndex: 'pat_dob',
                            text: 'DoB',
                            width: 100
                        }],
                        features: [{
                            ftype: 'grouping',
                            startCollapsed: true,
                            groupHeaderTpl: '<b>{name} </b>({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
                        }],
                        listeners: {
                            itemdblclick: function (dataview, index, item, e) {
                                var rec = index.getData();
                                if (rec) {
                                    if (rec.db == 1) {
                                        me.createFoundPatientTab(me, 'apps', rec, {
                                            xtype: 'in-patient-main-container',
                                            session: true,
                                            title: rec.pat_first_name + ' ' + rec.pat_middle_name + ' ' + rec.pat_last_name,
                                            viewModel: {
                                                data: {
                                                    patientRecord: rec
                                                }
                                            }
                                        });
                                        win.destroy();
                                    } else {
                                        var btnNext = win.query('#card-next')[0],
                                            addrForm = win.down('#addressForm').getForm(),
                                            patForm = win.down('#basicForm').getForm();

                                        //add openerp id to the record before saving    
                                        rec.pat_openerp_id = rec.id;

                                        addrForm.setValues({
                                            '_street': rec.addr_street,
                                            '_city': rec.addr_city,
                                            '_apt': rec.addr_apartment,
                                            '_zip': rec.addr_zipcode
                                        });
                                        me.config.foundPatientData = [];
                                        me.config.foundPatientData.push(rec);
                                        btnNext.click();
                                    }
                                }
                            }
                        }
                    }]
                }, {
                    cls: 'forms-patient-dynamic-window',
                    id: 'card-2',
                    xtype: 'form',
                    reference: 'addressForm',
                    itemId: 'addressForm',
                    items: [{
                        margin: '0 0 20 0',
                        xtype: 'component',
                        html: [
                            '<h2>Continue...</h2>',
                            '<p>Please make sure that the information below is correct.</p>'
                        ]
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Street',
                        reference: 'cStreet',
                        fieldStyle: 'text-transform:uppercase',
                        itemId: 'cStreet',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        name: '_street',
                        allowBlank: false,
                    }, {
                        xtype: 'textfield',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'City',
                        fieldStyle: 'text-transform:uppercase',
                        itemId: 'cCity',
                        name: '_city',
                        allowBlank: false
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Apt',
                        fieldStyle: 'text-transform:uppercase',
                        itemId: 'cApt',
                        name: '_apt'
                    }, {
                        xtype: 'textfield',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        fieldLabel: 'Zip',
                        itemId: 'cZip',
                        name: '_zip',
                        allowBlank: false,
                        maxLength: 5,
                        enforceMaxLength: true,
                        maskRe: /[0-9.]/,
                        regex: /^\d{5}?$/
                    }]
                }, {
                    id: 'card-3',
                    fieldDefaults: {
                        labelAlign: 'left',
                        labelWidth: 90,
                        anchor: '100%',
                        msgTarget: 'side'
                    },
                    items: [{
                        margin: '0 0 20 0',
                        xtype: 'component',
                        html: [
                            '<h2>Verify address...</h2>'
                        ]
                    }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                            columnWidth: 0.65,
                            width: 380,
                            layout: 'fit',
                            xtype: 'gridpanel',
                            reference: 'multiAddressGrid',
                            itemId: 'multiAddressGrid',
                            height: 380,
                            columns: [{
                                text: 'Address',
                                flex: 1,
                                sortable: true,
                                dataIndex: 'formatted_address'
                            }],
                            listeners: {
                                selectionchange: function (model, records) {
                                    var rec = (records) ? records[0] : null;
                                    if (rec) {
                                        var gmap = win.down('#multipleAddressMap').gmap,
                                            gridpanel = win.down('#multiAddressGrid');

                                        var results = gridpanel.getStore().getData().items,
                                            selectedrec = gridpanel.getSelectionModel().getSelection()[0],
                                            rowindex = gridpanel.store.indexOf(selectedrec);

                                        gmap.setCenter(results[rowindex].get('geometry').location);
                                        new google.maps.Marker({
                                            map: gmap,
                                            position: results[rowindex].get('geometry').location
                                        });
                                    }
                                },
                                itemdblclick: function (dataview, index, item, e) {
                                    try {
                                        var street = false,
                                            route = false,
                                            city = false,
                                            apt = win.down('#cApt').getValue(),
                                            zip = false,
                                            lat = false,
                                            lng = false;

                                        for (var i = 0; i < index.get('address_components').length; i++) {
                                            if (index.get('address_components')[i].types[0] === "street_number") {
                                                street = index.get('address_components')[i].long_name;
                                            }
                                            if (index.get('address_components')[i].types[0] === "route") {
                                                route = index.get('address_components')[i].long_name;
                                            }
                                            if (index.get('address_components')[i].types[0] === "locality") {
                                                city = index.get('address_components')[i].long_name;
                                            }
                                            if (index.get('address_components')[i].types[0] === "postal_code") {
                                                zip = index.get('address_components')[i].long_name;
                                            }
                                        }

                                        lat = index.get('geometry').location.lat;
                                        lng = index.get('geometry').location.lng;

                                        if (!street || !route || !city || !zip || !lat || !lng) {
                                            throw 'error';
                                        } else {
                                            street = street + ' ' + route;
                                            me.config._selectAddress = {
                                                street: street,
                                                apt: win.down('#cApt').getValue(),
                                                city: city,
                                                zip: zip,
                                                lat: lat,
                                                lng: lng
                                            };
                                            win.down('#cAddress').setValue(street + ' ' + ' ' + city + ' ' + zip);
                                            me.doCardNavigation(1, win);
                                        }
                                    }
                                    catch (err) {
                                        me.config._foundAddress = false;
                                        Ext.MessageBox.show({
                                            title: 'Verify',
                                            msg: 'Unable to geocode address, select the skip button',
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.WARNING
                                        });
                                        return;
                                    }
                                }
                            }
                        }, {
                            columnWidth: 0.35,
                            margin: '0 0 0 10',
                            xtype: 'gmappanel',
                            reference: 'multipleAddressMap',
                            itemId: 'multipleAddressMap',
                            center: new google.maps.LatLng(-34.397, 150.644),
                            mapOptions: {
                                mapTypeId: 'roadmap',
                                zoom: 12
                            },
                            markers: [],
                            width: 320,
                            height: 260
                        }]
                    }]
                }, {
                    cls: 'forms-patient-dynamic-window',
                    id: 'card-4',
                    scrollable: true,
                    layout: 'container',
                    reference: 'confirmInfo',
                    items: [{
                        xtype: 'component',
                        html: [
                            '<h2>Confirm...</h2>',
                            '<p>Please make sure that your information below is correct.</p>'
                        ]
                    }, {
                        xtype: 'form',
                        layout: 'hbox',
                        fieldDefaults: {
                            msgTarget: 'side',
                            labelAlign: 'left',
                            labelWidth: 100,
                            labelStyle: 'font-weight:bold'
                        },
                        items: [{
                            items: [{
                                xtype: 'displayfield',
                                fieldLabel: 'First Name',
                                name: 'cFirstName'
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'MI',
                                name: 'cMI'
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'Last Name',
                                name: 'cLastName'
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'SSN',
                                name: 'cSSN'
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'Sex',
                                name: 'cSex'
                            }]
                        }, {
                            align: 'r',
                            margin: '0 0 0 100',
                            items: [{
                                xtype: 'displayfield',
                                fieldLabel: 'Telephone',
                                name: 'cPhone'
                            }, {
                                xtype: 'datedisplayfield',
                                name: 'cDOB',
                                fieldLabel: 'DOB',
                                datePattern: 'Y-m-d'
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'Address',
                                name: 'cAddress',
                                itemId: 'cAddress'
                            }]
                        }]
                    }]
                }]
            }],
            listeners: {
                close: function () {
                    this.destroy();
                }
            }
        });

        win.show();
    },
    //endregion

    //region onFormFieldChange
    onFormFieldChange:function(field, newValue){
        (field.cFieldName == 'cDOB' || field.cFieldName == 'cPhone' || field.cFieldName == 'cSSN') ? '' : field.setValue(newValue.toUpperCase());
        var copyField = this.up().up().down('[name=' + field.cFieldName + ']');
        if(copyField.name == "cSex"){
            copyField.setValue(ECO.helper.GridRenderer.sexFormat(field.getValue()));
        }else{
            copyField.setValue(field.getValue());
        }

    },
    //endregion

    //region doCardNavigation
    doCardNavigation: function (incr, view) {

        var l = view.items.items[0].getLayout();
        if (l.activeItem == undefined){return}
        var i = l.activeItem.id.split('card-')[1];
        var next = parseInt(i, 10) + incr;
        l.setActiveItem(next);

        view.down('#card-prev').setDisabled(next===0);
        (next == 1 || next == 3) ? view.down('#card-next').setText('Skip &raquo;') : view.down('#card-next').setText('Next &raquo;');
        if(next == 4){view.down('#card-next').setText('Finish')}
        // view.down('#card-next').setDisabled(next===2);
    },
    //endregion

    //region createFoundPatientTab
    createFoundPatientTab: function (me, prefix, rec, cfg) {
        var type = me.type;
        var tabs = me.getView(),
            id = (rec != null) ? prefix + '_' + rec.pat_id : null,
            tab = tabs.items.getByKey(id);

        if (!tab) {
            cfg.itemId = id;
            cfg.closable = true;
            cfg.type = type;
            tab = tabs.add(cfg);
        }

        tabs.setActiveTab(tab);
    }
    //endregion

});