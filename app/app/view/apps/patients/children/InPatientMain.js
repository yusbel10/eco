Ext.define('ECO.view.apps.patients.children.InPatientMain', {
    extend: 'Ext.container.Container',
    xtype: 'in-patient-main-container',
    requires:[
        'Ext.ux.layout.ResponsiveColumn',
        'ECO.view.apps.patients.children.InPatientMainController',
        'ECO.view.apps.patients.children.InPatientTabs'
    ],
    controller:'in-patient',

    cls: 'chartProfile-container dash-background',

    layout: 'responsivecolumn',

    //region initComponent
    initComponent: function () {
        Ext.ariaWarn = Ext.emptyFn;

        var patRec = this.getViewModel().get('patientRecord'),
            patImg = (patRec.pat_picture == null || patRec.pat_picture == '') ? 'resources/images/user-profile/no-image.jpg' : patRec.pat_picture,
            activeStatus = (patRec.pat_active_status) || null;

        this.items =[{
            xtype:'panel',
            height: 300,
            bodyPadding: 10,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            cls: 'shadow-panel',
            title:'Profile',
            ui: 'light',
            responsiveCls: 'big-50 small-100',
            iconCls: 'x-fa fa-user',
            tools: [{
                xtype: 'tool',
                toggleValue: false,
                tooltip: 'Edit profile',
                cls: 'x-fa fa-pencil dashboard-tools',
                handler: 'onEditPatientProfile',
                width: 20,
                height: 20
            },{
                xtype:'tool',
                tooltip: 'Upload Image',
                cls: 'x-fa fa-upload dashboard-tools',
                handler:'onPatientProfileImageUploadClick',
                width: 20,
                height: 20
            }],
            items: [{
                xtype:'container',
                layout: 'hbox',
                items:[{
                    xtype: 'container',
                    flex:1,
                    layout: {
                        type: 'vbox',
                        align: 'left'
                    },
                    cls:'social-panel ',
                    items: [{
                        reference:'profilePictureImg',
                        html:"<img src='"+patImg+"' style='border-radius: 50%; border: 2px solid #ddd;' alt='Patient Picture' height='80px' width='80px'>"
                    },{
                        xtype:'component',
                        html:  (activeStatus) ? '' : '<span style="color: red" class="x-fa fa-exclamation-triangle"></span> <font color="red"><b>INACTIVE</b></font>'
                        //html:'test'
                    }]
                },{
                    xtype:'container',
                    padding:15,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    cls: 'timeline-items-wrap chart-profile-desc',
                    items:[{
                        xtype: 'component',
                        html: '<b>Chart:</b> '+ patRec.pat_chart_number,
                        padding: '0 0 12 0'
                    },{
                        xtype: 'component',
                        html: '<b>Name:</b> ' + patRec.pat_first_name + ' ' + (patRec.pat_middle_name || '') + ' ' + patRec.pat_last_name,
                        padding: '0 0 12 0'
                    },{
                        xtype: 'component',
                        html: '<b>DOB:</b> '  + Ext.util.Format.date(patRec.pat_dob, 'm/d/Y'),//Ext.util.Format.date(patRec.pat_dob, 'Y-m-d'),
                        padding: '0 0 12 0'
                    },{
                        xtype: 'component',
                        html: '<b>Sex:</b> '  + ECO.helper.GridRenderer.sexFormat(patRec.pat_sex),
                        padding: '0 0 12 0'
                    },{
                        xtype: 'component',
                        html: '<b>SSN:</b> '  + patRec.pat_ssn,
                        padding: '0 0 12 0'
                    },{
                        xtype: 'component',
                        html: '<b>HS:</b> '   + (patRec.insu_number || ''),
                        padding: '0 0 12 0'
                    }]

                }]
            }]

        },{
            xtype:'panel',
            height: 300,
            bodyPadding: 10,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            cls:'shadow-panel',
            title:'Note',
            ui: 'light',
            responsiveCls: 'big-50 small-100',
            iconCls: 'x-fa fa-file-text-o',
            tools: [{
                xtype: 'tool',
                toggleValue: false,
                tooltip: 'Save current note',
                cls: 'x-fa fa-save dashboard-tools',
                listeners: {
                    click: 'onActiveTabNoteSaveClick'
                },
                width: 20,
                height: 20
            }],
            items: [{
                xtype: 'htmleditor',
                reference: 'patientNote',
                emptyText: 'No note',
                enableColors: true,
                enableAlignments: true,
                value: patRec.pat_notes,
                listeners: {
                    sync : function(he, str) {
                        //localStorage.setItem("myNote", str);
                    }
                }
            }],
            bbar: {
                defaults : {
                    margin:'0 10 5 0'
                },
                items:[{
                    html: '<B>Last Modified By:</B> Yusbel Rojas'
                }/**,{
                    xtype: 'button',
                    iconCls: 'x-fa fa-video-camera'
                },{
                    xtype: 'button',
                    iconCls: 'x-fa fa-camera'
                },{
                    xtype: 'button',
                    iconCls: 'x-fa fa-file'
                }, '->',
                 {
                     xtype: 'button',
                     text: 'Share',
                     ui: 'soft-blue'
                 }**/]
            }
        },{
            xtype:'in-patient-tabs',
            layout: 'responsivecolumn',
            responsiveCls: 'big-100 small-100',
            cls:'shadow-panel',
            viewModel: {
                data: {
                    patientRecord:patRec
                }
            }
        }];
        this.callParent(arguments)
    }
    //endregion

});