Ext.define('ECO.cache.Global',{
    singleton : true,
    config : {
        voicePanel: null,
        twilioDevice: null,
        audioCtx: null,
        audioSrc: null,
        analyser: null,
        frequency: null,
        svgHeight: null,
        svg: null,
        raf: null,
        height:null,
        volume: null

    },

    //region renderChart
    renderChart: function(){
        var me = this;

        function loop() {
            var svg = me.getSvg(),
                analyser = me.getAnalyser(),
                frequencyData = me.getFrequency(),
                svgHeight = me.getSvgHeight();


            me.setRaf(requestAnimationFrame(loop));


            analyser.getByteFrequencyData(frequencyData);
            svg.selectAll('rect')
                .data(frequencyData)
                .attr('y', function(d) {
                    return svgHeight - d;
                })
                .attr('height', function(d) {
                    return d;
                })
                .attr('fill', function(d) {
                    return 'rgb(255, 0, ' + d + ')';
                });
        }

        loop();
    },
    //endregion

    //region stopRender
    stopRender: function (voicePanel, mainToolbar) {
        var me = this;

        cancelAnimationFrame(me.getRaf());

        // var volume = me.getVolume();
        // volume.gain.value= 0;


        me.getAudioCtx().close().then(function(){
            d3.select("svg").remove();
            me.getSvg().selectAll("*").remove();
            me.setRaf(null);
            me.setSvg(null);
            me.setAudioSrc(null);
            me.setFrequency(null);
            me.setAudioCtx(null);
            me.setSvgHeight(null);
            me.setAnalyser(null);
            voicePanel.collapse();
            voicePanel.updateLayout();

            ECO.helper.Func.delayMainToolbar(mainToolbar).delay(1000);



            console.log('Audio context off');
           // Ext.destroy(ECO.VoiceCache);
            //  ECO.VoiceCache.destroy();
           // ECO.VoiceCache = 'undefined';


        });

    },
    //endregion


    //region createVisualizers
    createVisualizers: function (mainToolbar) {
        var me = this,
            voicePanel = this.getVoicePanel();

        //  var p1 = me.createCollisionVis();
        // var p2 = me.createTreeVis();


        me.createAudioVis()
            .then(function () {
                var task = new Ext.util.DelayedTask(function() {
                    voicePanel.updateLayout();
                });
                task.delay(1200);
                if(mainToolbar){  ECO.helper.Func.delayMainToolbar(mainToolbar).delay(1000); }

            },function (error) {
                console.error(error);
            });
    },
    //endregion

    //
    // createCollisionVis: function () {
    //     //COLLISION
    //     var deferred = new Ext.Deferred();
    //
    //     var me = this;
    //
    //     var width = 300,
    //         height = 200;
    //
    //     var nodes = d3.range(200).map(function () {
    //             return {radius: Math.random() * 12 + 4};
    //         }),
    //         root = nodes[0],
    //         color = d3.scale.category10();
    //
    //
    //     root.radius = 0;
    //     root.fixed = true;
    //
    //     var force = d3.layout.force()
    //         .gravity(0.05)
    //         .charge(function (d, i) {
    //             return i ? 0 : -2000;
    //         })
    //         .nodes(nodes)
    //         .size([width, height]);
    //
    //
    //     force.start();
    //
    //     var area = Ext.getDom('audio_1');
    //
    //     var svg = d3.select(area).append("svg")
    //         .attr("width", width)
    //         .attr("height", height);
    //
    //     svg.selectAll("circle")
    //         .data(nodes.slice(1))
    //         .enter().append("circle")
    //         .attr("r", function (d) {
    //             return d.radius;
    //         })
    //         .style("fill", function (d, i) {
    //             return color(i % 3);
    //         });
    //
    //     force.on("tick", function (e) {
    //         var q = d3.geom.quadtree(nodes),
    //             i = 0,
    //             n = nodes.length;
    //
    //         while (++i < n) q.visit(collide(nodes[i]));
    //
    //         svg.selectAll("circle")
    //             .attr("cx", function (d) {
    //                 return d.x;
    //             })
    //             .attr("cy", function (d) {
    //                 return d.y;
    //             });
    //     });
    //
    //     svg.on("mousemove", function () {
    //         var p1 = d3.mouse(this);
    //         root.px = p1[0];
    //         root.py = p1[1];
    //         force.resume();
    //     });
    //
    //     function collide(node) {
    //         var r = node.radius + 16,
    //             nx1 = node.x - r,
    //             nx2 = node.x + r,
    //             ny1 = node.y - r,
    //             ny2 = node.y + r;
    //         return function (quad, x1, y1, x2, y2) {
    //             if (quad.point && (quad.point !== node)) {
    //                 var x = node.x - quad.point.x,
    //                     y = node.y - quad.point.y,
    //                     l = Math.sqrt(x * x + y * y),
    //                     r = node.radius + quad.point.radius;
    //                 if (l < r) {
    //                     l = (l - r) / l * .5;
    //                     node.x -= x *= l;
    //                     node.y -= y *= l;
    //                     quad.point.x += x;
    //                     quad.point.y += y;
    //                 }
    //             }
    //             return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
    //         };
    //     }
    //
    //     deferred.resolve(true);
    //     return deferred.promise;
    //
    // },
    //
    // createTreeVis: function () {
    //
    //     //TREE
    //     var deferred = new Ext.Deferred();
    //
    //     var c = Ext.getDom('audio_2');
    //     var w = c.width = 300
    //         , h = c.height = 200
    //         , ctx = c.getContext('2d')
    //
    //         , opts = {
    //
    //         impositions: 5,
    //         ampGeomConst: .4,
    //         baseAmp: 70,
    //         addedAmp: 30,
    //         // w = angular velocity. It's directly proportional to frequency
    //         wGeomConst: 1.5,
    //         baseW: .3,
    //         addedW: .1,
    //
    //
    //         wTickMultiplier: .1,
    //         scanSpeed: 3,
    //
    //         sparkChance: .1,
    //         sparkBaseHeight: 50,
    //         sparkAddedHeight: 30,
    //         sparkBaseTime: 30,
    //         sparkAddedTime: 10,
    //
    //         shardBaseCount: 8,
    //         shardAddedCount: 5,
    //         shardBaseVel: .3,
    //         shardAddedVel: .3,
    //
    //         minTicksSinceLastSpark: 20,
    //
    //         acc: .03,
    //
    //         repaintColor: 'rgba(0,0,0,.02)',
    //         waveColor: 'hsl(hue,80%,60%)',
    //         waveWidth: 4,
    //         sparkColor: 'hsl(hue,80%,light%)',
    //         sparkWidth: 2,
    //         shardColor: 'hsl(hue,40%,50%)',
    //         shardWidth: 1
    //     }
    //
    //         , waves = []
    //         , sparks = []
    //         , shards = []
    //
    //         , tick = 0
    //         , px = 0
    //         , py = h / 2
    //         , ticksSinceLastSpark = 0
    //
    //         , tau = Math.PI * 2;
    //
    //
    //     var baseAmp = opts.baseAmp
    //         , addedAmp = opts.addedAmp
    //         , baseW = opts.baseW
    //         , addedW = opts.addedW;
    //
    //     for (var i = 0; i < opts.impositions; ++i) {
    //
    //         baseAmp *= opts.ampGeomConst;
    //         addedAmp *= opts.ampGeomConst;
    //         baseW *= opts.wGeomConst;
    //         addedW *= opts.wGeomConst;
    //
    //         waves.push({
    //             amp: baseAmp + addedAmp * Math.random(),
    //             w: baseW + addedW * Math.random()
    //         });
    //     }
    //
    //     function tickToHue(tick) {
    //
    //         return tick * opts.scanSpeed / w * 360
    //     }
    //
    //     function Spark(x, y, dy, iTick) {
    //
    //         this.iTick = iTick;
    //         this.color = opts.sparkColor.replace('hue', tickToHue(iTick));
    //
    //         this.ix = this.x = x;
    //         this.iy = this.y = y;
    //
    //         this.dir = Math.sign(dy);
    //
    //         this.height = opts.sparkBaseHeight + opts.sparkAddedHeight * Math.random();
    //
    //         this.tick = 0;
    //         this.time = opts.sparkBaseTime + opts.sparkAddedTime * Math.random() | 0;
    //     }
    //
    //     Spark.prototype.step = function () {
    //
    //         ++this.tick;
    //
    //         var linearProp = this.tick / this.time
    //             , armonic = -Math.cos(linearProp * tau / 4) + 1;
    //
    //         var y = this.iy + this.height * armonic * this.dir;
    //
    //         //ctx.strokeStyle = this.color.replace( 'light', 60 + armonic*armonic * 40 );
    //
    //         ctx.beginPath();
    //         ctx.moveTo(this.x, this.y + this.dir);
    //         this.y = y;
    //         ctx.lineTo(this.x, this.y);
    //         ctx.stroke();
    //
    //         if (linearProp >= 1) {
    //             this.dead = true;
    //
    //             var shardCount = opts.shardBaseCount + opts.shardAddedCount * Math.random() | 0
    //                 , deltaRad = tau / shardCount
    //                 , baseRad = Math.random() * deltaRad
    //                 , shardVel = opts.shardBaseVel + opts.shardAddedVel * Math.random()
    //                 , colorOffset = tickToHue(this.iTick);
    //
    //             for (var i = 0; i < shardCount; ++i)
    //                 shards.push(new Shard(this.x, this.y, baseRad + deltaRad * i, this.dir, shardVel, colorOffset));
    //         }
    //
    //
    //
    //     };
    //
    //     function Shard(x, y, rad, dir, vel, colorOffset) {
    //
    //         this.x = x;
    //         this.y = y;
    //
    //         this.rad = rad;
    //         this.dir = dir;
    //
    //         this.vx = Math.cos(this.rad) * vel;
    //         this.vy = Math.sin(this.rad) * vel;
    //
    //         this.color = opts.shardColor.replace('hue', rad / tau * 360 + colorOffset);
    //     }
    //
    //     Shard.prototype.step = function () {
    //
    //         //ctx.strokeStyle = this.color;
    //
    //         ctx.beginPath();
    //         ctx.moveTo(this.x, this.y);
    //
    //         this.x += this.vx;
    //         this.y += this.vy += opts.acc * this.dir;
    //
    //         ctx.lineTo(this.x, this.y);
    //         ctx.stroke();
    //
    //         if (this.x < 0 || this.x > w || this.y < 0 || this.y > h)
    //             this.dead = true;
    //     };
    //
    //     function anim() {
    //
    //         window.requestAnimationFrame(anim);
    //
    //         ++tick;
    //         ++ticksSinceLastSpark;
    //
    //         ctx.fillStyle = opts.repaintColor;
    //         ctx.fillRect(0, 0, w, h);
    //
    //         ctx.strokeStyle = ctx.fillStyle = opts.waveColor.replace('hue', tickToHue(tick));
    //         ctx.lineWidth = opts.waveWidth;
    //
    //         ctx.beginPath();
    //         ctx.moveTo(px, py);
    //         // I know I can get rid of these variables, but this is clearer
    //
    //         var x = (( tick * opts.scanSpeed ) % w)
    //             , y = h / 2
    //
    //             , W = tick * opts.wTickMultiplier;
    //
    //         for (var i = 0; i < waves.length; ++i)
    //             y += Math.sin(waves[i].w * W) * waves[i].amp;
    //
    //         ctx.lineTo(px < x ? x : x + w, y);
    //         ctx.lineCap = 'round';
    //         ctx.stroke();
    //
    //         ctx.lineCap = 'square';
    //
    //         var dy = y - py;
    //
    //         //ctx.fillRect( x + 2 * Math.random(), y + dy * ( 2 + Math.random() * 3 ), 1, 1 );
    //
    //         if (ticksSinceLastSpark > opts.minTicksSinceLastSpark && Math.random() < opts.sparkChance) {
    //
    //             ticksSinceLastSpark = 0;
    //             sparks.push(new Spark(x, y, dy, tick));
    //         }
    //
    //         ctx.lineWidth = opts.sparkWidth;
    //         for (var i = 0; i < sparks.length; ++i) {
    //
    //             sparks[i].step();
    //
    //             if (sparks[i].dead) {
    //                 sparks.splice(i, 1);
    //                 --i;
    //             }
    //         }
    //
    //         ctx.lineWidth = opts.shardWidth;
    //         for (var i = 0; i < shards.length; ++i) {
    //
    //             shards[i].step();
    //
    //             if (shards[i].dead) {
    //                 shards.splice(i, 1);
    //                 --i;
    //             }
    //         }
    //
    //         px = x;
    //         py = y;
    //
    //         deferred.resolve(true);
    //         return deferred.promise;
    //
    //     }
    //
    //     ctx.fillStyle = '#32404e';
    //     ctx.fillRect(0, 0, w, h);
    //     anim();
    //
    // },

    //region createAudioVis
    createAudioVis: function () {
        var me = this,
            deferred = new Ext.Deferred(),
            audioCtx = new (window.AudioContext || window.webkitAudioContext)();

        if (!navigator.getUserMedia)
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia || navigator.msGetUserMedia;


        if (navigator.getUserMedia){

            navigator.getUserMedia({audio:true},
                function(stream) {
                    var audioSrc = audioCtx.createMediaStreamSource(stream);
                    var analyser = audioCtx.createAnalyser(),
                        frequencyData = new Uint8Array(200),
                        svgHeight = '100',
                        svgWidth = '300',
                        barPadding = '1',
                        i = 0;

                    console.log(analyser.smoothingTimeConstant)

                    analyser.smoothingTimeConstant = 0.8; //default
                    analyser.fftSize = 2048;

                    audioSrc.connect(analyser);

                    // Uncomment if you want the audio to feedback directly
                    //audioSrc.connect(audioCtx.destination);


                    //
                    // var volume = audioCtx.createGain();
                    // volume.gain.value = 0.0;
                    //
                    // audioSrc.connect(volume);
                    // analyser.connect(volume);
                    // volume.connect(audioCtx.destination);






                    function createSvg(parent, height, width) {
                        return d3.select(parent).append('svg').attr('height', height).attr('width', width);
                    }

                    var div = document.getElementById('audio_3');

                    var svg = createSvg(div, svgHeight, svgWidth);


                    svg.selectAll('rect')
                        .data(frequencyData)
                        .enter()
                        .append('rect')
                        .attr('x', function (d, i) {
                            return i * (svgWidth / frequencyData.length);
                        })
                        .attr('width', svgWidth / frequencyData.length - barPadding)
                        .attr('fill', '#13242F');



                    console.log(frequencyData.length);
                    me.setAudioCtx(audioCtx);
                    me.setAudioSrc(audioSrc);
                    me.setAnalyser(analyser);
                    me.setFrequency(frequencyData);
                    me.setSvgHeight(svgHeight);
                    me.setSvg(svg);
                    me.setHeight(svgHeight);
                    // ECO.VoiceFrequencyCache = Ext.create('ECO.cache.VoiceFrequency',{
                    //     audioCtx: audioCtx,
                    //     audioSrc: audioSrc,
                    //     analyser: analyser,
                    //     frequency: frequencyData,
                    //     svgHeight: svgHeight,
                    //     svg: svg,
                    //     height: svgHeight
                    // });
                    deferred.resolve(true);

                    // Continuously loop and update chart with frequency data.
                    function renderChart() {
                        requestAnimationFrame(renderChart);

                        // Copy frequency data to frequencyData array.
                        // analyser.getByteFrequencyData(frequencyData);
                        // ECO.VoiceFrequencyCache.setAnalyser(analyser.getByteFrequencyData(frequencyData));

                        // console.log(frequencyData.length)

                        // Update d3 chart with new data.

                        //  svg.insert("circle", "rect")
                        //      .attr("cx", frequencyData[0])
                        //      .attr("cy", frequencyData[1])
                        //      .attr("r", 1e-6)
                        //      .style("stroke", d3.hsl((i = (i + 1) % 360), 1, .5))
                        //      .style("stroke-opacity", 1)
                        //      .transition()
                        //      .duration(2000)
                        //      .ease(Math.sqrt)
                        //      .attr("r", 100)
                        //      .style("stroke-opacity", 1e-6)
                        //      .remove();
                        //
                        // // d3.event.preventDefault();

                        me.getSvg().selectAll('rect')
                            .data(frequencyData)
                            .attr('y', function(d) {
                                return svgHeight - d;
                            })
                            .attr('height', function(d) {
                                return d;
                            })
                            .attr('fill', function(d) {
                                return 'rgb(255, 0, ' + d + ')';
                            });



                        // svg.selectAll('rect')
                        //     .data(frequencyData)
                        //     .attr('y', function(d) {
                        //         return svgHeight - d;
                        //     })
                        //     .attr('height', function(d) {
                        //         return d;
                        //     })
                        //     .attr('fill', function(d) {
                        //         return 'rgb(255, 0, ' + d + ')';
                        //     });


                    }

                    // Run the loop
                    // renderChart();

                    me.renderChart();



                },
                function(e) {
                    deferred.reject(e);
                    Ext.Msg.alert('Error', 'Error capturing audio.');

                }
            );

        }else {
            deferred.reject('getUserMedia not supported in this browser.');
            Ext.Msg.alert('Error', 'getUserMedia not supported in this browser.');
        }

        return deferred.promise;
    },
    //endregion

    //region updateVoicePanelProfile
    updateVoicePanelProfile: function (img, profName, profNumber) {
        var voicePanel = this.getVoicePanel(),
            voiceRefs = voicePanel.getReferences();


        voiceRefs.voiceProfileImage.setSrc((img) || 'resources/images/user-profile/no-image.jpg');
        voiceRefs.voiceProfileName.setValue(profName);
        voiceRefs.voiceProfileTelephone.setValue((profNumber) || '');


        voicePanel.updateLayout();
    },
    //endregion


    constructor : function(config){
        this.initConfig(config);
        this.callParent(arguments);
    }
});