var MysqlCfg = require('./db-config.json'),
    PostgresqlCfg = require('./postgresdb-config.json');

function MySQL(config) {
    this.mysqlConnection = null;
    this.connectMySQL = function () {
        this.mysqlConnection = require('knex')({
            client: 'mysql',
            connection: config,
            pool: { min: 1, max: 10 },
            acquireConnectionTimeout: 10000
        });
    }
}
function PostgreSQL(config) {
    this.postgresConnection = null;
    this.connectPostgresql = function () {
        this.postgresConnection = require('knex')({
            client: 'postgresql',
            connection: config,
            pool: { min: 1, max: 1 },
            acquireConnectionTimeout: 10000
        });
    }
}
global.App.mySQlDatabase = new MySQL(MysqlCfg);
global.App.postgreSQlDatabase = new PostgreSQL(PostgresqlCfg);
