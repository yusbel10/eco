var jwt = require('jwt-simple');
var verify = require('../routes/Auth/Auth').verify;

module.exports = function(req, res, next) {
    // res.status(401);
    // res.json({
    //     status: 401,
    //     message: "Token Expired"
    // });
    // return;
    
    var MysqlKnex = global.App.mySQlDatabase.mysqlConnection;

    var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
    //var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];
    if (token) {
        try {
            var decoded = jwt.decode(token, require('../config/secret.js')());
            if (decoded.exp <= Date.now()) {
                res.status(401);
                res.json({
                    status: 401,
                    message: "Token Expired"
                });
                return;
            }
// Authorize the user to see if s/he can access our resources

            var username = decoded.user.user_username;

            MysqlKnex('eco_user')
                .where('user_username', username)
                .then(function (response) {
                    var dbUser = verify(username, response);
                    if (!dbUser) { // If authentication fails, we send a 401 back
                        res.status(400);
                        res.json({
                            "status": 400,
                            "message": "Invalid credentials"
                        });
                    }else{
                        next();
                    }
                }).catch(function(error) {
                    res.json({"code": 100, "status": "Error in connection database"});
                });


            // pool.getConnection(function (err, connection) {
            //     if (err) {
            //         res.json({"code": 100, "status": "Error in connection database"});
            //         return;
            //     }
            //
            //     console.log('connected as id ' + connection.threadId);
            //
            //     var username = decoded.user.username;
            //     connection.query('SELECT * FROM eco_user WHERE user_username = ?', [username], function (err, rows) {
            //         if (!err) {
            //
            //             var dbUser = verify(username, rows);
            //             if (!dbUser) { // If authentication fails, we send a 401 back
            //                 res.status(401);
            //                 res.json({
            //                     "status": 401,
            //                     "message": "Invalid credentials"
            //                 });
            //             }else{
            //                 next();
            //             }
            //
            //         }
            //     });
            //
            //     connection.on('error', function (err) {
            //         res.json({"code": 100, "status": "Error in connection database"});
            //     });
            //
            //     connection.release();
            // });



        } catch (err) {
            res.status(500);
            res.json({
                "status": 500,
                "message": "Oops something went wrong",
                "error": err
            });
        }
    } else {
        res.status(401);
        res.json({
            "status": 401,
            "message": "Invalid Token or Key"
        });
        return;
    }
};