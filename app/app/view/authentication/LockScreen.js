Ext.define('ECO.view.authentication.LockScreen', {
    extend: 'ECO.view.authentication.LockingWindow',
    xtype: 'lockscreen',

    requires: [
        'ECO.view.authentication.Dialog',
        'Ext.Img',
        'Ext.container.Container',
        'Ext.form.field.Text',
        'Ext.button.Button'
    ],

    title: 'Session Expired',

    defaultFocus : 'authdialog',  // Focus the Auth Form to force field focus as well

    initComponent: function () {
        var me = this,
            token = Ext.decode(localStorage.getItem('token')),
            img = (token.user[0].user_picture) ? token.user[0].user_picture : 'resources/images/user-profile/no-image.jpg',
            name = token.user[0].user_firstname + ' ' + (token.user[0].user_mi  || '') + ' ' + token.user[0].user_lastname;


        this.items= [{
            xtype: 'authdialog',
            reference: 'authDialog',
            defaultButton : 'loginButton',
            autoComplete: false,
            width: 455,
            cls: 'auth-dialog-login',
            defaultFocus : 'textfield[inputType=password]',
            layout: {
                type  : 'vbox',
                align : 'stretch'
            },
            items: [{
                xtype: 'container',
                cls: 'auth-profile-wrap',
                height : 120,
                layout: {
                    type: 'hbox',
                    align: 'center'
                },
                items: [{
                    xtype: 'image',
                    height: 80,
                    margin: 20,
                    width: 80,
                    alt: 'lockscreen-image',
                    cls: 'lockscreen-profile-img auth-profile-img',
                    src: img //'resources/images/user-profile/2.png'
                },{
                    xtype: 'box',
                    html: '<div class=\'user-name-text\'>' + name + ' </div><div class=\'user-post-text\'> La Colonia Employee </div>'
                }]
            },{
                xtype: 'container',
                padding: '0 20',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                    margin: '10 0'
                },
                items: [{
                    xtype: 'textfield',
                    reference: 'LockScreenPasswordField',
                    labelAlign: 'top',
                    cls: 'lock-screen-password-textbox',
                    labelSeparator: '',
                    fieldLabel: 'It\'s been a while. please enter your password to resume',
                    emptyText: 'Password',
                    inputType: 'password',
                    allowBlank: false,
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop password-trigger'
                        }
                    }
                },{
                    xtype: 'button',
                    reference: 'loginButton',
                    name: 'loginButton',
                    scale: 'large',
                    ui: 'soft-green',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: 'Login',
                    formBind: true,
                    listeners: {
                        click: 'onLockScreenLoginClick'
                    }
                },{
                    xtype: 'button',
                    reference: 'loginPinButton',
                    name: 'loginPinButton',
                    scale: 'large',
                    ui: 'soft-blue',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: 'Use Pin',
                    formBind: true,
                    listeners: {
                        click: 'onLockScreenLoginClick'
                    }
                },{
                    xtype: 'component',
                    html: '<div style="text-align:right">' +
                    '<a href="#" class="link-forgot-password">'+
                    'or, sign in using other credentials</a>' +
                    '</div>',
                    listeners: {
                        render: function(c){
                            c.getEl().on({
                                click: function() {
                                   me.LogOff();
                                }
                            });
                        }
                    }
                }]
            }]
        }];

        this.callParent(arguments)
    },

    LogOff: function () {
        localStorage.removeItem('token');
        location.reload();
    }
});
