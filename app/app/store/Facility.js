Ext.define('ECO.store.Facility', {
    extend: 'Ext.data.Store',

    alias: 'store.facility',

    model: 'ECO.model.Facility',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'http://localhost:3000/api/v1/facilities',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success',
            totalProperty: 'totalCount'
        },
        writer: {
            type: 'json',
            returnJson: true
        }

        // },
        // api: {
        //     read: 'http://localhost:3000/api/v1/facility',
        //     create: 'http://localhost:3000/api/v1/facility',
        //     update: 'http://localhost:3000/api/v1/facility',
        //     destroy: 'http://localhost:3000/api/v1/facility'
        // }
    }
});
