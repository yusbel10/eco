Ext.define('ECO.view.main.ViewportController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mainviewport',

    listen : {
        controller : {
            '#' : {
                unmatchedroute : 'onRouteChange'
            }
        }
    },

    routes: {
        ':node': 'onRouteChange'
    },
    collapsedCls: 'main-nav-collapsed',

    onBellClick: function (btn) {

       // ;

        // var mask = new Ext.LoadMask({ target: this.getView(), msg: "<img src='resources/images/ripple.svg'/>", indicator:false, useMsg: true, alwaysOnTop: true,  focusable: false});
        // mask.show();

        ECO.helper.Func.initMask();

        ECO.helper.Func.fadeOutMask().delay(2000);


    },

    setCurrentView: function(hashTag) {
        hashTag = (hashTag || '').toLowerCase();

        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            navigationList = refs.navigationTreeList,
            store = navigationList.getStore(),
            node = store.findNode('routeId', hashTag) ||
                   store.findNode('viewType', hashTag),
            view = (node && node.get('view')) || 'pages.Error404Window',
            lastView = me.lastView,
            existingItem = mainCard.child('component[routeId=' + hashTag + ']'),
            newView;

        // Kill any previously routed window
        if (lastView && lastView.isWindow) {
            lastView.destroy();
        }

        lastView = mainLayout.getActiveItem();

        if (!existingItem) {
            //;
            newView = Ext.create('ECO.view.' + (view || 'pages.Error404Window'), {
                hideMode: 'offsets',
                routeId: hashTag
            });
        }

        if (!newView || !newView.isWindow) {
            // !newView means we have an existing view, but if the newView isWindow
            // we don't add it to the card layout.
            if (existingItem) {
                // We don't have a newView, so activate the existing view.
                if (existingItem !== lastView) {
                    mainLayout.setActiveItem(existingItem);
                }
                newView = existingItem;
            }
            else {
                // newView is set (did not exist already), so add it and make it the
                // activeItem.
                Ext.suspendLayouts();
                mainLayout.setActiveItem(mainCard.add(newView));
                Ext.resumeLayouts(true);
            }
        }

        navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        me.lastView = newView;

    },

    onNavigationTreeSelectionChange: function (tree, node) {

        if (node.data.view == "logout"){
            localStorage.removeItem('token');
            location.reload();
            return;
        }
        var to = node && (node.get('routeId') || node.get('viewType'));

        if (to) {
            this.redirectTo(to);
        }
    },

    onToggleNavigationSize: function () {
        var me = this,
            refs = me.getReferences(),
            navigationList = refs.navigationTreeList,
            wrapContainer = refs.mainContainerWrap,
            collapsing = !navigationList.getMicro(),
            new_width = collapsing ? 64 : 250;

        if (Ext.isIE9m || !Ext.os.is.Desktop) {
            Ext.suspendLayouts();

            refs.senchaLogo.setWidth(new_width);

            navigationList.setWidth(new_width);
            navigationList.setMicro(collapsing);

            Ext.resumeLayouts(); // do not flush the layout here...

            // No animation for IE9 or lower...
            wrapContainer.layout.animatePolicy = wrapContainer.layout.animate = null;
            wrapContainer.updateLayout();  // ... since this will flush them
        }
        else {
            if (!collapsing) {
                // If we are leaving micro mode (expanding), we do that first so that the
                // text of the items in the navlist will be revealed by the animation.
                navigationList.setMicro(false);
            }

            // Start this layout first since it does not require a layout
            refs.senchaLogo.animate({dynamic: true, to: {width: new_width}});

            // Directly adjust the width config and then run the main wrap container layout
            // as the root layout (it and its chidren). This will cause the adjusted size to
            // be flushed to the element and animate to that new size.
            navigationList.width = new_width;
            wrapContainer.updateLayout({isRoot: true});

            // We need to switch to micro mode on the navlist *after* the animation (this
            // allows the "sweep" to leave the item text in place until it is no longer
            // visible.
            if (collapsing) {
                navigationList.on({
                    afterlayoutanimation: function () {
                        navigationList.setMicro(true);
                    },
                    single: true
                });
            }
        }
    },

    onMainViewRender:function() {

        if (!window.location.hash) {
              this.redirectTo("dashboard");
            return;
        }
        var route;
        if(window.location.hash.charAt(0) === '#'){
            route = window.location.hash.substr(1);
            this.setCurrentView(route);
        }



    },

    onRouteChange:function(id){
        var ref = this.getView().getReference();
        if(ref === "voicePanel"){ return; }
        this.setCurrentView(id);
    },

    onSearchRouteChange: function () {
        this.setCurrentView('search');
    },

    onEmailRouteChange: function () {
        this.setCurrentView('email');
    },

    redirectTo: function(token, force) {
        if (token.isModel) {
            token = token.toUrl();
        }
        if (!force) {
            var currentToken = Ext.util.History.getToken();

            if (currentToken === token) {
                return false;
            }
        } else {
            Ext.app.route.Router.onStateChange(token);
        }

        Ext.util.History.add(token);

        return true;
    },

    onOpenVoiceToolClick: function (btn) {
        if(!ECO.Profile.isUserInRole(['2'])){
           return;
        }
        var me = this,
            refs = me.getReferences(),
            voicePanel = refs.voicePanel,
            isCollapse = voicePanel.getCollapsed(),
            created = false,
            mainToolbar = refs.mainToolbar;

        mainToolbar.disable();


        // if(isCollapse){
        //     Ext.create('Ext.fx.Anim', {
        //         target: voicePanel,
        //         duration: 1000,
        //         easing: 'easeOut',
        //         from: {
        //             width: 0 //starting width 400
        //         },
        //         to: {
        //             width: 200, //end width 300
        //         }
        //     });
        // }else{
        //     Ext.create('Ext.fx.Anim', {
        //         target: voicePanel,
        //         duration: 1000,
        //         easing: 'easeOut',
        //         from: {
        //             width: 200 //starting width 400
        //         },
        //         to: {
        //             width: 0, //end width 300
        //         }
        //     });
        // }



        // voicePanel.animCollapse.fadeOut({
        //     duration: 500,
        //     remove: false
        // });



        if(isCollapse && created){

            voicePanel.expand();
        }else if(isCollapse && !created){

            ECO.cache.Global.createVisualizers(mainToolbar);
            voicePanel.expand();
        }else if(!isCollapse && !created){
            ECO.cache.Global.stopRender(voicePanel, mainToolbar)
        }else{
            ECO.cache.Global.stopRender(voicePanel, mainToolbar)
        }


      //  (isCollapse) ?  : ECO.VoiceFrequencyCache.stopRender(voicePanel);

    },


    onVoiceTelephoneBtnClick: function (btn) {
        var me = this,
            refs = me.getReferences(),
            voiceProfileTelephoneStatusField = refs.voiceProfileTelephoneStatusField,
            params = {
                To: refs.voiceProfileTelephone.getValue()
            };


        Twilio.Device.connect(params);
        console.log('Calling ' + params.To + '...');
        voiceProfileTelephoneStatusField.setValue('Connecting');
    },

    onVoiceTelephoneBtnClickHangUp: function (btn) {
        var me = this,
            refs = me.getReferences(),
            voiceProfileTelephoneStatusField = refs.voiceProfileTelephoneStatusField;
        console.log('Hanging up...');
        voiceProfileTelephoneStatusField.setValue('Hanging up');
        Twilio.Device.disconnectAll();
    },

    onGmapClick: function () {
        var mapwin;
        if(mapwin) {
            mapwin.show();
        } else {
            mapwin = Ext.create('Ext.window.Window', {
                autoShow: true,
                layout: 'fit',
                title: 'GMap Window',
                closeAction: 'hide',
                width: 450,
                height: 450,
                border: false,
                x: 40,
                y: 60,
                items: {
                    xtype: 'gmappanel',
                    center: {
                        geoCodeAddr: '4 Yawkey Way, Boston, MA, 02215-3409, USA',
                        marker: {title: 'Fenway Park'}
                    },
                    markers: [{
                        lat: 42.339641,
                        lng: -71.094224,
                        title: 'Boston Museum of Fine Arts',
                        listeners: {
                            click: function (e) {
                                Ext.Msg.alert('It\'s fine', 'and it\'s art.');
                            }
                        }
                    }, {
                        lat: 42.339419,
                        lng: -71.09077,
                        title: 'Northeastern University'
                    }]
                }
            });
        }
    }
});
