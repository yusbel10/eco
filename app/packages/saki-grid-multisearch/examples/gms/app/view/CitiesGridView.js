// vim: sw=4:ts=4:nu:nospell:fdc=4
/*global Ext:true */
/*jslint browser: true, devel:true, sloppy: true, white: true, plusplus: true */

/*
 This file is part of Saki Grid MultiSearch Plugin Example Application

 Copyright (c) 2014, Jozef Sakalos, Saki

 Package:  saki-grid-multisearch
 Author:   Jozef Sakalos, Saki
 Contact:  http://extjs.eu/contact
 Date:     6. March 2014

 Commercial License
 Developer, or the specified number of developers, may use this file in any number
 of projects during the license period in accordance with the license purchased.

 Uses other than including the file in the project are prohibited.
 See http://extjs.eu/licensing for details.
 */
Ext.define('Gms.view.CitiesGridView', {
     extend:'Ext.grid.Panel'
    ,requires:[
         'Gms.store.CitiesStore'
        ,'Gms.model.ComboModel'
        ,'Ext.saki.grid.MultiSearch'
        ,'Ext.form.field.Date'
        ,'Ext.toolbar.Paging'
        ,'Ext.form.field.ComboBox'
        ,'Ext.util.Point'
    ]
    ,alias:'widget.citiesgrid'
    ,pageSize:15

    ,stateId:'cities-grid'
    ,stateful:true

    ,initComponent:function() {
        var  me = this
            // numeric filter function for local filtering
            ,numFilterFn = function(record, name, value) {
                var  numValue = parseFloat(value.replace(/[^0-9]/g, ''))
                    ,operator = value.substr(0,1)
                    ,retval = true
                    ,fieldValue = record.get(name)
                ;

                if(isNaN(numValue)) {
                    return false;
                }

                switch(operator) {
                    case '>':
                        retval = fieldValue > numValue;
                    break;

                    case '<':
                        retval = fieldValue < numValue;
                    break;

                    case '=':
                    default:
                        retval = fieldValue == numValue;
                    break;
                }

                return retval;
            } // eo function numFilterFn
            ,config = {
                store:Ext.create('Gms.store.CitiesStore', {
                     remoteSort:true
                    ,remoteFilter:true
                    ,pageSize:me.pageSize
                })
                ,plugins:[{
                     ptype:'saki-gms'
                    ,filterOnEnter:false
                    ,filterAfterRender:true
                    ,parseOperator:false
                    ,height:21
                }]
                ,selType:'checkboxmodel'
                ,selModel:{

                }
                ,columns:[{
                     text:'Geography'
                    ,columns:[{
                        text:'City'
                            ,dataIndex:'name'
                            ,sortable:true
                            ,filter:true
                        },{
                            text:'Ascii Name'
                            ,dataIndex:'asciiName'
                            ,sortable:true
                            ,filter:true
                        },{
                            text:'Alternate Names'
                            ,dataIndex:'alternateNames'
                            ,sortable:true
                            ,width:180
                            ,filter:true
                        },{
                            text:'Country'
                            ,dataIndex:'countryCode'
                            ,sortable:true
                            ,align:'center'
                            ,width:70
                            ,filter:true
                        },{
                            text:'Country 2'
                            ,dataIndex:'countryCode2'
                            ,sortable:true
                            ,align:'center'
                            ,width:70
                            ,filter:true
                        },{
                            text:'Population'
                            ,dataIndex:'population'
                            ,sortable:true
                            ,align:'right'
                            ,format:'0,000'
                            ,width:80
                            ,filter:{
                                xtype:'textfield'
                                ,filterFn:numFilterFn
                            }
                        }]
                }, {
                    text:'Latitude'
                    ,dataIndex:'latitude'
                    ,renderer:Ext.util.Format.numberRenderer('0.00000')
                    ,align:'right'
                    ,sortable:true
                    ,width:80
                    ,filter:{
                         xtype:'combobox'
                        ,valueField:'value'
                        ,displayField:'text'
                        ,queryMode:'local'
                        ,store:{
                             model:'Gms.model.ComboModel'
                            ,data:[
                                 {value:'>=0', text:'Northern'}
                                ,{value:'<=0', text:'Southern'}
                            ]
                        }
                    }
                },{
                     text:'Longitude'
                    ,dataIndex:'longitude'
                    ,renderer:Ext.util.Format.numberRenderer('0.00000')
                    ,align:'right'
                    ,sortable:true
                    ,width:80
                    ,filter:{
                         xtype:'combobox'
                        ,store:[
                             ['>=0', 'Eastern']
                            ,['<=0', 'Western']
                        ]
                    }
                },{
                     text:'F Code'
                    ,dataIndex:'featureCode'
                    ,sortable:true
                    ,align:'center'
                    ,width:50
                    ,filter:true
                },{
                    text:'F Class'
                    ,dataIndex:'featureClass'
                    ,sortable:true
                    ,align:'center'
                    ,width:50
                    ,filter:true
                },{
                     text:'Elevation'
                    ,dataIndex:'gtopo30'
                    ,sortable:true
                    ,renderer:Ext.util.Format.numberRenderer('0,000')
                    ,align:'right'
                    ,width:80
                    ,filter:{
                         xtype:'combo'
                        ,store:[
                             ['<0', '<0']
                            ,['>500', '>500']
                            ,['>1000', '>1000']
                            ,['>2000', '>2000']
                            ,['>3000', '>3000']
                        ]
                    }
                },{
                     text:'Admin Code 1'
                    ,dataIndex:'adminCode1'
                    ,align:'center'
                    ,sortable:true
                    ,width:70
                    ,filter:true
                },{
                     text:'Admin Code 2'
                    ,dataIndex:'adminCode2'
                    ,align:'center'
                    ,sortable:true
                    ,width:70
                    ,filter:true
                },{
                     text:'Time Zone'
                    ,dataIndex:'timezone'
                    ,sortable:true
                    ,filter:true
                },{
                     text:'Updated'
                    ,dataIndex:'lastUpdated'
                    ,sortable:true
                    ,filter:'datefield'
                    ,renderer:Ext.util.Format.dateRenderer('M j, Y')
                }]
                ,scroll:'both'
            }
        ;
        config.bbar = {
            items:['->',{
                 xtype:'tbtext'
                ,text:'Total Count: 0'
                ,itemId:'count'
            }]
        };
        //config.store.on('load', function(store){
        //    var  count = me.down('toolbar #count')
        //        ,text = Ext.String.format('Total Count: {0}', store.getTotalCount());
        //    count.setText(text);
        //}, me);
        config.bbar = config.paging = Ext.create('Ext.toolbar.Paging', {
             store:config.store
            ,displayInfo:true
        });

        // apply config
        Ext.apply(me, config);

        me.callParent(arguments);
    } // eo function initComponent
});


// eof
