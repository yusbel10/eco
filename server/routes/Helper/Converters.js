var converter = {


    date: function (date) {
        var dateParts = date.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
        return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    },

    gCloudUrl: function (filename) {
        return 'https://storage.googleapis.com/' + 'lcmc-eco' + '/' + filename;
    }

};
module.exports = converter;