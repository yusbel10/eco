var express = require('express'),
    router = express.Router(),
    auth = require('./Auth/Auth'),
    facility = require('./Facility/Facility'),
    patient = require('./apps/Patient/Patient'),
    voice = require('./Voice/Voice'),
    google = require('./Helper/Google')

/*
 * Routes that can be accessed by any one
 */
router.post('/api/login', auth.login);

router.get('/api/voice', voice.token);
router.post('/api/voice', voice.voice);
//router.get('/products', products.getAll);

/*
 * Routes that can be accessed only by autheticated users
 */




/** GLOBALS **/

/*
 * FACILITIES
 */

router.get('/api/v1/facility', facility.get);
router.post('/api/v1/facility', facility.post);
router.put('/api/v1/facility', facility.put);
router.delete('/api/v1/facility', facility.delete);

/*
 * AUTHENTICATION
 */

router.get('/api/v1/authenticate', auth.validateToken);


/** APPS **/

/*
 * PATIENT
 */

router.get('/api/v1/patient', patient.get);
router.post('/api/v1/patient', patient.post);
router.put('/api/v1/patient/profile', patient.put);
router.delete('/api/v1/patient', patient.delete);

router.get('/api/v1/patient/profile', patient.getProfile);
router.post('/api/v1/patient/profile/image', patient.editProfileImage);
router.put('/api/v1/patient/profile/update_note', patient.updateNote);

router.get('/api/v1/patient/profile/phone', patient.getAllTelephone);
router.post('/api/v1/patient/profile/phone', patient.addEditPhone);
router.delete('/api/v1/patient/profile/phone', patient.removePhone);
router.put('/api/v1/patient/profile/phone/primary', patient.markPhonePrimary);
router.post('/api/v1/patient/profile/address', patient.addEditAddress);
router.delete('/api/v1/patient/profile/address', patient.removeAddress);
router.put('/api/v1/patient/profile/address/primary', patient.markAddressPrimary);

router.get('/api/v1/patient/address', patient.getAllAddress);
router.get('/api/v1/patient/create_patient_search', patient.createPatientSearch);


//GOOGLE
router.post('/api/v1/google/geocode', google.geocode);

/*
 * Routes that can be accessed only by authenticated & authorized users
 */




module.exports = router;

