Ext.define('ECO.view.apps.AppsController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'
    ],

    alias: 'controller.apps',

    //region appDblClick
    appDblClick: function(rec){
        if(rec){
            this.createTab('apps', rec, {
                xtype: rec.data.url,
                session: true,
                title: rec.data.name,
                viewModel: {
                    data: {
                    }
                }
            });
        }
    },
    //endregion

    //region createTab
    createTab: function (prefix, rec, cfg) {
        var type = this.type;
        var tabs = this.getView(),
            id = (rec != null) ? prefix + '_' + rec.get('url') : null,
            tab = tabs.items.getByKey(id);

        if (!tab) {
            cfg.itemId = id;
            cfg.closable = true;
            cfg.type = type;
            tab = tabs.add(cfg);
        }

        tabs.setActiveTab(tab);
    }
    //endregion

});