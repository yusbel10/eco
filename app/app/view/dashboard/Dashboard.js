Ext.define('ECO.view.dashboard.Dashboard', {
    extend: 'Ext.Container',
    xtype: 'eco-dashboard',


    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'ECO.view.dashboard.CalendarPanel'
    ],
    layout: 'responsivecolumn',

    initComponent: function () {
        this.items = [{
            userCls: 'big-60 small-100',
            xtype: 'calendar-panel'
        },
            {
                // xtype: 'hddusage',
                userCls: 'big-20 small-50'
            },
            {
                //    xtype: 'earnings',
                userCls: 'big-20 small-50'
            },
            {
                //  xtype: 'sales',
                userCls: 'big-20 small-50'
            },
            {
                //   xtype: 'topmovies',
                userCls: 'big-20 small-50'
            },
            {
                //   xtype: 'weather',
                cls: 'weather-panel shadow',
                userCls: 'big-40 small-100'
            },
            {
                //  xtype: 'todo',
                userCls: 'big-60 small-100'

            },
            {
                //   xtype: 'services',
                userCls: 'big-40 small-100'
            }
        ];

        this.callParent(arguments)
    }

});
