/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */

Ext.define('ECO.view.main.Main', {
    extend: 'Ext.container.Viewport',
    xtype: 'mainviewport',
    reference: 'mainviewport',
    requires: [
        'Ext.plugin.Viewport',
        'Ext.list.Tree',
        'ECO.view.dashboard.Dashboard',
        'ECO.view.apps.Apps',
        'ECO.view.main.Voice',
        'Ext.ux.DataView.Animated',
        'Ext.ux.GMapPanel'
    ],

    controller: 'mainviewport',
    viewModel: {
        type: 'mainviewport'
    },

    cls: 'sencha-dash-viewport',
    itemId: 'mainView',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: false,

    listeners: {
        render: 'onMainViewRender',
        'afterrender':function(c){
            var meRender = this;
            Ext.onReady(function(){
                var task = new Ext.util.DelayedTask(function(){
                    var collapsebtn = meRender.lookupReference('collapsebtn');
                    collapsebtn.btnEl.dom.click();


                });
               // task.delay(1200);
            });


        }
    },

    initComponent : function() {

        var isConnected = '<span style="color: #ADB3B8" class="x-fa fa-wifi"></span>',
            micIcon = '<span style="color: red" class="x-fa fa-microphone"></span>',
            _connected = false,
            me = this;

        var DataViewStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            sortOnLoad: true,
            remoteSort: false,
            fields: ['name', 'thumb', 'url', 'type', 'roles'],
            data:  ECO.Profile.getAppList()
            /** proxy: {
                type: 'ajax',
                url : appsList,
                reader: {
                    type: 'json',
                    rootProperty: ''
                }
            } **/
        });

        var root = ECO.Profile.getNavStore();



        var NavStore = new Ext.data.TreeStore({
            storeId: 'NavigationTree',
            root: root,
            fields: [{
                name: 'text'
            }]
        });

        var firstname = ECO.Profile.getFirstname(),
            lastname = ECO.Profile.getLastname(),
            imgurl = ECO.Profile.getImg_url(),
            connectedComponent = {
                xtype:'component',
                name:'isConnected',
                itemId:'isConnected',
                html:  isConnected
                //html:'test'
            };


        var toolbarItem = [{
            xtype: 'component',
            reference: 'senchaLogo',
            cls: 'sencha-logo',
            html: '<div class="main-logo"><img src="resources/images/sencha-icon.png">E.C.O.</div>',
            width: 250
        },{
            margin: '0 0 0 8',
            cls: 'delete-focus-bg',
            reference:'collapsebtn',
            iconCls:'x-fa fa-navicon',
            handler: 'onToggleNavigationSize'
        },{
            xtype: 'tbspacer',
            flex: 1
        }];


        if(ECO.Profile.isUserInRole(['2'])){
            toolbarItem.push({
                // xtype:'component',
                cls: 'delete-focus-bg',
                //iconCls:'x-fa fa-microphone',
                html: micIcon,
                handler:'onOpenVoiceToolClick'
                //href: '#profile',
                //hrefTarget: '_self',
                // tooltip: 'Open voice console'
            });
        }

        toolbarItem.push({
           // xtype:'component',
           // name:'isConnected',
           // itemId:'isConnected',
            html:  isConnected,
            handler: 'onGmapClick'
            //html:'test'
        },{
            cls: 'delete-focus-bg',
            iconCls:'x-fa fa-bell',
            handler: 'onBellClick'
        },{
            xtype: 'tbtext',
            text: firstname + ' ' + lastname,
            cls: 'top-user-name'
        },{
            xtype: 'image',
            //  id: 'img',
            cls: 'header-right-profile-image',
            height: 35,
            width: 35,
            // layout: 'absolute',
            alt:'current user image',
            src: imgurl,
            listeners:{
                afterrender: function(c) {
                    c.el.on('click', function (e) {
                        Ext.Msg.confirm('Log off', 'Continue Log Off?',
                            function (choice) {
                                if (choice === 'yes') {
                                    localStorage.removeItem('token');
                                    window.location.reload();
                                }
                            }
                        );
                    });
                }
            }
        });





        Ext.apply(this, {
            items:[{
                xtype: 'toolbar',
                cls: 'sencha-dash-dash-headerbar toolbar-btn-shadow',
                height: 64,
                itemId: 'headerBar',
                reference:'mainToolbar',
                items: toolbarItem
            },{
                xtype: 'maincontainerwrap',
                id: 'main-view-detail-wrap',
                reference: 'mainContainerWrap',
                flex: 1,
                items: [{
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    ui: 'navigation',
                    store: NavStore,
                    width: 250,
                    expanderFirst: false,
                    expanderOnly: false,
                    singleExpand: true,
                    listeners: {
                        selectionchange: 'onNavigationTreeSelectionChange'
                    }
                },{
                    xtype: 'container',
                    flex: 1,
                    reference: 'mainCardPanel',
                    cls: 'sencha-dash-right-main-container',
                    itemId: 'contentPanel',
                    layout: {
                        type: 'card',
                        anchor: '100%'
                    }
                },{
                    xtype: 'voice',
                    userCls: 'big-50 small-100 shadow'
                }]
            }]

        });


        this.callParent(arguments);

    }

});
