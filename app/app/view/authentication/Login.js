Ext.define('ECO.view.authentication.Login', {
    extend: 'ECO.view.authentication.LockingWindow',
    xtype: 'authenticationLogin',

    requires: [
        'ECO.view.authentication.Dialog',
        'Ext.container.Container',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.button.Button'
    ],

    title: 'Let\'s Log In To ECO',
    defaultFocus: 'authdialog', // Focus the Auth Form to force field focus as well

    items: [{
        xtype: 'authdialog',
        defaultButton : 'loginButton',
        autoComplete: true,
        bodyPadding: '20 20',
        cls: 'auth-dialog-login',
        header: false,
        width: 415,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        defaults : {
            margin : '5 0'
        },
        items: [{
            xtype: 'label',
            text: 'Sign into your account'
        },{
            xtype: 'textfield',
            reference:'loginusername',
            cls: 'auth-textbox',
            name: 'username',
            bind: localStorage.getItem('usernamefield'),
            height: 55,
            hideLabel: true,
            allowBlank : false,
            emptyText: 'Username',
            value: localStorage.getItem('usernamefield'),
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-email-trigger'
                }
            }
        },{
            xtype: 'textfield',
            cls: 'auth-textbox',
            reference: 'loginpassword',
            height: 55,
            hideLabel: true,
            emptyText: 'Password',
            inputType: 'password',
            name: 'password',
            bind: '{password}',
            allowBlank : false,
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-password-trigger'
                }
            }
        },{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'checkboxfield',
                reference: 'loginremember',
                flex : 1,
                cls: 'form-panel-font-color rememberMeCheckbox',
                height: 30,
                bind: localStorage.getItem('rememberLogin'),
                value: localStorage.getItem('rememberLogin'),
                boxLabel: 'Remember me'
            }]
        },{
            xtype: 'button',
            reference: 'loginButton',
            name: 'loginButton',
            scale: 'large',
            ui: 'soft-green',
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: 'Login',
            formBind: true,
            listeners: {
                click: 'onLoginButton'
            }
        },{
            xtype: 'button',
            reference: 'loginPinButton',
            name: 'loginPinButton',
            scale: 'large',
            ui: 'soft-blue',
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: 'Use Pin',
            formBind: true,
            listeners: {
                click: 'onLoginButton'
            }
        }]
    }],

    initComponent: function() {
        this.addCls('user-login-register-container');
        this.callParent(arguments);
    }
});
