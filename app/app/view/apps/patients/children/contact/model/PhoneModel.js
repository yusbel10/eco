
Ext.define('ECO.view.apps.patients.children.contact.model.PhoneModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type:'int'},
        {name:'tele_type'},
        {name:'tele_number'},
        {name:'tele_comment'},
        {name:'primary', type: 'bool'}
    ]
});