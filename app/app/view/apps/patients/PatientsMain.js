Ext.define('ECO.view.apps.patients.PatientsMain', {
    extend: 'Ext.tab.Panel',
    xtype: 'patients-main',
    reference:'patients-main',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Ext.grid.column.Action',
        'ECO.view.apps.patients.PatientsMainController',
        'Ext.saki.grid.MultiSearch'
    ],

    controller: 'patients',

    config: {
        PatientStore: null,
        PatientColumn: null

    },

    ui:'ChartMainTabUi',
    layout: 'responsivecolumn',
    tabPosition: 'left',
    tabRotation: 0,
    iconCls: 'right-icon x-fa fa-users',
    listeners:{
        openSelectedPatient:  'openPatient',
        loadAllGridStores: 'loadAllGridStores',
        loadAllColumns: 'loadAllColumns'
    },



    //region initComponent
    initComponent: function () {
        var me = this,
            note;
        this.fireEvent('loadAllGridStores');
        this.fireEvent('loadAllColumns');

        if(!localStorage.getItem('token')){
            return;
        }

        Ext.apply(this, {
            items: [{
                xtype: 'container',
                layout: 'fit',
                iconCls: 'x-fa fa-th-large',
                title: 'Dashboard',
                cls: 'shadow-panel dash-background',
                flex: 1,
                padding: 15,
                items: [{
                    xtype:'gridpanel',
                    reference:'patientGrid',
                    cls: 'patient-chart-grid shadow-panel',
                    responsiveCls: 'big-100 small-100',
                    iconCls: 'x-fa fa-user ',
                    ui: 'light',
                    title: 'Patient List',
                    scrollable: true,
                    height:500,
                    flex:1,
                    headerBorders: true,
                    multiSelect: false,
                    viewConfig: {
                        forceFit: true,
                        autoScroll: true,
                        stripeRows: true,
                        singleSelect: false,
                        emptyText: 'No matching appointments found',
                        enableTextSelection: false,
                        rowLines: true
                    },

                    plugins: [{
                        ptype:'saki-gms',
                        pluginId:'gms',
                        filterOnEnter:true
                    }],

                    tools: [{
                        xtype: 'tool',
                        toggleValue: false,
                        tooltip: 'Add patient',
                        cls: 'x-fa fa-user-plus',
                        listeners: {
                            click: 'onAddPatientBtnClick'
                        },
                        width: 20,
                        height: 20

                    }],
                    listeners : {
                        itemdblclick: function(dv, record, item, index, e) {
                            me.fireEvent('openSelectedPatient', record.data);
                        }
                    },

                    store: this.getPatientStore(),
                    columns: this.getPatientColumn(),
                    bbar: Ext.create('Ext.PagingToolbar', {
                        //store: chartStore,
                        displayInfo: true,
                        displayMsg: 'Displaying topics {0} - {1} of {2}',
                        emptyMsg: "No topics to display"
                    })
                }]

            }]
        });

        this.callParent(arguments)
    }
    //endregion
});