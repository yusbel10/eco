Ext.define('ECO.model.Facility', {
    extend: 'Ext.data.Model',
    fields:[{
        name: 'facility_id', type: 'int'
    },{
        name: 'facility_create_date',  type: 'date', dateFormat: 'Y-m-d H:i:s',
        convert: function(val, rec){
            // debugger;
           // return Ext.util.Format.date(val, 'Y-m-d');
            return Ext.util.Format.date(val, 'm/d/Y');
        }
    },{
        name: 'facility_medical_provider_id', type: 'int'
    },{
        name: 'facility_type', type: 'int'
    },{
        name: 'facility_name', type: 'string'
    }]
});
