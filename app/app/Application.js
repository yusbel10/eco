/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('ECO.Application', {
    extend: 'Ext.app.Application',
    requires:[
        'ECO.ux.ActivityMonitor',
        'ECO.helper.Func',
        'ECO.cache.Global',
        'Ext.window.Toast'
    ],
    name: 'ECO',

    stores: [
        // TODO: add global / shared stores here
    ],


    globals:{
        url: 'https://localhost:3000/api/',
        //url:'https://eco-yusbel80.c9users.io:/api/',
        //url:'https://lcmc.io:8000/api/',
        voiceUrl: 'https://voice.lcmc.io:3000/'
    },

    views:[
        'ECO.view.main.Main'
    ],


    defaultToken : 'dashboard',


    //region init
    init: function() {
        // start the mask on the body and get a reference to the mask
        ECO.helper.Func.initMask();
    },
    //endregion

    //region launch
    launch: function () {

        // TODO - Launch the application

        var me = this;

        Ext.tip.QuickTipManager.init();
        this.loadProfile(this)
            .then(function (content) {
                if(content){
                    var roles = ECO.Profile.getRoles();

                    if(roles){
                        var promise = ECO.helper.Func.getVoiceToken('token', 'GET');

                        promise.then(function (content) {
                            if(ECO.Profile.isUserInRole(['2'])){
                                console.log('Getting Voice Token');
                                me.loadVoiceToken(content);
                            }


                        },function (error) {
                            console.error(error);
                        }, function (value) {
                        });
                    }

                    Ext.widget("mainviewport");
                }else{
                    Ext.widget("authenticationLogin");
                    //Ext.create('ECO.view.' + ('pages.Error404Window'),{ hideMode:'offsets', routeId: 'pages.Error500Window'});
                }

            },function (err) {
                console.error(err);
                Ext.create('ECO.view.' + ('pages.Error500Window'),{ hideMode:'offsets', routeId: 'pages.Error500Window'});
            },function (value) {

            })
            .always(function(){
                ECO.helper.Func.fadeOutMask().delay(1000);
            });

    },
    //endregion

    //region loadProfile
    loadProfile: function () {
        var deferred = new Ext.Deferred();

        var supportsLocalStorage = Ext.supports.LocalStorage,
            loggedIn = Ext.decode(localStorage.getItem('token'));

        if (!supportsLocalStorage) {

            // Alert the user if the browser does not support localStorage
            Ext.Msg.alert('Your Browser Does Not Support Local Storage');
            return false;
        }


        if(loggedIn){
            Ext.Ajax.setDefaultHeaders({'X-Access-Token': loggedIn.token });

            var promise = ECO.helper.Func.callAjax('v1/authenticate', null, 'GET');

            promise.then(function (content) {
                if(content.success == true) {
                    ECO.Profile = Ext.create('ECO.cache.Profile',{
                        username: content.token.user.user_username,
                        userid: content.token.user.user_id,
                        firstname: content.token.user.user_firstname,
                        lastname: content.token.user.user_lastname,
                        mi: content.token.user.user_mi,
                        img_url: content.token.user.user_picture,
                        roles: content.token.user.roles
                    });
                    console.log(content.token.user.roles);
                    //me.activeMonitorInit();
                    deferred.resolve(true);
                } else{
                    localStorage.removeItem('token');
                    location.reload()
                }
            });


            // var roles = [];
            // roles.push({role: "1" });
            // roles.push({role: "2" });
            //
            // ECO.Profile = Ext.create('ECO.cache.Profile',{
            //     username: 'yusbel.rojas',
            //     user_id: 1,
            //     employee_id: 1,
            //     firstname: 'Yusbel',
            //     lastname: 'Rojas',
            //     img_url: null,
            //     roles: roles
            // });
            //
            // Ext.Ajax.setDefaultHeaders({'X-Access-Token': 'my_token'});
            //
            // deferred.resolve('mainviewport');

        }else {
            deferred.resolve(false);
        }
        return deferred.promise;
    },
    //endregion

    //region loadVoiceToken
    loadVoiceToken: function (data) {
        Twilio.Device.setup(data.token);

        Twilio.Device.ready(function (device) {
            ECO.cache.Global.setTwilioDevice(device);
            var voicePanel = ECO.cache.Global.getVoicePanel(),
                refs = voicePanel.getReferences(),
                voiceProfileTelephoneBtn = refs.voiceProfileTelephoneBtn,
                voiceProfileTelephoneStatusField = refs.voiceProfileTelephoneStatusField;

            voiceProfileTelephoneStatusField.setValue('Waiting');

            console.log('Twilio.Device Ready!');

        });

        Twilio.Device.error(function (error) {
            var voicePanel = ECO.cache.Global.getVoicePanel(),
                refs = voicePanel.getReferences(),
                voiceProfileTelephoneBtn = refs.voiceProfileTelephoneBtn,
                voiceProfileTelephoneStatusField = refs.voiceProfileTelephoneStatusField;

            voiceProfileTelephoneStatusField.setValue('Not Connected');
            console.log('Twilio.Device Error: ' + error.message);
        });

        Twilio.Device.connect(function (conn) {

            var voicePanel = ECO.cache.Global.getVoicePanel(),
                refs = voicePanel.getReferences(),
                voiceProfileTelephoneBtn = refs.voiceProfileTelephoneBtn,
                voiceProfileTelephoneStatusField = refs.voiceProfileTelephoneStatusField;


            voiceProfileTelephoneBtn.setUI('soft-red');
            voiceProfileTelephoneBtn.setHandler('onVoiceTelephoneBtnClickHangUp');
            voiceProfileTelephoneStatusField.setValue('Connected');

            console.log('Successfully established call!');
        });

        Twilio.Device.disconnect(function (conn) {

            var voicePanel = ECO.cache.Global.getVoicePanel(),
                refs = voicePanel.getReferences(),
                voiceProfileTelephoneBtn = refs.voiceProfileTelephoneBtn,
                voiceProfileTelephoneStatusField = refs.voiceProfileTelephoneStatusField;

            voiceProfileTelephoneBtn.setUI('soft-green');
            voiceProfileTelephoneBtn.setHandler('onVoiceTelephoneBtnClick');
            voiceProfileTelephoneStatusField.setValue('Waiting');

            console.log('Call ended.');
        });

        Twilio.Device.incoming(function (conn) {
            log('Incoming connection from ' + conn.parameters.From);
            var archEnemyPhoneNumber = '+12099517118';

            if (conn.parameters.From === archEnemyPhoneNumber) {
                conn.reject();
                console.log('It\'s your nemesis. Rejected call.');
            } else {
                // accept the incoming connection and start two-way audio
                conn.reject();
            }
        });
    },
    //endregion

    //region onAppUpdate
    onAppUpdate: function () {
        console.log('Update');
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
    //endregion
});
