Ext.define('ECO.view.apps.patients.model.Patient', {
    extend: 'Ext.data.Model',
    fields:[
        {name: 'pat_id', type: 'int'},
        {name: 'pat_picture', type: 'string'},
        {name: 'pat_active_status', type: 'boolean'},
        {name: 'pat_chart_number', type: 'string'},
        {name: 'pat_first_name', type: 'string'},
        {name: 'pat_last_name', type: 'string'},
        {name: 'pat_middle_name', type: 'string'},
        {name: 'pat_sex', type: 'int'},
        {name: 'pat_dob', type: 'date', dateFormat: 'Y-m-d H:i:s',
            convert: function(val, rec){
            return new Date(val);
               // return Ext.util.Format.date(val, 'm/d/Y');
               // return Ext.util.Format.date(val, 'Y-m-d');

            }},
        {name: 'pat_tele', type: 'string',
            convert: function (val, rec) {
                if(!val || val == null){
                    return '';
                }
                return (/^[\d\+\-\(\) ]+$/.test(val)) ? val : 'Wrong Format';
            }}

    ]
});
