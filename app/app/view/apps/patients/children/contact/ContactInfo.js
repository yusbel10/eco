Ext.define('ECO.view.apps.patients.children.contact.ContactInfo', {
    extend: 'Ext.container.Container',
    xtype: 'patient-profile-contactinfo',

    requires: [
        'ECO.view.apps.patients.children.contact.ContactInfoController',
        'ECO.view.apps.patients.children.contact.model.PhoneModel'
    ],

    controller: 'contactInfo',
    config: {
        teleComboStore: null,
        teleStore: null,
        addressStore: null,
        teleColumn: null,
        addressColumn: null

    },

    listeners:{
        loadAllGridStores: 'loadAllGridStores',
        loadAllColumns: 'loadAllColumns'
    },


    //region initComponent
    initComponent: function(){
        var me = this,
            ViewModelData = this.getViewModel().getData().patientRecord;

        this.fireEvent('loadAllGridStores');
        this.fireEvent('loadAllColumns');

        var PhoneCellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });
        var AddressCellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });




        Ext.apply(this, {
            listeners:{
                'afterrender': function () {
                }
            },
            items:[{
                    xtype:'container',
                    title:'Contact Information',
                    height: 300,
                    padding:15,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },

                    items: [{
                        xtype:'gridpanel',
                        ui: 'light',
                        cls: 'patient-chart-grid telephone-grid-panel shadow-panel',
                        scroll: true,
                        headerBorders: true,
                        rowLines: true,
                        stripeRows:false,
                        emptyText: 'No data',
                        viewConfig: {
                            forceFit: true,
                            preserveScrollOnRefresh: true,
                            autoScroll: true,
                            stripeRows: true,
                            singleSelect: false,
                            emptyText: 'No matching telephone found',
                            enableTextSelection: false,
                            rowLines: true
                        },
                        store: this.getTeleStore(),
                        columns: this.getTeleColumn(),
                        plugins: [PhoneCellEditing],
                        iconCls: 'x-fa fa-phone',
                        tools: [{
                            xtype: 'tool',
                            toggleValue: false,
                            tooltip: 'Add phone',
                            cls: 'x-fa  fa-plus dashboard-tools',
                            width: 20,
                            height: 20,
                            handler:function (e,btn,panel) {

                                var rec = new ECO.view.apps.patients.children.contact.model.PhoneModel({
                                    type: '',
                                    phone: '',
                                    primary: false
                                });

                                me.getTeleStore().insert(0, rec);
                                PhoneCellEditing.startEditByPosition({
                                    row: 0,
                                    column: 0
                                });

                            }
                        }]
                    }]


            },{
                xtype:'panel',
                height:300,
                cls: 'shadow-panel',
                layout: 'fit',
                title:'Address Information',
                ui: 'light',
                responsiveCls: 'big-50 small-100',
                iconCls: 'x-fa fa-map-marker',
                tools: [{
                    xtype: 'tool',
                    toggleValue: false,
                    tooltip: 'Add address information',
                    cls: 'x-fa  fa-plus dashboard-tools',
                    listeners: {
                        // click: 'onActiveTabNoteSaveClick'
                    },
                    width: 20,
                    height: 20,
                  //  handler:'onAddAddressClick'
                    handler:function (e,btn,panel) {

                        var rec = new ECO.view.apps.patients.children.contact.model.Address({
                            addr_street: '',
                            addr_apartment: '',
                            addr_zipcode: '',
                            addr_city: '',
                            addr_comment: '',
                            addr_lat: null,
                            addr_lng: null
                        });

                        me.getAddressStore().insert(0, rec);
                        PhoneCellEditing.startEditByPosition({
                            row: 0,
                            column: 0
                        });

                    }
                }],
                items:[{
                    xtype:'container',
                    padding:15,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    // cls: 'timeline-items-wrap chart-profile-desc',
                    items: [{
                        xtype:'gridpanel',
                        cls: 'telephone-grid-panel',
                        scroll: true,
                        headerBorders: true,
                        name:'contactAddressGrid',
                        reference:'contactAddressGrid',
                        rowLines: true,
                        stripeRows:false,
                        emptyText: 'No data',
                        viewConfig: {
                            forceFit: true,
                            preserveScrollOnRefresh: true,
                            stripeRows: true
                        },
                        store: this.getAddressStore(),
                        columns: this.getAddressColumn(),
                        plugins: [AddressCellEditing]
                    }]


                }]


            }]

        });

        this.callParent(arguments);
    }
    //endregion


});
