var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyBALn-B9cb9IA_GKHZvThslxoYjhKr460E'
});

var google = {
    geocode: function(req, res) {
        googleMapsClient.geocode({
            address: req.body.address
        }, function(err, response){
            if (!err){
                res.json({
                    success: true,
                    data: response
                })
                return;
            }
            res.status(400);
            res.json({
                status: 400,
                message: "Unable to geocode address"
            });
        });
    }
}

module.exports = google;
  
