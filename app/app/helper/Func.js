Ext.define('ECO.helper.Func',{
    singleton : true,
    config : {
        splashScreen: {}
    },
    requires: ['Ext.window.Toast'],

    //region fadeOutMask
    fadeOutMask: function () {
        var me = this;
        return new Ext.util.DelayedTask(function () {

            // fade out the body mask
            me.getSplashScreen().fadeOut({
                duration: 500,
                remove: true
            });


        });
    },
    //endregion

    //region initMask
    initMask: function () {
        this.setSplashScreen(Ext.getBody().mask("<img src='resources/images/ripple.svg'/>"));

    },
    //endregion

    //region delayMainToolbar
    delayMainToolbar: function (mainToolbar) {
        var me = this;
        return new Ext.util.DelayedTask(function () {
            mainToolbar.enable();
        });
    },
    //endregion

    //region getVoiceToken
    getVoiceToken: function ( url, method) {
        var me = this;

        // var mask = new Ext.LoadMask({ target: view, msg: "<img src='resources/images/ripple.svg'/>", indicator:false, useMsg: true, alwaysOnTop: true,  focusable: false});
        // mask.show();

        var deferred = new Ext.Deferred();

        Ext.Ajax.request({
            url:ECO.app.globals.voiceUrl+url,
            disableCaching: false,
            method: method,
            dataType: 'json',
            success: function(response) {
                var res = Ext.decode(response.responseText);
                if(res.success) {
                    deferred.resolve(res);
                } else{
                    deferred.reject(res.message);
                }
            },
            failure: function(res) {
                Ext.Msg.alert('Error', 'Unable to connect to voice server');
                deferred.reject(res.message);
            },
            callback: function(){
            }
        });

        return deferred.promise;
    },
    //endregion

    //region getProperty
    getProperty: function (data, propertyName) {
        return data[propertyName];
    },
    //endregion

    //region callAjax
    callAjax: function(url, params, method){
        var me = this;

        // var mask = new Ext.LoadMask({ target: view, msg: "<img src='resources/images/ripple.svg'/>", indicator:false, useMsg: true, alwaysOnTop: true,  focusable: false});
        // mask.show();

        var deferred = new Ext.Deferred();


        me.initMask();

        if(ECO.Profile){

            params.userid = ECO.Profile.getUserid();
        }


        var jsonData =  Ext.encode(params);


        Ext.Ajax.request({
            url:ECO.app.globals.url+url,
            disableCaching: false,
            method: method,
            dataType: 'json',
            // headers: {
            //     'X-Access-Token': (localStorage.getItem('token')) ? Ext.decode(localStorage.getItem('token')).token : null
            // },
            jsonData: jsonData,
            params: params,
            success: function(response) {
                var res = Ext.decode(response.responseText);
                if(res.success) {
                    deferred.resolve(res);
                } else{
                    deferred.reject(res.message);
                    Ext.MessageBox.alert('Error', res.message)
                }
            },
            failure: function(response) {
                var statusCode = response.status,
                    res;
                if(response.responseText !== ""){
                    res = Ext.decode(response.responseText);
                }else{
                    localStorage.removeItem('token');
                    console.error('Unable to connect to Server');
                    deferred.reject('Unable to connect to Server');
                    Ext.create('ECO.view.' + ('pages.Error500Window'),{ hideMode:'offsets', routeId: 'pages.Error500Window'});
                    return;
                }

                switch (statusCode) {
                    case 400:
                        me.showToast(res.message);

                        break;

                    case 401:
                        console.error('Unable to connect to Server');
                        deferred.reject('Unable to connect to Server');
                        Ext.create('ECO.view.' + ('authentication.LockScreen'),{ hideMode:'offsets', routeId: 'authentication.LockScreen'});
                        break;


                    case 403:

                        break;

                    default:
                        if(response.responseText === ""){
                            localStorage.removeItem('token');
                            console.error('Unable to connect to Server');
                            deferred.reject('Unable to connect to Server');
                            Ext.create('ECO.view.' + ('pages.Error500Window'),{ hideMode:'offsets', routeId: 'pages.Error500Window'});
                            return;
                        }

                        console.error(res.message);
                        deferred.reject(res.message);

                        Ext.MessageBox.alert('Error', res.message,
                            function (choice) {
                                if (choice === 'ok') {
                                    window.location.reload();
                                }
                            }
                        );
                }

            },
            callback: function(){
                me.fadeOutMask().delay(1000);
            }
        });

        return deferred.promise;

    },
    //endregion

    //region showToast
    showToast: function (msg) {

        Ext.toast({
            html: msg,
            closable: false,
            align: 't',
            slideInDuration: 400,
            minWidth: 400
        });
    },
    //endregion

    //region showMsg
    showMsg: function(title, msg, icon, btn){
        var me = this;
        icon = Ext.MessageBox[icon];

        Ext.MessageBox.show({
            title: title,
            msg: msg,
            buttons: Ext.MessageBox.OK,
            scope: me,
            fn: me.showToast(msg),
            icon: icon
        });
    },
    //endregion


    validateVariable: function (data) {
        if(data == false || data == null || typeof data == "undefined" || data == ''){
            return false
        }
        return true;


    },
    //region codeAddress
    codeAddress: function(address, _callback){
        var geocoder = new google.maps.Geocoder();    // instantiate a geocoder object
        geocoder.geocode( { 'address': address}, function(results, status) {
            (status == google.maps.GeocoderStatus.OK) ? _callback(results) : _callback(false);
        });

    },
    //endregion

    constructor : function(config){
        this.initConfig(config);
        this.callParent(arguments);
    }
});