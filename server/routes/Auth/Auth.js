var jwt = require('jwt-simple'),
    Promise = require("bluebird");

var ActiveDirectory = require('activedirectory');



var auth = {

    login: function(req, res) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection,
            me = this,
            username = req.body.username || req.query.username,
            password = req.body.password || req.query.password,
            type   = req.body.type || req.query.type;

        if (!username || !password) {
            res.status(400);
            res.json({
                status: 400,
                message: "Invalid credentials"
            });
            return;
        }

        if(type == 0){
            var config = { url: 'ldap://ldap.lcmcnet.com:389',
                baseDN: 'dc=lcmc,dc=local',
                username: username+'@lcmc.local',
                // username: 'username@domain.com',
                password: password }
            var ad = new ActiveDirectory(config);

            ad.authenticate(username+'@lcmc.local', password, function (err, response) {
                if (err) {
                    res.status(400);
                    res.json({ success: false, code: 400, message: "Invalid Credentials or Account"});
                    return;
                }

                if (response) {
                    MysqlKnex('eco_user')
                        .where('user_username', username)
                        .then(function (rows) {


                            if(!auth.verify(username, rows)){
                                res.status(403);
                                res.json({ success: false, code: 403, message: "Invalid Account"});
                                return;
                            }

                            auth.validate(username, password, rows, type).then(function (content) {

                                if(!content.success){
                                    res.status(content.code);
                                    res.json({
                                        status: content.code,
                                        message: content.message
                                    });
                                    return;
                                }

                                if (content.success) {

                                    // If authentication is success, we will generate a token
                                    // and dispatch it to the client

                                    res.json({
                                        token: genToken(rows, content),
                                        success: true
                                    });
                                }
                            });
                        }).catch(function(error) {
                        res.json({ success: false, code: 503, message: "Error in connection database"});
                    });
                }
                else {
                    console.log('Authentication failed!');
                }
            });
        }else if(type == 1){
            MysqlKnex('eco_user')
                .where('user_username', username)
                .then(function (rows) {
                    auth.validate(username, password, rows, type).then(function (content) {

                        if(!content.success){
                            res.status(content.code);
                            res.json({
                                status: content.code,
                                message: content.message
                            });
                            return;
                        }

                        if (content.success) {

                            // If authentication is success, we will generate a token
                            // and dispatch it to the client

                            res.json({
                                token: genToken(rows, content),
                                success: true
                            });
                        }
                    });
                }).catch(function(error) {
                res.json({ success: false, code: 503, message: "Error in connection database"});
            });
        }

    },


    verify: function (username, rows) {
        if (rows.length > 0) {
            if(rows[0].user_active == 0){
                return {
                    username: username,
                    user_id: rows[0].user_id,
                    firstname: rows[0].user_firstname,
                    lastname: rows[0].user_lastname,
                    mi: rows[0].user_mi,
                    img_url: rows[0].user_picture

                };
            }
        }
        return false;
    },


    validate: function (username, password, rows, type) {
        var MysqlKnex = global.App.mySQlDatabase.mysqlConnection;
        return new Promise(function (resolve) {
            if (rows.length > 0) {
                if(type == 1){
                    if(password != rows[0].user_pin){
                        resolve({ success: false, code: 400, message: "Invalid Credentials"});
                        return;
                    }
                }

                MysqlKnex.select('map_user_permissions.mup_id','map_user_permissions.mup_permission_id', 'map_user_permissions.mup_employee',
                    'map_user_permissions.mup_supervisor','map_user_permissions.mup_manager','map_user_permissions.mup_administrator')
                    .from('eco_user')
                    .leftJoin('map_user_permissions', function () {
                        this.on('map_user_permissions.mup_user_id', '=', 'eco_user.user_id')
                    })
                    .where('eco_user.user_username', username)
                    .then(function (response) {
                        resolve({ success: true, roles: response });
                    }).catch(function(error) {
                    resolve({success: false, code: 503, message: "Error in connection database"});
                });
            }else {
                resolve({ success: false, code: 400, message: "No user found"});
            }
        })



    },

    validateToken: function(req, res){
        res.status(202);
        res.json({
            token: jwt.decode((req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'], require('../../config/secret')()),
            success: true
        });
      //  res.json(jwt.decode(req.query.token, require('../config/secret.js')()));
    }
};


function genToken(user, roles) {
    var expires = expiresIn(1); // 1 days

    user[0].roles = roles.roles;
    var data = user[0];

    var token = jwt.encode({
        exp: expires,
        user:data
    }, require('../../config/secret')());

    console.log("token: "+token + "\n" + "Expires: " + expires);

    return {
        token: token,
        expires: expires,
        user: user
    };
}


function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
