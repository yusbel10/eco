module.exports = function(req, res, next) {

    //Implement any logic that should be carried prior to execution
    //Example: open database connection
    //console.log('before');

    var appDBMySql = global.App.mySQlDatabase;
    var appDBPostgreSql = global.App.postgreSQlDatabase;

    if(!appDBMySql.mysqlConnection){
        appDBMySql.connectMySQL();
    }
    if(!appDBPostgreSql.postgresConnection){
        appDBPostgreSql.connectPostgresql();
    }



    next();
};
