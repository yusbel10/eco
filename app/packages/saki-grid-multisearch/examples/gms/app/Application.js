// vim: sw=4:ts=4:nu:nospell:fdc=4
/*global Ext:true */
/*jslint browser: true, devel:true, sloppy: true, white: true, plusplus: true */

/*
 This file is part of Saki Grid MultiSearch Plugin Example Application

 Copyright (c) 2014, Jozef Sakalos, Saki

 Package:  saki-grid-multisearch
 Author:   Jozef Sakalos, Saki
 Contact:  http://extjs.eu/contact
 Date:     6. March 2014

 Commercial License
 Developer, or the specified number of developers, may use this file in any number
 of projects during the license period in accordance with the license purchased.

 Uses other than including the file in the project are prohibited.
 See http://extjs.eu/licensing for details.
 */

Ext.define('Gms.Application', {
     name: 'Gms'
    ,extend: 'Ext.app.Application'

    ,requires:[
         'Gms.view.CitiesGridView'
        ,'Ext.state.CookieProvider'
    ]

    ,uses:[
        'Ext.window.Window'
    ]

    ,views:[]
    ,controllers:[]
    ,stores: []

    ,launch:function() {
//        Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
        Ext.widget('citiesgrid',{
            height:420
//            ,width:668
            ,renderTo:Ext.getBody()
        })
    } // eo function launch
}); // eo define

// eof
