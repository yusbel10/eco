Ext.define('ECO.view.apps.patients.children.contact.model.Address', {
    extend: 'Ext.data.Model',
    fields:[
        {name: 'addr_id', type: 'int'},
        {name: 'addr_create_date', type: 'date', dateFormat: 'Y-m-d H:i:s',
            convert: function(val, rec){
                return Ext.util.Format.date(val, 'm/d/Y');
                //return Ext.util.Format.date(val, 'Y-m-d');

            }},
        {name: 'addr_street', type: 'string'},
        {name: 'addr_apartment', type: 'string'},
        {name: 'addr_zipcode', type: 'string'},
        {name: 'addr_city', type: 'string'},
        {name: 'addr_state', type: 'string'},
        {name: 'addr_primary', type: 'boolean'},
        {name: 'pat_sex', type: 'int'},
        {name: 'addr_lat', type: 'string'},
        {name: 'addr_lng', type: 'string'},
        {name: 'addr_comment', type: 'string'}

    ]
});
