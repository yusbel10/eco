Ext.define('ECO.view.apps.patients.children.InPatientMainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox',
        'Ext.layout.container.Card'
    ],

    alias: 'controller.in-patient',

    //region onActiveTabNoteSaveClick
    onActiveTabNoteSaveClick: function () {
        var me = this,
            refs = this.getReferences(),
            patId = this.getView().getViewModel().getData().patientRecord.pat_id;

        var noteVal = refs.patientNote.getValue();

        ECO.helper.Func.callAjax('v1/patient/profile/update_note',{ pat_id: patId, pat_note: noteVal },'PUT').then(function (content) {

            if(content.success){
                ECO.helper.Func.showToast('Note saved successfully');
            }else{
                ECO.helper.Func.showToast('<img src="resources/images/icons/error-icon.svg" alt="Error" width="42" height="42" align="middle"> Error saving note');
            }

        });

    },
    //endregion

    //region onEditPatientProfile
    onEditPatientProfile: function () {
        var me = this,
            view = this.getView(),
            PatientRecord =  view.getViewModel().getData().patientRecord;

        PatientRecord.pat_dob = new Date(PatientRecord.pat_dob);

        var win = new Ext.Window({
                applyTo: me,
                layout: 'fit',
                resize: false,
                header: false,
               // border: false,
                closable: false,
                frame:false,
                shadow:false,
                border:false,
                modal: true,
                width: 755,
                height: 460,
                viewModel:{
                    data: PatientRecord
                },
                closeAction: 'close',

                items:[{
                    xtype:'form',
                    cls: 'forms-patient-dynamic-window',
                    bodyPadding: 10,
                    scrollable:true,
                    // bodyStyle:{
                    //     "background-color":"#579ddb",
                    //     "color": "#fff"
                    // },
                    // style: {
                    //     background: '#579ddb'
                    // },
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    fieldDefaults: {
                        labelAlign: 'left',
                        labelWidth: 90,
                        anchor: '100%',
                        msgTarget: 'side'
                    },

                    items: [{
                        margin: '0 0 20 0',
                        xtype: 'component',
                        bind:{
                            html:
                                '<div class="createpatient-outer">'+
                                    '<div class="createpatient-inner">' +
                                        '<h3>Edit: {pat_first_name} {pat_last_name}</h3>'+
                                    '</div>'+
                                '</div>'
                        }

                    },{
                        xtype: 'textfield',
                        fieldLabel: 'First Name',
                        emptyText: 'First Name',
                        name: 'FirstName',
                        fieldStyle: 'text-transform:uppercase',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        allowBlank: false,
                        bind: '{pat_first_name}',
                        listeners:{
                            change: function (e, newValue, oldValue, eOpts) {
                                return newValue.toUpperCase();
                            }
                        }
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'MI',
                        emptyText: 'MI',
                        fieldStyle: 'text-transform:uppercase',
                        allowBlank: true,
                        name: 'MI',
                        bind: '{pat_mi}',
                        listeners:{
                            change: function (e, newValue, oldValue, eOpts) {
                                return newValue.toUpperCase();
                            }
                        }
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Last Name',
                        emptyText: 'Last Name',
                        fieldStyle: 'text-transform:uppercase',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        allowBlank: false,
                        name: 'LastName',
                        bind: '{pat_last_name}',
                        listeners:{
                            change: function (e, newValue, oldValue, eOpts) {
                                return newValue.toUpperCase();
                            }
                        }
                    },{
                        xtype: 'datefield',
                        fieldLabel: 'DOB',
                       // format: 'Y-m-d',
                      //  format: 'Y-m-d H:i:s',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        ],
                        allowBlank: false,
                        name: 'DOB',
                        bind: '{pat_dob}',
                        maxValue: new Date()

                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        fieldStyle: 'text-transform:uppercase',
                        allowBlank: true,
                        name: 'email',
                        vtype: 'email',
                        bind: '{pat_email}',
                        listeners:{
                            change: function (e, newValue, oldValue, eOpts) {
                                return newValue.toUpperCase();
                            }
                        }
                    }],

                    buttons:[{
                        text: 'Update',
                        disabled: true,
                        formBind: true,
                        handler: function () {
                            //http://www.soft4tec.de/development/extjs/fileupload-with-ajax.html
                            //http://stackoverflow.com/questions/19661157/how-to-add-header-data-in-xmlhttprequest-when-using-formdata

                            var form = this.up('form').getForm(),
                                values = form.getValues();

                            values.pat_id = PatientRecord.pat_id;

                            // for(var i in PatientRecord){
                            //     if(PatientRecord.hasOwnProperty(i)){
                            //         values[i] = PatientRecord[i];
                            //
                            //     }
                            // }



                            if(form.isValid()){
                                ECO.helper.Func.callAjax('v1/patient/profile', values, 'PUT').then(function (content) {
                                    if(content.success == true) {
                                        ECO.helper.Func.showToast('Profile Updated Successfully');
                                        win.close();
                                    } else{
                                        Ext.Msg.alert(content.message);
                                    }
                                });
                            }else {

                                Ext.Msg.alert( "Error!", "Your form is invalid!" );
                            }
                        }
                    },{
                        text:'Close',
                        handler: function () {
                            win.close();
                        }
                    }]

                }]
            });

        win.show();
    },
    //endregion

    //region onPatientProfileImageUploadClick
    onPatientProfileImageUploadClick: function (btn) {
        var me = this,
            view = this.getView(),
            PatientRecord =  view.getViewModel().getData().patientRecord,

            fileChangeExt = null,

            uploadWindow = Ext.create('Ext.window.Window',{
                title: 'Upload Attachment',
                applyTo: me,
                layout: 'fit',
                resize: false,
                header: false,
                // border: false,
                closable: false,
                frame:false,
                shadow:false,
                border:false,
                modal: true,
                width: 500,
                height: 150,
                viewModel:{
                    data: PatientRecord
                },
                items:[{
                    xtype:'form',
                    cls: 'forms-patient-dynamic-window',
                    reference: 'uploadform',
                    bodyPadding: 10,
                    scrollable:true,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    fieldDefaults: {
                        labelAlign: 'left',
                        labelWidth: 90,
                        anchor: '100%',
                        msgTarget: Ext.supports.Touch ? 'side' : 'qtip'
                    },

                    items: [{
                        xtype: 'filefield',
                      //  inputType: 'file',

                        emptyText: 'Select an image',
                        fieldLabel: 'Photo',
                        name: 'images',
                        id: 'profileImageField',
                        itemId: 'profileImageField',
                        buttonText: '',
                        buttonConfig: {
                            iconCls: 'upload-icon'
                        },
                        allowBlank: false,
                        listeners:{
                            afterrender:function(cmp){
                                cmp.fileInputEl.set({
                                    accept:'image/*' // or w/e type
                                });
                            },
                            change:function(filefield, value){

                                var filePath = value,
                                    fileNameIndex = filePath.lastIndexOf("\\"),
                                    fileName = filePath.substring(fileNameIndex + 1),
                                    fileExt = fileName.substring(fileName.lastIndexOf(".") + 1);

                                fileChangeExt = fileExt.toLowerCase();
                            }
                        }
                    }],

                    buttons:[{
                        text: 'Upload',
                        disabled: true,
                        formBind: true,
                        handler: function () {

                            //https://www.html5rocks.com/en/tutorials/file/xhr2/
                            var form = this.up('form').getForm(),
                                url = ECO.app.globals.url + 'v1/patient/profile/image';




                            if(fileChangeExt == 'jpeg' || fileChangeExt == 'jpg' || fileChangeExt == 'gif' || fileChangeExt == 'png'){
                                if (form.isValid()) {


                                    // Ext.Ajax.cors = true;
                                   //  Ext.Ajax.useDefaultXhrHeader = false;
                                    //
                                    // form.headers = {
                                    //      //"x-access-token": Ext.decode(localStorage.getItem('token')).token
                                    //     'Content-Type': 'multipart/form-data',
                                    //     "Accept" : "application/json;charset=utf-8"
                                    // };

                                    form.submit({
                                        url : url,
                                        method: 'POST',
                                        params: {
                                            domain: document.domain,
                                            'access_token': Ext.decode(localStorage.getItem('token')).token,
                                            'pat_id': PatientRecord.pat_id
                                        },
                                        // headers:{
                                        //     'Content-Type': 'multipart/form-data',
                                        //     "Accept" : "application/json;charset=utf-8"
                                        // },
                                       // useDefaultXhrHeader: false,
                                        waitMsg : 'Uploading your Photo...',
                                        success : function (fp, o) {
                                            var res = Ext.decode(o.response.responseText),
                                                profilePictureImg = view.lookupReference('profilePictureImg');

                                            view.getViewModel().getData().patientRecord.pat_picture = res.pat_picture ;


                                            profilePictureImg.update("<img src='"+res.pat_picture +"' style='border-radius: 50%; border: 2px solid #ddd;' alt='Patient Picture' height='80px' width='80px'>")
                                            profilePictureImg.updateLayout();

                                            ECO.helper.Func.showToast('Your Photo has been uploaded.');
                                            uploadWindow.close();
                                        },
                                        failure : function (fp, o) {
                                            Ext.Msg.alert('Failed', 'Your Photo failed to uploaded.');
                                        }
                                    });
                                }
                            }
                            else if(fileChangeExt){
                                //showOKInfoMsg(SWP_Msg.SWP_CompressFileFrmtMsg);
                                Ext.Msg.alert('Warning', 'Please select Image file only');
                                form.reset();
                            }
                        }
                    },{
                        text:'Close',
                        handler: function () {
                            uploadWindow.close();
                        }
                    }]

                }]
            });
        uploadWindow.show();
    }
    //endregion

});