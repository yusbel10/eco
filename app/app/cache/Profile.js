Ext.define("ECO.cache.Profile", {
    //singleton : true,
    config: {
        username: '',
        userid: '',
        firstname: '',
        lastname: '',
        mi: '',
        img_url: '',
        roles: []
    },


    //region getNavStore
    getNavStore: function () {
        var me = this;
        return {
            expanded: true,
            children: [{
                text:   'Dashboard',
                view:   'pages.BlankPage',
                //view:   'dashboard.Dashboard',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-desktop',
                routeId: 'dashboard'
            },{
                text: 'Apps',
                iconCls: 'x-fa fa-th-large',
                expanded: false,
                leaf:   true,
                //selectable: true,
                routeId: 'apps-parent',
                //children: me.getNavList(),
                view: 'apps.Apps'
            },{
                text: 'Log Out',
                iconCls: 'x-fa fa-sign-out',
                leaf:   true,
                //selectable: true,
                routeId: 'Dashboard',
                //children: me.getNavList(),
                view: 'logout'
            }]

        }
    },
    //endregion

    //region getNavList
    getNavList: function(){
        var navChildren = [];

        if (this.isUserInRole(['1'])){
            navChildren.push({
                text:   'Patients',
                view:   'apps.patients.PatientsMain',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-users',
                routeId: 'apps-parent'
            })
        }

        if (this.isUserInRole(['3'])){
            navChildren.push({
                text: 'Logistics',
               // view: 'apps.logistics.LogisticsMain',
                view: 'pages.BlankPage',
                leaf: true,
                iconCls: 'right-icon x-fa fa-map-pin',
                routeId: 'apps-parent'
            });
        }

        return navChildren;

    },
    //endregion

    //region getAppList
    getAppList: function () {
        var appsList = [];

        if (this.isUserInRole(['1'])){
            appsList.push({
                "name": "Patients",
                "thumb": "charts.svg",
                "href":'apps.patients',
                "url": "patients-main",
                "type": "Application",
                "role": ["1"]
            });
        }

        if (this.isUserInRole(['4'])){
            appsList.push({
                "name": "Logistics",
                "thumb": "dispatch.svg",
              //  "href":'apps.logistics',
                "href":'pages.pageblank',
                //"url": "logistics-main",
                "url": "pageblank",
                "type": "Application",
                "role": ["2"]
            });
        }


        return appsList;
    },
    //endregion

    //region isUserInRole
    isUserInRole: function(roles) {
        var inArray = false,
            curRoles = [];

        //Check if the values returned from client store is array format

        if(Array.isArray)
            var isArray = Array.isArray(this.getRoles());

        //If values is in array format put it in curRoles var, if not then convert it to array and put it in var
        if(isArray){
            curRoles = this.getRoles();
        }else{
            curRoles = this.getRoles().split(',');
        }

        for(var i = 0; i < curRoles.length; i++){
            if(curRoles[i].mup_permission_id == roles[0]){
                inArray = true;
            }
        }
        return inArray;

        //Other forms of checking if var is in array
        /**for (var i = 0; i < roles.length; i++) {
            var inArray = Ext.Array.contains(this.getRoles(), roles[i]);
        } **/

        /**for (var i = 0; i < roles.length; i++){
            var inArray = isInArray(roles[i], this.getRoles())
        } **/


        /**  for (var i = 0; i < roles.length; i++){
           for(var x = 0; x < curRoles.length; x++){
               if(curRoles[x] === roles[i]){
                   inArray = true;
               }
           }
        } **/


    },
    //endregion

    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    }
});